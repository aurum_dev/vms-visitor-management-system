

<!DOCTYPE html>
<html>
<head>
<title>Orize Securities</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
  <link href="css/jquery.signaturepad.css" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body  >
 
<section class=" container-fluid  ">
  <header class="row   innerpage_header">
 
      <h2 class="">Sign Out </h2>
    
    
  </header>
  <div class="container">
    <diV class="row  align-items-center justify-content-center">
      
     <div class="col-sm-12"><h1 class="title_heading text-center">Please enter your otp to logout user</h1>
  
 </div>
 <div class="col-sm-12">
    <form class="row align-items-center justify-content-center" id="slider_form" method="post" action="" enctype="multipart/form-data">
	<div class="clearfix col-sm-12">
      <div id="result"></div>
	  </div>
   <div class="form-group col-sm-10 col-10">
 
  <input type="text" class="form-control input_style" id="vist_name">
</div>
    
 
   <div class="form-group col-sm-10 col-xs-11 text-center">
  <button  type="submit" class="btn btn-primary sub_testi">Sign Out</button>
</div>

</form>
</div>
  </div>



</section>
<!--Script -->
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
   <script src="js/jquery.signaturepad.js"></script>
 
  <script>
   $(document).ready(function() {
      $('#slider_form').signaturePad({drawOnly:true});
    });
  </script> 
 


 <script type="text/javascript">
      $('.sub_testi').on('click', function (e) {
      e.preventDefault();
      $("#upload").css("display","none");
      $("#loader").css("display","block");
      var form_data = new FormData();
    
    form_data.append("vist_name", document.getElementById('vist_name').value); 
     
      $.ajax({
      url: 'sign_out_conf.php',
      dataType: 'text',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function (response) {
 
 if(response==1)
      {
        document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      $("#result").addClass("alert alert-success fade show");
      $("#result").html("Successfully logged out!");
       window.location.replace("thanks.php");
      }
      else
      {
      $("#result").addClass("alert alert-danger fade show");
      $("#result").html("Invalid otp.");
      } 
      $('#description').val('');
      $(".alert").delay(2000).slideUp(250, function() {
      $(this).alert('close');
      });
      
  //location.reload();
      },
      error: function (response) {
      alert(response);
      
     // location.reload();
      }
      });
      });
      </script>
<!--Script -->
</body>
</html>