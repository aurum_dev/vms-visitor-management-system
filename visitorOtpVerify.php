<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
print_r($_POST);*/
require __DIR__ . '/vendor/sms/Pinnacle.php';
session_start();
if(isset($_POST['visitorMobileNo']) && !empty($_POST['visitorMobileNo'])) { 
    $status = 0;
    $mobileNo = trim($_POST['visitorMobileNo']);

    if(preg_match('/^[0-9]{10}$/', $mobileNo)) {
        $n = 6; // OTP digit Count
        $otpCode=generateNumericOTP($n);

        /*
        * code to send OTP SMS 
        */
        $sms = new Pinnacle();
        $resp = $sms->sendOtp(trim($otpCode), trim($mobileNo));
        /*
        * SMS Code Ends here
        */
        $resp = json_decode($resp, true);
        if($resp['status'] == 'success') {
            $msg = $otpCode;
            $status = 1;
        }else{
            $msg = "Something went wrong message have not sent";
        }
    }else{
        $msg = "Invalid Mobile Number";
    }

    if($status == 1 && isset($_POST['visitor_reg']) && $_POST['visitor_reg'] == 1) {
        $_SESSION['register'] = array('code' => $otpCode, 'number' => $mobileNo);
        $msg = "Otp sent Successfully";
    }
    
    echo json_encode(array("status" => $status, "msg" => $msg)); die();  
}

if(isset($_POST['resend_otp']) && $_POST['resend_otp'] == 1) {
    if(isset($_SESSION['register']['number']) && !empty($_SESSION['register']['number'])) {
        $n = 6; // OTP digit Count
        $otpCode=generateNumericOTP($n);

        /*
        * code to send OTP SMS 
        */
        $sms = new Pinnacle();
        $resp = $sms->sendOtp(trim($otpCode), trim($_SESSION['register']['number']));
        /*
        * SMS Code Ends here
        */
        $resp = json_decode($resp, true);
        if($resp['status'] == 'success') {
            $_SESSION['register']['code'] = $otpCode;
            $msg = "Otp sent Successfully";
            $status = 1;
        }else{
            $msg = "Something went wrong message have not sent";
        }

        echo json_encode(array("status" => $status, "msg" => $msg)); die(); 
        
    } else{
        echo json_encode(array("status" => 0, "msg" => "Something went wrong!!!")); die();
    }  
}
 
$mobileNo=$_POST['visitorMobileNo']; // the number you'd like to send the message to
//$mobileNo='9999059687';

$n = 6; // OTP digit Count
$otpCode=generateNumericOTP($n);

// Required if your environment does not handle autoloading
require __DIR__ . '/vendor/autoload.php';

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;
// Your Account SID and Auth Token from twilio.com/console
$sid = 'AC20727a5dd67b744b7fcbf067c14507ad';
$token = 'eb45fb93d869eb2cfa30896a03354fa9';
$client = new Client($sid, $token);
// Use the client to do fun stuff like send text messages!
$client->messages->create(
    // the number you'd like to send the message to
    "+91$mobileNo",
    [
        // A Twilio phone number you purchased at twilio.com/console
        'from' => '+16823564425',
        // the body of the text message you'd like to send
        'body' => "Your Verification Code is $otpCode"
    ]
);


// Function to generate OTP
function generateNumericOTP($n) {      
    // Take a generator string which consist of
    // all numeric digits
    $generator = "1357902468";  
    // Iterate for n-times and pick a single character
    // from generator and append it to $result      
    // Login for generating a random character from generator
    //     ---generate a random number
    //     ---take modulus of same with length of generator (say i)
    //     ---append the character at place (i) from generator to result
  
    $result = "";  
    for ($i = 1; $i <= $n; $i++) {
        $result .= substr($generator, (rand()%(strlen($generator))), 1);
    }  
    // Return result
    return $result;
} 
// Main program
echo $otpCode;
?>