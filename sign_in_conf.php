<?php
date_default_timezone_set('Australia/Melbourne');

include ('secure_login/class/Curd.php');
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
session_start();
$where2 = array(
    "mobile_no" => $_POST['persons_count']
);
$variable2 = $obj_curd->display_all_record("tenants_users", $where2);
foreach ($variable2 as $row2)
{
    $verify_email = $row2['mem_email'];
    $verify_tenant_id = $row2['id'];
}

if (isset($verify_email) && $verify_email != '')
{
    /*SELECT `id`, `visitor_name`, `mobile`, `email_id`, `coming_from`, `visitor_org`, `photo`, `persons_alogn_with_visitor`, `host_orgn`, `host_name`, `meeting_purpose`, `vehicle_no`, `signature`, `in_time`, `out_time`, `verified`, `status`, `add_date`, `update_date` FROM `visitor_info` WHERE 1*/
    $uniqueId = date('ymd', time()) . mt_rand(1, 999);
    $image_name = $_POST['webcamcapture'];

    if ($image_name == '')
    {

        $image_name = $obj_curd->img_new_fun();
    }

    $intime = date('Y-m-d h:i:s', time());
    $about_data = array(
        'visitor_id' => $uniqueId,
        'visitor_name' => $_POST['vist_name'],
        'mobile' => $_POST['vist_mobile'],
        'email_id' => $_POST['vist_email'],
        'coming_from' => $_POST['vist_comming_from'],
        'visitor_org' => $_POST['vist_org'],
        'photo' => $image_name,
        'signature' => $_POST['sign_output'],
        'host_mobile_number' => $_POST['persons_count'],
        'host_orgn' => $_POST['host_org'],
        'host_name' => $verify_tenant_id,
        'meeting_purpose' => $_POST['visit_purpose'],
        'vehicle_no' => $_POST['vehicle_no'],
        'in_time' => $intime,
        'verified' => '0',
        'status' => '0'
    );

    if ($obj_curd->add_record("visitor_info", $about_data))
    {
        /*user mail send*/
        /*SELECT `id`, `user_name`, `mobile_no`, `mem_email`, `tenant_id`, `status` FROM `tenants_users` WHERE 1*/
        $where2 = array(
            "mobile_no" => $_POST['persons_count']
        );
        $variable2 = $obj_curd->display_all_record("tenants_users", $where2);
        foreach ($variable2 as $row2)
        {
            $t_user_email = $row2['mem_email'];
            $t_user_name = $row2['user_name'];
        }

        /*SELECT `id`, `tenant_name`, `tenant_email`, `tenant_contact`, `status`, `add_info_date`, `allow_access`, `update_info_date` FROM `tenants` WHERE 1*/
        $where3 = array(
            "id" => $_POST['host_org']
        );
        $variable3 = $obj_curd->display_all_record("tenants", $where3);
        foreach ($variable3 as $row3)
        {

            $t_name = $row3['tenant_name'];
        }

        /*user mail send*/

        $from = 'itsupport@orize.co.in';
        $to2 = $_POST['vist_email'];
        $subject2 = "Thanks for visit";
        /*$message2="
        <html>
        <head>
        <title>Visitor Id Details</title>
        </head>
        <body>
        <p>Dear Sir/Madam,<br> Greetings from Orize !!! Thanks for visit. please use following visitor id to verify as security desk.</p>
        <table style='    margin: 0 auto 30px;
        display: block;
        max-width: 420px;
        border: 1px solid #000;
        border-radius: 10px;
        '>
                <tr>
                  <td   style='background: #000; color: #fff; font-weight: 700; font-size: 20px; line-height: 23px;padding: 10px 7px;  ' >V<br>I<br>S<br>I<br>T<br>O<br>R</td>
                  <td style='padding: 15px'>
                    <table>
                      <tr >
                        <td colspan='2' class='text-left'><h3 style='margin-bottom: 5px; font-weight: 700; font-size:24px'>ID: ".$uniqueId."</h3></td>
                      
                      </tr>
                      <tr>
                        <td>
                          <img src='https://qparc.in/visitor/secure_login/".substr($image_name,0,-2)."'  style='width: 120px;'>
                        </td>
                        <td class='text-left'>
                          
                      
                            <ul style='font-size: 15px; margin-left: 15px; list-style:none'>
                              <li>Name: <strong>".$_POST['vist_name']."</strong></li> 
                              <li>From: <strong>".$_POST['vist_org'].", ".$_POST['vist_comming_from']."</strong></li> 
                              <li>Meets: <strong>".$t_user_name."</strong></li> 
                              <li>Host company: <strong>".$t_name." </strong></li> 
                              <li>Purpose: <strong>".$_POST['visit_purpose']."</strong></li> 
                              <li>Intime: <strong>".$intime."</strong></li> 
                            </ul>
                       
                        </td> 
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
        </body>
        </html>
        ";*/
        $message2 = "
          <html>
          <head>
          <title>Visitor Id Details</title>
          </head>
          <body>
          <p>Dear Sir/Madam,<br> Greetings from Orize !!!<br> Thanks for visit. <br>Please Wait your verification in progress. Once you will receive the Visitor ID on SMS and Showing to the Security Card to enter into the Premises.</p>

          </body>
          </html>";
        $headers2 = "MIME-Version: 1.0" . "\r\n";
        $headers2 .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers2 .= 'From: <' . $from . '>';

        //mail($to2,$subject2,$message2,$headers2);
        $obj_curd->send_secure_mail($to2, $message2, $subject2);

        /*send mail to t_user*/
        $to = $t_user_email;
        $subject = "Visitor details";
        $message = "
          <html>
          <head>
          <title>Visitor Details</title>
          </head>
          <body>
          <p>Dear Sir/Madam,<br>

          Greetings from Aurum Q Parć<br>
          Your guest '<b>" . $_POST['vist_name'] . "</b>' has requested you to verify his visit.<br>
          We request you to kindly verify it <a href='https://qparc.in/visitor/secure_login/index.php'>Verify</a>.<br>
          Visitor details for your reference:  </p>
          <p>Visitor Details</p>
          <table>
          <tr>
          <th>Name:</th>
           <td>" . $_POST['vist_name'] . "</td>
          </tr>
          <tr>
          <th>Mobile No:</th>
           <td>" . $_POST['vist_mobile'] . "</td>
          </tr>
          <tr>
          <th>Email Id:</th>
           <td>" . $_POST['vist_email'] . "</td>
          </tr>
          <th>Coming From:</th>
           <td>" . $_POST['vist_comming_from'] . "</td>
          </tr>
           
          <tr>
          <th>Meeting Purpose:</th>
           <td>" . $_POST['visit_purpose'] . "</td>
          </tr>
           
           
          </table>
          <p>
          Thanks & Regards<br>
          Orize Property Management Pvt Ltd<br>
          <!--Visitor management number<br>
          Visitor management email id<br>-->
          www.orize.in

          </p>
          </body>
          </html>";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <' . $from . '>';

        $obj_curd->send_secure_mail($to, $message, $subject);
        unset($_SESSION["register"]);
        echo '1';

    }

    else
    {
        echo '0';
    }
}
else
{
    echo "4";
}
?>
