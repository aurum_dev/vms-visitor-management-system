<?php

include ('session_check.php');
$oprn=$_GET['opr'];
include('class/Curd.php');
if($oprn=='add')
{
$area_name="";
$delivery_charge="";
 
$st='checked';
}
else
{
 /*SELECT `id`, `tenant_name`, `tenant_email`, `tenant_contact`, `status`, `add_info_date`, `update_info_date` FROM `tenants` WHERE 1*/
  $id=$_GET['id'];
  
  $where=array("id"=>$id);
$variable=$obj_curd->display_all_record("tenants",$where);


  foreach ($variable as $row) {
  $tenant_name=$row['tenant_name']; 
  $tenant_email=$row['tenant_email']; 
  $tenant_contact=$row['tenant_contact'];     
 
  $status=$row['status'];
  if($status=='1')
  {
    $st="checked";
  }
  else
  {
    $st='';
  }
  $al_access=$row['allow_access'];
  if($al_access=='1')
  {
    $al_access="checked";
  }
  else
  {
    $al_access='';
  }
  }

}
?>

<!DOCTYPE html>
<html>
<?php include('head.php');?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <?php include('nav.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 <?php include('side_menu.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
 

    <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-sm-3"></div>
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><?php echo $oprn?> tenant</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form  id="slider_form" method="post" action="" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="col-sm-12">
                    <center><div id="result"></div></center>
                  </div>
               
               
                   
                  <div class="form-group">
                    <label for="tenant_name">Tenant Name</label>
                    
                    <input type="text" id="tenant_name" name="tenant_name" value="<?php echo $tenant_name;?>" class="form-control input_style validate[required]"> 
                  </div>
                  <div class="form-group">
                    <label for="tenant_email">Email Id</label>
                    
                    <input type="email" id="tenant_email" name="tenant_email" value="<?php echo $tenant_email;?>" class="form-control input_style validate[required]"> 
                  </div>
                  <div class="form-group">
                    <label for="tenant_contact">Mobile No</label>
                    
                    <input type="number" id="tenant_contact" name="tenant_contact" value="<?php echo $tenant_contact;?>" class="form-control input_style validate[required]"> 
                  </div>
               
              
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" value="1" name="status" id="status" <?php echo $st;?>>
                    <label class="form-check-label"  for="status">Active</label>
                  </div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" value="1" name="access_allow" id="access_allow" <?php echo $allow_access;?>>
                    <label class="form-check-label"  for="access_allow">Allow direct access to visitor</label>
                  </div>
                   
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <input type="hidden" value="<?php echo $oprn; ?>"  id="oprn" name="oprn">
                      <?php
if($oprn=='edit')
{
  ?>
 <input type="hidden" value="<?php echo $id; ?>"  id="rid" name="rid"> 
  <?php
}

                  ?>
                  <button type="submit" class="btn btn-primary sub_testi">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

   
          </div>
          <!--/.col (left) -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('Footer.php');?>

  
</div>
<!-- ./wrapper -->

<?php include('script.php');?>
  <script src="../js/jquery.validationEngine.min.js" type="text/javascript" charset="utf-8"></script>
      <script src="../js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">
    $('.sub_testi').on('click', function (e) {
         e.preventDefault();
          if(!$("#slider_form").validationEngine('validate', {promptPosition : "inline", scroll: false})){
      return false;
      }
 $("#upload").css("display","none");
                  $("#loader").css("display","block");
    var form_data = new FormData();
  
   
  form_data.append("tenant_name", document.getElementById('tenant_name').value);  
  form_data.append("tenant_email", document.getElementById('tenant_email').value);  
  form_data.append("tenant_contact", document.getElementById('tenant_contact').value);  
  form_data.append("status", document.getElementById('status').value);
  form_data.append("access_allow", document.getElementById('access_allow').value);
  form_data.append("oprn", document.getElementById('oprn').value);
  /*edit*/
     <?php
if($oprn=='edit')
{
  ?>
 
  form_data.append("rid", document.getElementById('rid').value); 
  <?php 
}
  ?>
  /*edit*/

    $.ajax({
    url: 'tenant_conf.php',
    dataType: 'text',
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
     success: function (response) {
      var res=response.trim();
  //  
       if(res=='1')
        {
              $("#result").addClass("alert alert-success fade show");
              $("#result").html("Update information added successfuly ");
               alert("Update information added successfuly ");
          }
           else if(res=='2')
          {
              $("#result").addClass("alert alert-danger fade show");
              $("#result").html("This data already Exist. Please try again");
              alert("This data already Exist. Please try again");
          }
          else
          {
             $("#result").addClass("alert alert-danger fade show");
              $("#result").html("Please update proper data.");
               alert("Please update proper data.");
          } 

      $('#description').val('');  
       $(".alert").delay(25000).slideUp(4050, function() {
    $(this).alert('close');
}); 
       location.reload();
    },
    error: function (response) {
      alert(response);
       
                            location.reload();
    }
    });
});   

</script>
</body>
</html>
