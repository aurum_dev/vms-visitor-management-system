<?php
    include ('secure_login/class/Curd.php');
    require __DIR__ . '/vendor/sms/Pinnacle.php';

    $q = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
    if(isset($q) && !empty($q)) {
        $id = $obj_curd->decrypt_data(trim($q));
        //$id = $q;

        if(!empty($id)) {
            if(preg_match('/^[0-9]+$/', $id)) {
                $query = "SELECT vf.*, tu.user_name, t.tenant_name FROM visitor_info as vf 
                  LEFT JOIN tenants_users as tu ON vf.host_name = tu.id
                  LEFT JOIN tenants as t ON vf.host_orgn = t.id
                  WHERE vf.id = $id AND vf.verified = 1 AND (vf.out_time IS NULL)";

                $data = $obj_curd->executeRawQuery($query);
                if(count($data) > 0) {
                    $data = $data[0];
                    $row = $data;
                    $err = false;
                }
            }
        }
    }

    if($err) {
        echo "<h1>oops!!</h1>"; die();
    }

?>

<!DOCTYPE html>
<html>
    <style media="print" type="text/css">
        @media print {
            body {-webkit-print-color-adjust: exact;}
            .visitor_bg {
                background-color: #000 !important; border:1px solid #000; text-align: center;
            }
        }
    </style>
    <body class="hold-transition sidebar-mini layout-fixed">
        <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> -->
        <div class="wrapper">
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <!-- left column -->
                            <div class="col-md-12 text-center">
                                <!-- general form elements -->
                                <div class="card " style="width: 500px; margin: 10px; margin:50px auto; padding: 0px; border:1px solid #000; text-align: center;"  id="printableArea">
                                    <table style="">
                                        <tr>
                                            <td class="visitor_bg" style="background: #000; color: #fff; font-weight: 700; font-size: 20px; line-height: 23px;padding: 10px 7px; " bgcolor="#000">V<br>I<br>S<br>I<br>T<br>O<br>R</td>
                                            <td style="padding: 15px">
                                                <table>
                                                    <!--<tr >
                                                        <td colspan="2" class="text-left"><h3 style="margin-bottom: 5px; font-weight: 700; font-size:24px">ID: <?php echo $row['visitor_id']; ?></h3></td>
                                                        
                                                        </tr>-->
                                                    <tr>
                                                        <td>
                                                            <img src="secure_login/<?php echo $row['photo']; ?>"  style="width: 120px;">
                                                        </td>
                                                        <td class="text-left">
                                                            <ul style="font-size: 15px; margin-left: 15px;text-align:left;list-style:none;" class="list-unstyled">
                                                                <li><strong>Name: </strong><?php echo $row['visitor_name']; ?></li>
                                                                <li><strong>Phone: </strong><?php echo $row['mobile']; ?></li>
                                                                <li><strong>Id:</strong><?php echo $row['visitor_id']; ?></strong></li>
                                                                <li><strong>From: </strong><?php echo $row['visitor_org'].', '.$row['coming_from']; ?></li>
                                                                <li><strong>Meets: </strong>
                                                                    <?php echo $row['user_name']; ?></li>
                                                                <li><strong>Host company: </strong>
                                                                    <?php echo $row['tenant_name'];?>
                                                                </li>
                                                                <li><strong>Purpose: </strong><?php echo $row['meeting_purpose']; ?></li>
                                                                <li><strong>Intime: </strong><?php    echo $row['in_time'];                 
                                                                    //echo date_format($row['in_time'],"d/m/Y H:i:s"); 
                                                                    ?></strong></li>
                                                            </ul>
                                                            <img src="images/verified.jpg" style="width:150px;float:right;">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- /.card -->
                            </div>
                            <!--/.col (left) -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- Get user data-->
    </body>
</html>
