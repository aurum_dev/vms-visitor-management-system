<?php
  include ('secure_login/class/Curd.php');
  session_start();
  if(isset($_SESSION['register']['verified']) && $_SESSION['register']['verified'] == 1) {
    $user_data = [];
    $where = array('mobile'=> $_SESSION['register']['number']);
    //$where = array('mobile' =>'7021098759');
    $data = $obj_curd->display_all_record("visitor_info", $where);
    if(!empty($data)) {
      $user_data = $data[0]; 
    }
  }else{
    header("Location: ".$obj_curd->base_url); die();
  }

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Visitor registration</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="css/jquery.signaturepad.css" rel="stylesheet">
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="webcam/bootstrap.min.css">
    <link rel="stylesheet" href="webcam/layout.css">
    <link rel="stylesheet" href="webcam/font-awesome.min.css">  
    <link rel="stylesheet" href="webcam/cropper.css">
      <style>

.frmSearch {margin: 2px 0px;border-radius:4px;}
#country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;z-index: 9;}
#country-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;z-index: 9;}
#country-list li:hover{background:#ece3d2;cursor: pointer;z-index: 9;}
#search-box{padding: 10px;border-radius:4px;}

.loader {
  
  margin:auto;
  position:fixed;
  z-index:  999;
  text-align: center;
  width: 100%;
  height: 100%;
  background: grey;
  opacity: 0.5;
  display: none;
}


</style>
  </head>
  <body  >
    <div class="loader" >
        <img src="images/loader.gif" style="margin-top: 300px;" width="100" height="100">
    </div>
    <section class=" container-fluid  ">
      <header class="row  innerpage_header ">
        
        <h2 class="">Visitor registration </h2>
        
        
      </header>
      <div class="container">
        <diV class="row  align-items-center justify-content-center">
          
          <h1 class="title_heading">Please fill up your information</h1>
          
          <form class="row align-items-center justify-content-center" id="slider_form" method="post" action="" enctype="multipart/form-data">
            <div id="result"></div>
            <div class="form-group col-sm-10 col-10">
              <label for="usr">Name: </label>
              <input type="text" class="form-control input_style validate[required]" id="vist_name" data-errormessage-value-missing="Enter your name." value="<?php echo $user_data['visitor_name']; ?>">
            </div>
            <input type="hidden" value="<?php echo $_SESSION['register']['number']; ?>" id="vist_mobile">
            <!-- <div class="form-group col-sm-10  col-10">
              <label for="usr">Mobile No:</label>
              <input type="text" class="form-control input_style validate[required, custom[onlyNumberSp],minSize[10],maxSize[10]]" maxlength="10" id="vist_mobile" data-errormessage-value-missing="Mobile number is required!" data-errormessage-value-missing="Mobile number is required!"  value="<?php echo $user_data['mobile']; ?>">
            </div> -->
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Email Id:</label>
              <input type="text" class="form-control input_style validate[required]" id="vist_email" data-errormessage-value-missing="Email id is required!" value="<?php echo $user_data['email_id']; ?>">
            </div>
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Coming from:</label>
              <input type="text" class="form-control input_style validate[required]" id="vist_comming_from" value="<?php echo $user_data['coming_from']; ?>">
            </div>
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Your Organisation:</label>
              <input type="text" class="form-control input_style validate[required]" id="vist_org" value="<?php echo $user_data['visitor_org']; ?>">
            </div>
        <div class="form-group col-sm-10  col-10">
              <label for="usr">Upload your photo:</label>
              <input type="file" accept="image/*" class="form-control input_style" id="multiFiles">
            </div>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <div class="form-group col-sm-10  col-10">
   <label for="usr">Webcam:</label></div><br>
<div class="form-group col-sm-10  col-10">
   
      <style>
        #my_camera{
            width: 311px;
            height: 240px;
            border: 1px solid #ebebeb;
        }
      </style>

<input type="hidden" id="webcamcode">
 <div id="my_camera" class="col-sm-4  col-4" style="float: left;
    display: inline-block;"></div> <div id="results" class="col-sm-4  col-4" style="float: left;
    display: inline-block;"></div>
    <div class=" col-sm-4  col-4" style="float: left;
    display: inline-block;">
 <input type=button class="btn btn-primary" value="Webcam" onClick="configure()" style="margin-left:20px;">
 <input type=button class="btn btn-danger" value="Take Snapshot" onClick="take_snapshot()"></div>

 
 
 
        </div>
      <script language="JavaScript">
 
 // Configure a few settings and attach camera
 function configure(){
    Webcam.set({
       width: 311,
       height: 240,
       image_format: 'jpeg',
       jpeg_quality: 90
    });
    Webcam.attach( '#my_camera' );
 }
 // A button for taking snaps


 // preload shutter audio clip
 var shutter = new Audio();
 shutter.autoplay = false;
 shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';

 function take_snapshot() {
    // play sound effect
    shutter.play();

    // take snapshot and get image data
    Webcam.snap( function(data_uri) {
       // display results in page
       document.getElementById('results').innerHTML = 
           '<img id="imageprev" src="'+data_uri+'"/>';

     } );
    saveSnap();
     Webcam.reset();
 }

function saveSnap(){
   // Get base64 value from <img id='imageprev'> source
   var base64image = document.getElementById("imageprev").src;

   Webcam.upload( base64image, 'uploadnew.php', function(code, text) {
        console.log('Save successfully');
       document.getElementById("webcamcode").value=text;
   });

}
</script>
          
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Host organisation:</label>
              
			   <select class="form-control input_style validate[required]" id="host_org"  
                onChange="getEmployee(this.value);">
                      <option value="">Select Organization</option>
                      <?php
                      /*SELECT `id`, `tenant_name`, `tenant_email`, `tenant_contact`, `status`, `add_info_date`, `update_info_date` FROM `tenants` WHERE 1*/
                      $where2=array("status"=>'1');
                      $variable2=$obj_curd->display_all_record("tenants",$where2);
                      foreach ($variable2 as $row2) {
                         
                        ?>
                        <option value="<?php echo $row2['id'];?>"><?php echo $row2['tenant_name'];?></option>
                        <?php
                      }
                      ?>
                    </select>
            </div>
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Whom you are visiting:</label>
          
               <div class="frmSearch">
<input type="text" id="search-box" class="form-control input_style validate[required]" placeholder="" />
<div id="suggesstion-box"></div>
            </div></div>
              <div class="form-group col-sm-10  col-10">
              <label for="usr">Person Whom You Meet Mobile Number:</label>
              <input type="text" class="form-control input_style validate[required]" id="persons_count">
            </div>
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Your purpose of visit:</label>
              <input type="text" class="form-control input_style validate[required]" id="visit_purpose">
            </div>
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Vehicle number:</label>
              <input type="text" class="form-control input_style " id="vehicle_no">
            </div>
            <div class="form-group col-sm-10  col-10 ">
              <label for="usr">Make signature:</label>
              
              
              <div class="typed"></div>
              <canvas class="pad"  height="150" style="display: block; background: #fbfbfb; width: 300px; border:1px solid #ccc;"></canvas>
              <input type="hidden" name="output" class="output" id="sign_output">
              
            </div>
            <div class="form-group col-sm-10  col-10" id="finalsubmit" >
              <button  type="submit" class="btn btn-primary sub_testi">Sign In</button>
            </div>
          </form>
        </div>
      </section>
      <!--Script -->
     <!-- <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>-->
      
      <script src="js/jquery.signaturepad.js"></script>
      <script src="js/jquery.validationEngine.min.js" type="text/javascript" charset="utf-8"></script>
      <script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
    <!-- <script type="text/javascript" src="js/bootstrap.min.js"></script>-->
     
      
         <script>
      
      jQuery(document).ready(function () {
      $('#slider_form').signaturePad({drawOnly:true});
      
      });
      
      </script>
    
      
      <script>
        $(document).ready(function() {
            $("#search-box").keyup(function() {
                var val2 = document.getElementById("host_org").value;
                var keyLength = $(this).val().length;
                if (keyLength >= 3) {
                    $.ajax({
                        type: "POST",
                        url: "reademployee.php",
                        data: 'company_id=' + val2 + '&keyword=' + $(this).val(),
                        beforeSend: function() {
                            $("#search-box").css("background", "#FFF url(images/LoaderIcon.gif) no-repeat 165px");
                        },
                        success: function(data) {

                            $("#suggesstion-box").show();
                            $("#suggesstion-box").html(data);
                            $("#search-box").css("background", "#FFF");
                        }
                    });
                }

            });
        });

        function selectCountry(val) {
            //alert(val);
            $("#search-box").val(val);
            $("#suggesstion-box").hide();
        } 
      </script> 

      <script type = "text/javascript" >
            $('.sub_testi').on('click', function(e) {
                e.preventDefault();
                if (!$("#slider_form").validationEngine('validate', {
                        promptPosition: "inline",
                        scroll: false
                    })) {
                    return false;
                }

                $("#upload").css("display", "none");
                $("#loader").css("display", "block");
                var form_data = new FormData();
                var ins = document.getElementById('multiFiles').files.length;

                for (var x = 0; x < ins; x++) {
                    form_data.append("files[]", document.getElementById('multiFiles').files[x]);

                }
                form_data.append("vist_name", document.getElementById('vist_name').value);
                form_data.append("vist_mobile", document.getElementById('vist_mobile').value);
                form_data.append("vist_email", document.getElementById('vist_email').value);
                form_data.append("vist_comming_from", document.getElementById('vist_comming_from').value);
                form_data.append("vist_org", document.getElementById('vist_org').value);
                form_data.append("persons_count", document.getElementById('persons_count').value);
                form_data.append("host_org", document.getElementById('host_org').options[document.getElementById('host_org').selectedIndex].value);
                form_data.append("host_person", document.getElementById('search-box').value);


                form_data.append("sign_output", document.getElementById('sign_output').value);
                form_data.append("visit_purpose", document.getElementById('visit_purpose').value);
                form_data.append("vehicle_no", document.getElementById('vehicle_no').value);
                form_data.append("webcamcapture", document.getElementById('webcamcode').value);
                    
                if (ins != 0 || document.getElementById('webcamcode').value != '') {

                    $(".loader").show();
                    $.ajax({
                        url: 'sign_in_conf.php',
                        dataType: 'text',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function(response) {
                          $(".loader").hide();
                            // $("#result").html(response);
                            //alert(response.trim());
                            if (response.trim() == '1') {

                                document.body.scrollTop = 0; // For Safari
                                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                                $("#result").addClass("alert alert-success fade show");
                                $("#result").html("Visitor information added successfuly!");
                                window.location.replace("thanks.php");
                            } else if (response.trim() == '4') {
                                alert("Person Whom You Visit Mobile Number is not valid.Please must enter valid number");
                            } else if (response.trim() == '5') {
                                alert("The name of the person you want to meet is wrong");
                            } else {
                                $("#result").addClass("alert alert-danger fade show");
                                $("#result").html("Please update proper data.");
                            }
                            $('#description').val('');
                            $(".alert").delay(2000).slideUp(250, function() {
                                $(this).alert('close');
                            });

                            // location.reload();
                        },
                        error: function(response) {
                          $(".loader").hide();
                            //alert(response);

                            // location.reload();
                        }
                    });
                } else {
                    alert("Please Upload Your Image or Capture Your Image");
                }

            });

        function getEmployee(val) {

            $.ajax({
                type: "POST",
                url: "get-employee.php",
                data: 'company_id=' + val,
                beforeSend: function() {
                    $("#shost_person").addClass("loader");
                },
                success: function(data) {
                    $("#host_person").html(data);
                    $("#host_person").removeClass("loader");
                }
            });
        }
      </script>
      <!--Script -->


    
    </body>
  </html>