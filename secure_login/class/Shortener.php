<?php
/** 
 * Class to create short URLs and decode shortened URLs
 * 
 * @author CodexWorld.com <contact@codexworld.com> 
 * @copyright Copyright (c) 2018, CodexWorld.com
 * @url https://www.codexworld.com
 */ 
class Shortener
{
    protected static $chars = "abcdfghjkmnpqrstvwxyz|ABCDFGHJKLMNPQRSTVWXYZ|0123456789";
    protected static $table = "short_urls";
    protected static $checkUrlExists = false;
    protected static $codeLength = 7;

    protected $pdo;
    protected $timestamp;

    public function __construct(PDO $pdo){
        $this->pdo = $pdo;
        $this->timestamp = date("Y-m-d H:i:s");
    }

    public function getUrlCode($id) {
        if(empty($id)){
            throw new Exception("No id was supplied.");
        }

        $shortCode = $this->createShortCode();
        $id = $this->insertUrlInDB($id, $shortCode);

        return $shortCode;
    }

    protected function createShortCode(){
        $shortCode = $this->generateRandomString(self::$codeLength);
        if($this->codeExistsInDB($shortCode)) {
            return $this->createShortCode();
        }
        return $shortCode;
    }

    protected function codeExistsInDB($code){
        $query = "SELECT short_code FROM ".self::$table." WHERE short_code = :short_code AND status = 0 LIMIT 1";
        $stmt = $this->pdo->prepare($query);
        $params = array(
            "short_code" => $code
        );
        $stmt->execute($params);

        $result = $stmt->fetch();
        return (!empty($result)) ? true : false;
    }
    
    protected function generateRandomString($length = 6){
        $sets = explode('|', self::$chars);
        $all = '';
        $randString = '';
        foreach($sets as $set){
            $randString .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++){
            $randString .= $all[array_rand($all)];
        }
        $randString = str_shuffle($randString);
        return $randString;
    }

    protected function insertUrlInDB($url, $code){
        $query = "INSERT INTO ".self::$table." (long_url, short_code, created) VALUES (:long_url, :short_code, :timestamp)";
        $stmnt = $this->pdo->prepare($query);
        $params = array(
            "long_url" => $url,
            "short_code" => $code,
            "timestamp" => $this->timestamp
        );
        $stmnt->execute($params);

        return $this->pdo->lastInsertId();
    }
    
    public function getCodeId($code) {
        if(empty($code)){
            throw new Exception("No code was supplied.");
        }

        if($this->validateShortCode($code) == false){
            throw new Exception("Short code does not have a valid format.");
        }

        $urlRow = $this->getIdFromDB($code);
        if(empty($urlRow)){
            throw new Exception("Short code does not appear to exist.");
        }

        return $urlRow;
    }

    protected function getIdFromDB($code){
        $query = "SELECT long_url FROM ".self::$table." WHERE short_code = :short_code AND status = 0 LIMIT 1";
        $stmt = $this->pdo->prepare($query);
        $params=array(
            ":short_code" => $code
        );
        $stmt->execute($params);

        $result = $stmt->fetchColumn();
        return (empty($result)) ? false : $result;
    }

    protected function validateShortCode($code){
        $rawChars = str_replace('|', '', self::$chars);
        return preg_match("|[".$rawChars."]+|", $code);
    }
}