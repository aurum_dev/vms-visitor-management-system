<?php
 

class Database
{
    public $con;
    public $pdoConn;

    //public $base_url = "https://qparc.in/visitor/";
    //public $base_url = "https://qparc.in/visitor_test/";
    public $base_url = "https://vsitr_uat.recx.in/visitor1/";
    //public $base_url = "http://localhost/vms-visitor-management-system/";


    public function __construct()
    {
      //  $this->con=mysqli_connect("dev-database.cgqiqpdtvxo2.ap-south-1.rds.amazonaws.com","aurumdevdatabase","Aurumdev1database","aurumdevdatabase");
        //$this->con=mysqli_connect("database-1.cluster-cgqiqpdtvxo2.ap-south-1.rds.amazonaws.com","prodaurumdb","Aurumdev1database","prodaurum");

        //UAT
        $this->con=mysqli_connect("uatdatabase.cgqiqpdtvxo2.ap-south-1.rds.amazonaws.com","uatdbadmin","VisitorAurumuat","uatdatabase");

        //LOCAL
        //$this->con=mysqli_connect("localhost","root","","visitor");
        
        $this->pdoConnection();
    }

    public function pdoConnection() {
      try {
        //PROD
        //$this->pdoConn = new PDO("mysql:host=database-1.cluster-cgqiqpdtvxo2.ap-south-1.rds.amazonaws.com;dbname=prodaurum", "prodaurumdb", "Aurumdev1database");
      
        //UAT
        $this->pdoConn = new PDO("mysql:host=uatdatabase.cgqiqpdtvxo2.ap-south-1.rds.amazonaws.com;dbname=uatdatabase", "uatdbadmin", "VisitorAurumuat");

        //LOCALHOST
        //$this->pdoConn = new PDO("mysql:host=localhost;dbname=visitor", "root", "");

        // set the PDO error mode to exception
        $this->pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "Connected successfully"; die();
      } catch(PDOException $e) {
        echo "PDO Connection failed: " . $e->getMessage(); die();
      }
    }

} 
?>