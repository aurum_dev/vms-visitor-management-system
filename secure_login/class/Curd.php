<?php
    date_default_timezone_set('Asia/Kolkata'); 
    error_reporting(1);
    include 'DbConfig.php';
    include("phpmailer/class.phpmailer.php");
    include __DIR__.'/Shortener.php';

    include __DIR__.'/../../vendor/phpqrcode/qrlib.php';

    class Curd extends Database {
        // private $encryption_key = hex2bin("d989250ea63f218b14ea7537b7f1d82e778b32dee4f6b3dc6ec696da67a42b3e");
        // private $iv = hex2bin("c1fefe9b79ae94910a9232855283ab2f");

        private $encryption_key = "d989250ea63f218b14ea7537b7f1d82e778b32dee4f6b3dc6ec696da67a42b3e";
        private $iv = "c1fefe9b79ae94910a9232855283ab2f";

        public function add_record($table, $records) {
            $keys = implode(",", array_keys($records));
            $values = implode("','", array_values($records));
            $sql = "INSERT INTO " . $table . "(" . $keys . ") values ('" . $values . "')";
            $query = mysqli_query($this->con, $sql);
            return true;
        }

        public function add_record1($table, $records) {
            $keys = implode(",", array_keys($records));
            $values = implode("','", array_values($records));
            $sql = "INSERT INTO " . $table . "(" . $keys . ") values ('" . $values . "')";
            $query = mysqli_query($this->con, $sql);
            return mysqli_insert_id($this->con); die();
            //return true;
        }

        /*  CHECK RECORDS EXIST COUNT */
        public function check_record($table, $field1,$value1,$field2,$value2) {
            $sql = "select * from $table where $field1='$value1' || $field2='$value2'";
            $query=mysqli_query($this->con,$sql);
            $result = mysqli_num_rows($query);
        return $result;
        }

        public function display_all_Tenants($table, $where) {
            $sql = '';
            $condition = '';
            $userid=$_SESSION['mem_id'];
            $sql = "select * from " . $table ." where tenant_id='".$userid."'";
            $query = mysqli_query($this->con, $sql);
            while ($row = mysqli_fetch_assoc($query)) {
                $array[] = $row;
            }
            return $array;
        }

        /*Update records*/
        public function update_records($table, $where, $fields) {
            $sql = '';
            $condition = '';
            foreach ($where as $key => $value) {
                $condition.= $key . "='" . $value . "' and ";
            }
            $condition = substr($condition, 0, -4);
            foreach ($fields as $key => $value) {
                $sql.= $key . "='" . $value . "',";
            }
            $sql = substr($sql, 0, -1);
            $sql = "Update " . $table . " set " . $sql . " where " . $condition;
            $query = mysqli_query($this->con, $sql);
            return true;
        }
        /*Update records*/

        /*Display all records*/
        public function display_all_record($table, $where) {
            $sql = '';
            $condition = '';
            $array = array();
            if ($where != '') {
                foreach ($where as $key => $value) {
                    $condition.= $key . "='" . $value . "' and ";
                }
                $condition = " where " . substr($condition, 0, -5);
            } else {
                $condition = "";
            }
            $sql = "select * from " . $table . $condition ." order by id desc";
            $query = mysqli_query($this->con, $sql);
            while ($row = mysqli_fetch_assoc($query)) {
                $array[] = $row;
            }
            return $array;
        }
        /*Display all records*/
        
        
        /*Display all Like records*/
        public function display_all_like_record($table, $where, $like) {
            $sql = '';
            $condition = '';
            $array = array();
            if ($where != '') {
                foreach ($where as $key => $value) {
                    $condition.= $key . "='" . $value . "' and ";
                }
                if ($like != '') {
            foreach ($like as $key1 => $value1) {
                    $condition.= $key1 . " like '" . $value1 . "%' and ";
                }
            }
                
                
                $condition = " where " . substr($condition, 0, -5);
            } else {
                $condition = "";
            }
            $sql = "select * from " . $table . $condition;
            $query = mysqli_query($this->con, $sql);
            
            while ($row = mysqli_fetch_assoc($query)) {
                $array[] = $row;
            }
            return $array; 
        }
        /*Display all records*/
        
        /*Display all records between*/
        public function display_all_between_record($table, $comapre_col, $start_date, $end_date, $where, $order_id, $order) {
            $sql = '';
            $condition = '';
            $array = array();
            if ($where != '') {
                foreach ($where as $key => $value) {
                    $condition.= $key . "='" . $value . "' and ";
                }
                if ($like != '') {
            foreach ($like as $key1 => $value1) {
                    $condition.= $key1 . " like '" . $value1 . "%' and ";
                }
            }
                
                
                $condition = " where " . substr($condition, 0, -5) .'and';
            } else {
                $condition = "";
            }
            echo $sql = "SELECT * FROM ".$table." ".$condition." ".$comapre_col." >= " . strtotime($start_date) . " AND ".$comapre_col." <= " . strtotime($end_date) . " ORDER by ".$order_id." ".$order."";
            $query = mysqli_query($this->con, $sql);
            
            while ($row = mysqli_fetch_assoc($query)) {
                $array[] = $row;
            }
            return $array; 
        }
        /*Display all records between*/
        
        /*Display all records*/
        public function display_all_record_group_by($table, $where, $group_col) {
            $sql = '';
            $condition = '';
            $array = array();
            if ($where != '') {
                foreach ($where as $key => $value) {
                    $condition.= $key . "='" . $value . "' and ";
                }
                $condition = " where " . substr($condition, 0, -5);
            } else {
                $condition = "";
            }
            if ($group_col != '') {
                $group_by = " group by " . $group_col;
            } else {
                $group_by = '';
            }
            $sql = "select * from " . $table . $condition . $group_by;
            $query = mysqli_query($this->con, $sql);
            while ($row = mysqli_fetch_assoc($query)) {
                $array[] = $row;
            }
            return $array;
        }
        /*Display all records*/
        /*Delete record*/
        public function delete_record($table, $id) {
        echo  $sql = "DELETE FROM " . $table . " WHERE id='" . $id . "'";
            $query = mysqli_query($this->con, $sql);
            return true;
        }
        /*delete record*/
        
        /*Get max record number*/
        public function get_max_count($table, $where) {
            $sql = '';
            $condition = '';
        
            if ($where != '') {
                foreach ($where as $key => $value) {
                    $condition.= $key . "='" . $value . "' and ";
                }
                $condition = " where " . substr($condition, 0, -5);
            } else {
                $condition = "";
            }
        $sql = "select * from " . $table . $condition;
            $query = mysqli_query($this->con, $sql);
            $count = mysqli_num_rows($query);
            return $count;
        }
        /*Display all records*/
        
        
        /*image1*/function resizeImage($resourceType,$image_width,$image_height) {    $resizeWidth = 100;    $resizeHeight = 100;    $imageLayer = imagecreatetruecolor($resizeWidth,$resizeHeight);    imagecopyresampled($imageLayer,$resourceType,0,0,0,0,$resizeWidth,$resizeHeight, $image_width,$image_height);    return $imageLayer;}
        public function img_new_fun() {
            $files_list = '';
            $no_files = count($_FILES["files"]['name']);
            for ($i = 0;$i < $no_files;$i++) {
                if ($_FILES["files"]["error"][$i] > 0) {
                    echo "Error: " . $_FILES["files"]["error"][$i] . "<br>";
                } else {
                    $file_name = $_FILES["files"]["name"][$i];
                    $file_name = rand() . $file_name;

                if(move_uploaded_file($_FILES["files"]["tmp_name"][$i], 'secure_login/uploads/' . $file_name))
                {
                    $files_list.= 'uploads/' . $file_name;
                }
                else
                {
                    $files_list.= '';
                }
                }
            }
            return $files_list;
        }
        /*image1*/
        /*image2*/
        public function img_new_fun2() {
            $files_list = '';
            $no_files = count($_FILES["files2"]['name']);
            for ($i = 0;$i < $no_files;$i++) {
                if ($_FILES["files2"]["error"][$i] > 0) {
                    echo "Error: " . $_FILES["files2"]["error"][$i] . "<br>";
                } else {
                    $file_name = $_FILES["files2"]["name"][$i];
                    $file_name = rand() . $file_name;
                    move_uploaded_file($_FILES["files2"]["tmp_name"][$i], 'secure_login/uploads/' . $file_name);
                    $files_list.= 'uploads/' . $file_name . ', ';
                }
            }
            return $files_list;
        }
        /*image2*/
        /*image3*/
        public function img_new_fun3() {
            $files_list = '';
            $no_files = count($_FILES["files3"]['name']);
            for ($i = 0;$i < $no_files;$i++) {
                if ($_FILES["files3"]["error"][$i] > 0) {
                    echo "Error: " . $_FILES["files3"]["error"][$i] . "<br>";
                } else {
                    $file_name = $_FILES["files3"]["name"][$i];
                    $file_name = rand() . $file_name;
                    move_uploaded_file($_FILES["files3"]["tmp_name"][$i], 'uploads/' . $file_name);
                    $files_list.= 'uploads/' . $file_name . ', ';
                }
            }
            return $files_list;
        }
        /*image3*/
        /*image4*/
        public function img_new_fun4() {
            $files_list = '';
            $no_files = count($_FILES["files4"]['name']);
            for ($i = 0;$i < $no_files;$i++) {
                if ($_FILES["files4"]["error"][$i] > 0) {
                    echo "Error: " . $_FILES["files4"]["error"][$i] . "<br>";
                } else {
                    $file_name = $_FILES["files4"]["name"][$i];
                    $file_name = rand() . $file_name;
                    move_uploaded_file($_FILES["files4"]["tmp_name"][$i], 'uploads/' . $file_name);
                    $files_list.= 'uploads/' . $file_name . ', ';
                }
            }
            return $files_list;
        }
        /*image4*/
        
        /*image_upload_mobile*/
        public function img_new_fun_mob() {
            $files_list = '';
            $no_files = count($_FILES["files"]['name']);
            for ($i = 0;$i < $no_files;$i++) {
                if ($_FILES["files"]["error"] > 0) {
                    echo "Error: " . $_FILES["files"]["error"] . "<br>";
                } else {
                    $file_name = $_FILES["files"]["name"];
                    $file_name = rand() . $file_name;
                    move_uploaded_file($_FILES["files"]["tmp_name"], '../../ctrl_login/uploads/' . $file_name);
                    $files_list.= 'uploads/' . $file_name . ', ';
                }
            }
            return $files_list;
        } 
        public function img_new_fun_mob2() {
            $files_list = '';
            $no_files = count($_FILES["files2"]['name']);
            for ($i = 0;$i < $no_files;$i++) {
                if ($_FILES["files2"]["error"] > 0) {
                    echo "Error: " . $_FILES["files2"]["error"] . "<br>";
                } else {
                    $file_name = $_FILES["files2"]["name"];
                    $file_name = rand() . $file_name;
                    move_uploaded_file($_FILES["files2"]["tmp_name"], '../../ctrl_login/uploads/' . $file_name);
                    $files_list.= 'uploads/' . $file_name . ', ';
                }
            }
            return $files_list;
        }
        public function img_new_fun_mob3() { 
            $files_list = '';
            $no_files = count($_FILES["files3"]['name']);
            for ($i = 0;$i < $no_files;$i++) {
                if ($_FILES["files3"]["error"] > 0) {
                    echo "Error: " . $_FILES["files3"]["error"] . "<br>";
                } else {
                    $file_name = $_FILES["files3"]["name"];
                    $file_name = rand() . $file_name;
                    move_uploaded_file($_FILES["files3"]["tmp_name"], '../../ctrl_login/uploads/' . $file_name);
                    $files_list.= 'uploads/' . $file_name . ', ';
                }
            }
            return $files_list;
        }
        /*image_upload_mobile*/
        /*Compress image*/
        function compress_image($source_url, $destination_url, $quality) {
            $info = getimagesize($source_url);
            if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
            elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
            elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
            imagejpeg($image, $destination_url, $quality);
            return $destination_url;
        }
        /*compress image*/
        /*password encryption*/
        public function safe_b64encode($string) {
            $data = base64_encode($string);
            $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
            return $data;
        }
        /*password encryption*/
        /*password_description*/
        public function safe_b64decode($string) {
            $data = str_replace(array('-', '_'), array('+', '/'), $string);
            $mod4 = strlen($data) % 4;
            if ($mod4) {
                $data.= substr('====', $mod4);
            }
            return base64_decode($data);
        }
        /*password_description*/
        public function encode($value) {
            if (!$value) {
                return false;
            }
            $text = $value;
            $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
            $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
            $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, '123', $text, MCRYPT_MODE_ECB, $iv);
            print_r($crypttext); die();
            return trim($this->safe_b64encode($crypttext));
        }
        public function decode($value) {
            if (!$value) {
                return false;
            }
            $crypttext = $this->safe_b64decode($value);
            $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
            $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
            $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, '123', $crypttext, MCRYPT_MODE_ECB, $iv);
            return trim($decrypttext);
        }
        /*get_rows_having*/
        public function display_data_having($table, $fields, $where, $having, $group, $num_count) {
            $sql = '';
            $condition = '';
            $array = array();
            if ($where != '') {
                foreach ($where as $key => $value) {
                    $condition.= $key . "='" . $value . "' and ";
                }
                $condition = " where " . substr($condition, 0, -5);
            } else {
                $condition = "";
            }
            if ($fields != '') {
                foreach ($fields as $fieldskey => $fieldsvalue) {
                    $fields_new.= $fieldskey . ", ";
                }
                $fields_new = substr($fields_new, 0, -2);
            } else {
                $fields_new = "";
            }
            $sql = "SELECT " . $fields_new . " FROM " . $table . " " . $condition . " GROUP BY " . $group . " HAVING COUNT(" . $having . ") >" . $num_count . "";
            $query = mysqli_query($this->con, $sql);
            while ($row = mysqli_fetch_assoc($query)) {
                $array[] = $row;
            }
            return $array;
        }
        /*get_rows*/


        /*send sms*/
        public function send_sms($message,$phone_numbers)
        {
            // Account details
            $apiKey = urlencode('4oP5/MYWob4-PvFvMzSuhj72Mggmw7wssP29KBoINm');
            
            // Message details
            $numbers = array($phone_numbers);
            $sender = urlencode('QORIZE');
            $message = rawurlencode($message);
        
            $numbers = implode(',', $numbers);
        
            // Prepare data for POST request
            $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
        
            // Send the POST request with cURL
            $ch = curl_init('https://api.textlocal.in/send/');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            
            // Process your response here
            echo $response;
        }
        /*send sms*/

        /*send mail*/
        /* public function send_mail($from,$to,$message,$subject)
        {
        
        

        

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <'.$from.'>' . "\r\n"; 

        mail($to,$subject,$message,$headers);
        }*/
        /*send mail*/


        /*Send SMTP mail*/
        public function send_secure_mail($to,$message,$subject)
        {
            
            $mail = new PHPMailer();
            $mail->CharSet = "UTF-8";
            $mail->IsSMTP();
            $mail->SMTPDebug = 1;
            $mail->Host = "smtp.gmail.com";
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = "tls";
            $mail->Port = 587;
            $mail->Username="orize.visitor@gmail.com"; //User email
            $mail->Password="snblxrrlopyoybgs"; //User Password
            $mail->From = "orize.visitor.management@gmail.com"; // From Mail
            $mail->FromName= 'Orize VMS';  //From Name
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body = $message;
            $mail->addAddress($to);
            $headers2 = "MIME-Version: 1.0" . "\r\n";
            $headers2 .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // More headers
            $headers2 .= 'From: <orize.visitor.management@gmail.com>';
            $mail->AddCustomHeader("MIME-Version: 1.0");
            $mail->AddCustomHeader("Content-Type: text/html; charset=ISO-8859-1");
            $mail->AddCustomHeader("From: <orize.visitor.management@gmail.com>");
            if(!$mail->send()){
            //echo "Mailer Error: " . $mail->ErrorInfo;
            }else{
            //echo "E-Mail has been sent";
            }
        }





        /*Send SMTP mail*/

        public function getEmployee($companyId,$tenantName){
            $sql = "SELECT * from tenants_users where tenant_id='$companyId' and user_name like '" .$tenantName. "%'";
            
                $query = mysqli_query($this->con, $sql);
                while ($row = mysqli_fetch_array($query)) {
                $data[]=$row['id']."###".$row['user_name'];
                }
                return $data;
            //print_r($rt);
        }

        public function executeRawQuery($query,$params="",$placeholder="") {
                // $query = "SELECT * FROM visitor_info WHERE id=?";
                // $params = '334';
                // $params1 = '1';
                // $param = array('334');
                // $placeholder = 'i';
                // $stmt = $this->con->prepare($query);
                // $stmt->bind_param('s', $params);
                // $stmt->execute();
                // $result = $stmt->get_result();
                $data = [];
                $sql = mysqli_query($this->con, $query);
            
                while ($row = mysqli_fetch_assoc($sql)) {
                    $data[] = $row;
                }
                
                return $data;
        }


        public function encrypt_data($data) {
            return $name = openssl_encrypt($this->pkcs7_pad($data, 16), 'AES-256-CBC', hex2bin($this->encryption_key), 0, hex2bin($this->iv));
        }

        public function decrypt_data($data) {
            return $name = $this->pkcs7_unpad(openssl_decrypt($data, 'AES-256-CBC', hex2bin($this->encryption_key), 0, hex2bin($this->iv)));
        }

        private function pkcs7_pad($data, $size)
        {
            $length = $size - strlen($data) % $size;
            return $data . str_repeat(chr($length), $length);
        }

        private function pkcs7_unpad($data)
        {
            return substr($data, 0, -ord($data[strlen($data) - 1]));
        }

        public function getHostByMemId($mem_id, $type="") {
            if($type == TENANT_ADMIN) {
                $qry = "SELECT t.id, t.tenant_name FROM tenants AS t  WHERE t.id = ?";
            }else if($type == TENANT_USER) {
                $qry = "SELECT t.id, t.tenant_name FROM tenants_users AS tu LEFT JOIN tenants AS t ON tu.tenant_id = t.id WHERE tu.id = ?";
            }
                
            $stmt = $this->con->prepare($qry);   
            $stmt->bind_param("i", $mem_id);
            $stmt->execute();
            $result = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return (!$result) ? 0 : $result;
        }

        public function getHost($type, $mem_id="") {
            
            if($type != 1) {
                return $this->getHostByMemId($mem_id);
            }else{
                $status  = 1;
                $qry = "SELECT id, tenant_name FROM tenants WHERE status = ?";    
                $stmt = $this->con->prepare($qry);   
                $stmt->bind_param("i", $status);
                $stmt->execute();
                $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
                $stmt->close();
                return (!$result) ? 0 : $result;
            }
        }

        public function getHostMemberByOrg($org_id) {
            $status = 1;
            $qry = "SELECT id, user_name FROM tenants_users WHERE status = ? AND tenant_id = ?";    
            $stmt = $this->con->prepare($qry);   
            $stmt->bind_param("ii", $status,$org_id);
            $stmt->execute();
            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
            $stmt->close();
            return (!$result) ? 0 : $result;   
        }

        public function createSelectOption($data,$key_name,$val_name) {
            $html = "";
            if($data) {
                foreach ($data as $key => $val) {
                    $html .= "<option value=".$val[$key_name].">".$val[$val_name]."</option>";
                }
            }
           return $html;
        }

        public function pdoInsertData($table, $records) {
            try{
                $arr_key = array_keys($records);
                $keys = implode(",", array_keys($records));
                $placeholder = [];
                for($i= 0; $i < count($records); $i++ ) {
                    $placeholder[$i] = ":".$arr_key[$i];
                }
                $placeholder = implode(",", $placeholder);
                
                $qry = "INSERT INTO " . $table . " (" . $keys . ") values (" . $placeholder . ")";
                //return $qry;
                $stmt = $this->pdoConn->prepare($qry);
                if($stmt !== FALSE) {
                    if($stmt->execute($records)){
                        return $this->pdoConn->lastInsertId();
                    }
                }

                return 0;
            }catch (Exception $e) {
                return $e->getMessage();
            }
        }

        public function pdoUpdatetData($table, $records, $action_col, $where="") {
            try{
                $keyval = [];
                foreach($records as $key => $val) {
                    if(in_array($key, $action_col)) {
                        $keyval[] = $key." = :".$key;
                    }
                }
                $keyval = implode(", ", $keyval);
                
                $qry = "UPDATE " . $table . " SET ".$keyval." WHERE ".$where;
                $stmt = $this->pdoConn->prepare($qry);
                if($stmt !== FALSE) {
                    if($stmt->execute($records)){
                        if($stmt->rowCount() > 0) {
                            return 1;
                        }
                    }
                }

                return 0;
            }catch (Exception $e) {
                return $e->getMessage();
            }
        }

        public function pdoGetData($table, $form_data, $where_col_name, $cust_where="") {
            try{
                $where = [];
                $params = [];
                foreach ($where_col_name as $key=> $val) {
                    $where[] = $val ." = :".$val;
                    $params[$val] = $form_data[$val];
                }
                $where =  implode(" AND ", $where);
                $qry = "SELECT * FROM ".$table." WHERE 1 AND ". $where.$cust_where;
                
                //return $qry;
                $stmt = $this->pdoConn->prepare($qry);
                if($stmt !== FALSE) {
                    if($stmt->execute($params)){
                        return $stmt->fetchAll(PDO::FETCH_ASSOC);
                    }
                }
                return false;
            }catch (Exception $e) {
                return $e->getMessage();
            }
        }
        

        public function sanitizeData($data) {
            $data = trim(strip_tags($data));
            return preg_replace("/[^A-Za-z0-9\-\ ]/", '', $data);
        }

        public function generateNumericOTP($n) {     
            // Take a generator string which consist of
            // all numeric digits
            $generator = "1357902468";  
            // Iterate for n-times and pick a single character
            // from generator and append it to $result      
            // Login for generating a random character from generator
            //     ---generate a random number
            //     ---take modulus of same with length of generator (say i)
            //     ---append the character at place (i) from generator to result
          
            $result = "";  
            for ($i = 1; $i <= $n; $i++) {
                $result .= substr($generator, (rand()%(strlen($generator))), 1);
            }  
            // Return result
            return $result;
        }

        public function createShortenerUrl($id) {
            try{
                $shortener = new Shortener($this->pdoConn);
                $shortCode = $shortener->getUrlCode($id);
                    
                // Create short URL
                $shortURL = $this->base_url.$shortCode;
                //$shortURL = $shortURL_Prefix.$shortCode;
                return $shortURL;
            }catch (Exception $e) {
                return $e->getMessage();
            }
            
        }

        public function getLongUrl($code) {
            try{
                $shortener = new Shortener($this->pdoConn);
                $id = $shortener->getCodeId($code);
                return $id;
            }catch (Exception $e) {
                return $e->getMessage();
            }
        }

        public function generateQrCode($id, $file_name = "qr_verification.php") {
            if(!empty($id)) {
                if(preg_match('/^[0-9]+$/', $id)) {
                    $PNG_QR_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR;
                    $PNG_WEB_DIR = 'uploads/';
                    if (!file_exists($PNG_QR_DIR))
                        mkdir($PNG_QR_DIR);    
                    $filename = $PNG_QR_DIR.'visitor_qr.png';
                    $errorCorrectionLevel = 'L';
                    $matrixPointSize = 4;
                    $id = $this->encrypt_data($id);
                    $text = $this->base_url.$file_name.'?'.$id;
                    QRcode::png($text, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
                    
                    return "class/".$PNG_WEB_DIR.basename($filename);
                }
            }
        }

        public function generatePass($id) {
            $status = 0;
            $html = "Invalid Visitor Id";
            if(isset($id) && !empty($id)) {
                if(preg_match('/^[0-9]+$/', trim($id))) {
                    $qry="SELECT vf.*, t.tenant_name, tu.user_name FROM visitor_info AS vf
                        LEFT JOIN tenants AS t ON vf.host_orgn = t.id
                        LEFT JOIN tenants_users AS tu ON tu.id = vf.host_name
                        WHERE vf.visitor_id = '".$id."' AND vf.pass_status = 1 ORDER BY id DESC LIMIT 1";
                    
                    $data = $this->executeRawQuery($qry);
                    if(count($data) > 0) {
                        $data = $data[0];
                        $current_date = strtotime(date('Y-m-d'));
                        $btn = '<h5><span class="badge badge-danger">Expired</span></h5>';
                        if($data['verified'] == 1) {
                            $verified = "../images/verified.jpg";
                            if($data['type'] == 'CUSTOMIZE') {
                                if($data['pass_status'] == 1) {
                                    $from_date = strtotime(date('Y-m-d',strtotime($data['from_date'])));
                                    $to_date = strtotime(date('Y-m-d',strtotime($data['to_date'])));
                                    if (($current_date >= $from_date) && ($current_date <= $to_date)){
                                        
                                        if(!empty($data['in_time'])) {
                                           
                                            if($current_date == strtotime(date('d-m-Y', strtotime($data['in_time']))) && !empty($data['out_time'])) {
                                                $btn_check = "recheckin";
                                            }else if($current_date == strtotime(date('d-m-Y', strtotime($data['in_time'])))) {
                                                $btn_check = "out";
                                            }else{
                                                $btn_check = "in";
                                            }
                                        }else{
                                            $btn_check = "in";
                                        } 
                                    }else if($current_date < $from_date) {
                                        $btn = '<h5><span class="badge badge-success">Pass valid from '.date('d-m-Y',strtotime($data['from_date'])).' to '.date('d-m-Y',strtotime($data['to_date'])).'</span><h5>';
                                    }
                                }else{
                                    $btn = '<h5><span class="badge badge-danger">Pass is not valid</span></h5>';
                                }
                                
                            }elseif($data['type'] == 'SCHEDULE') {
                                if($data['pass_status'] == 1) {
                                    $pass_date = date('Y-m-d', strtotime($data['in_time']));
                                    $pass_date = strtotime($pass_date);
                                    if ($current_date == $pass_date){
                                        $btn_check = "out";
                                    }else if($current_date < $pass_date) {
                                        $btn = '<h5><span class="badge badge-success">Meet Scheduled : '.date('d-m-Y H:i:s', strtotime($data['in_time'])).'</span></h5>';
                                    }else if($current_date > $pass_date) {
                                        $btn = '<h5><span class="badge badge-danger">Expired</span></h5>';
                                    }
                                }else{
                                    $btn = '<h5><span class="badge badge-danger">Pass is not valid</span></h5>';
                                }
                            }else {
                                if($current_date <= strtotime($data['in_time'])) {
                                    $btn_check = "out";
                                    $btn = "";
                                }
                            }
        
                            if($btn_check == 'in') {
                                $btn = '<button class="btn btn-primary check_in_out" data-id="'.$data['id'].'" data-value="in">CheckIn</button>';
                            }else if($btn_check == 'out'){
                                $btn = '<button class="btn btn-warning check_in_out" data-id="'.$data['id'].'" data-value="out">Check Out</button>';
                            }else if($btn_check == 'recheckin'){
                                $btn = '<button class="btn btn-warning check_in_out" data-id="'.$data['id'].'" data-value="in">Re-CheckIn</button>';
                            }
                        }else{
                            $btn = '<h5><span class="badge badge-danger">Not Verified</span></h5>';
                        }
                        $status = 1;
                        $qr_code = $this->generateQrCode($data['visitor_id'], "secure_login/visitor_check_in_out.php");
                        $html = '<div class="card col-md-12 card col-sm-12" id="printableArea">
                                    <table>
                                        <tr>
                                            <td class="visitor_bg" style="background: #000; color: #fff; font-weight: 700; font-size: 20px; line-height: 23px;padding: 10px 7px; " bgcolor="#000">V<br>I<br>S<br>I<br>T<br>O<br>R</td>
                                            <td style="padding: 15px">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            
                                                        </td>
                                                        <td class="text-left">
                                                        <ul style="font-size: 15px; margin-left: 15px;" class="list-unstyled">
                                                            <li><strong>Id:</strong>'.$data['visitor_id'].'</li>
                                                            <li><strong>Name: </strong>'.$data['visitor_name'].'</li>
                                                            <li><strong>Mobile: </strong>'.$data['mobile'].'</li>
                                                            <li><strong>From: </strong>'.$data['visitor_org'].', '.$data['coming_from'].'</li>
                                                            <li><strong>Host: </strong>'.$data['tenant_name'].'</li>
                                                            <li><strong>Host Member: </strong>'.$data['user_name'].'</li>
                                                            <li><strong>Purpose: </strong>'.$data['meeting_purpose'].'</li>
                                                            <li><strong>Intime: </strong>'.(!empty($data['in_time']) && $data['verified'] == 1 ? date('d-m-Y H:i:s', strtotime($data['in_time'])) : "").' </strong></li>
                                                            <li><strong>Outtime: </strong>'.(!empty($data['out_time']) && $data['verified'] == 1 ? date('d-m-Y H:i:s', strtotime($data['out_time'])) : "").' </strong></li>
                                                            <li>
                                                                <div class="mt-3">
                                                                    <img src="'.$qr_code.'" style="width:100px;"/> 
                                                                </div>
                                                            </li>
                                                        </ul>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="d-flex" style="flex-direction: column; align-items: center;">
                                                <div class="mt-3">
                                                    <img src="'.(!empty($data['photo']) ? $data['photo'] : "uploads/profile.png").'"  style="width: 150px; height: 160px;">
                                                </div>
                                                <div>
                                                    <img src="'.$verified.'" style="width:100px;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>';
    
                        $html = '<div class="card-body" style="padding:0px">'.$html.'</div><div class="card-footer" style="padding:0px">'.$btn.'</div>';        
                    }else{
                        $html = "Visitor ID not found";
                    }
                }
            }
            return $data = ['status' => $status, 'data' => $html];
        }

        public function checkPageAcces($page) {
            try{
                ;
                $res = $this->getHost($_SESSION['mem_type'], $_SESSION['mem_id']);
                if(!is_array($res[0])) {
                    $host_id = $res['id'];
                    $data = [
                        "user_id" => $_SESSION['mem_id'],
                        "host_id" => $host_id,
                        "role_id" => $_SESSION['mem_type'],
                        "access_page" => $page,
                        "status" => 1,
                    ];
                    $where_col = array_keys($data);
                    
                    $res = $this->pdoGetData("role_access", $data, $where_col);
                    
                    if(COUNT($res) > 0) {
                        return 1;
                    }
                }
                return 0;
            }catch(Exception $e){
                return $e->getMessage();
            }
        }

        public function display_all_recordCount($table, $where) {
            $sql = '';
            $condition = '';
            $array = array();
            if ($where != '') {
                foreach ($where as $key => $value) {
                    $condition.= $key . "='" . $value . "' and ";
                }
                $condition = " where " . substr($condition, 0, -5);
            } else {
                $condition = "";
            }
            $sql = "select COUNT(*) from " . $table . $condition ." order by id desc";
            $result = $this->con->query($sql);
            $row = $result->fetch_row();
            return $row[0];
        }
        

    }
    $obj_curd = new Curd;
?>