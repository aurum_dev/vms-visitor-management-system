<?php
include ('session_check.php');
$user_id = $_SESSION['user_id'];
    $mem_id = $_SESSION['mem_id'];
    $mem_type = $_SESSION['mem_type'];
include('class/Curd.php');

if(isset($_POST['delete_data']) && $_POST['delete_data'] == 1) {
  try{
      $id = $obj_curd->sanitizeData($_POST['id']);
      $status = 0;
      $msg = "Something went wrong";
      if(!empty($id)) {
          
          $data['pass_status'] = 0;
          $data['update_date'] = date('Y-m-d H:i:s');
          $action_col = array_keys($data);
          $data['visitor_id'] = $id;
          $where = "visitor_id = :visitor_id";
          
          $res = $obj_curd->pdoUpdatetData("visitor_info", $data, $action_col, $where);
          
          if($res == 1) {
              $status = 1;
              $msg = "Data Deleted Successfully";
          }else{
              $msg = $res;
          }
      }
      echo json_encode(array('status'=>$status, 'msg' => $msg)); die();
  }catch(Exception $e) {
      echo json_encode(array('status'=>0, 'msg' => $e->getMessage())); die();
  }
  
}

if(isset($_POST['getTabledata']) && $_POST['getTabledata'] == 1) {
  $where = $action = "";

  if($mem_type == TENANT_ADMIN) {
      $host_id = $obj_curd->getHost($mem_type, $mem_id); 
      $where = ' AND vf.host_orgn = '.$host_id;
  }else if($mem_type == TENANT_USER) {
      $where = ' AND vf.host_name = '.$mem_id;
  }
  $action_btn = false;

  if($mem_type == TENANT_ADMIN || $mem_type == ADMIN) {
      $action_btn = true;
  }
  $qry = "SELECT vf.*, t.tenant_name, tu.user_name FROM visitor_info AS vf
          LEFT JOIN tenants AS t ON vf.host_orgn = t.id
          LEFT JOIN tenants_users AS tu ON tu.id = vf.host_name
          WHERE vf.type = 'SCHEDULE' AND vf.pass_status = 1 ".$where." GROUP BY vf.visitor_id";
  
  $data = $obj_curd->executeRawQuery($qry);
  $html="";
  $cnt = 1;

  foreach($data as $key => $val) {
      if($action_btn) {
          $action = '<a data-id="'.$val['visitor_id'].'" class="delete_record"><i class="fas fa-trash text-danger"></i></a>';
      }
      $html .= '<tr>
                  <td>'.$cnt++.'</td>
                  <td>'.$val['visitor_id'].'</td>
                  <td>'.$val['visitor_name'].'</td>
                  <td>'.$val['email_id'].'</td>
                  <td>'.$val['mobile'].'</td>
                  <td>'.$val['tenant_name'].'</td>
                  <td>'.$val['user_name'].'</td>
                  <td>'.$val['meet_purpose'].'</td>
                  <td>'.(!empty($val['in_time']) ? date('d-m-y', strtotime($val['in_time'])) : "").'</td>
                  <td>'.(!empty($val['in_time']) && $val['verified'] == 1 ? date('H:i:s', strtotime($val['in_time'])) : "").'</td>
                  <td>'.(!empty($val['out_time']) && $val['verified'] == 1 ? date('H:i:s', strtotime($val['out_time'])) : "").'</td>
                  <td>'.($val['verified'] > 0 ? "<span class='badge badge-success'>Yes</span>" : "<span class='badge badge-danger'>No</span>").'</td>
                  <td>
                      <a href="id_display.php?id='.$val['id'].'"<i class="fas fa-eye"></i></a>
                      '.$action.'
                  </td>
              </tr>';
                                      
  }
  echo json_encode(array("data" => $html, "status" => 1)); die();
}
?>
<!DOCTYPE html>
<html>
<?php include('head.php');?>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <!-- Navbar -->
    <?php include('nav.php');?>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <?php include('side_menu.php');?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content"  style="padding: 0px">
          <div class="container-fluid">
            <div class="row">
               <h4 class="page-title ">View all visitors</h4>


    
               
                  <!-- /.card-header -->
                  <div class="col-sm-12">
                    <center><div id="result"></div></center>
                  </div>
                  <!-- form start -->
				  <div class="col-sm-12">
				  <form action="#" method="POST">
<div class="row" style="margin-bottom:25px;">

<div class="col-sm-5"> 
<input type="text" name="start_date" id="start_date" required placeholder="Start Date" class="form-control datetimepicker">
</div>
<div class="col-sm-5"> 
<input type="text" name="end_date" id="end_date"  placeholder="End Date" class="form-control datetimepicker">
</div>
<div class="col-sm-2"><button type="submit" class="btn btn-success">Submit</button></div>
 </div>
 </form>
 </div>
 <br>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <!-- SELECT `id`, `visitor_name`, `mobile`, `email_id`, `coming_from`, `visitor_org`, `photo`, `persons_alogn_with_visitor`, `host_orgn`, `host_name`, `meeting_purpose`, `vehicle_no`, `signature`, `in_time`, `out_time`, `verified`, `status`, `add_date`, `update_date`, `visitor_id` FROM `visitor_info` WHERE 1 -->
                    <tr> 
                      <th>Sr No</th>
                      <th>Visitor Id</th>
                      <th>Name</th>  
                      <th>Email</th>
                      <th>Mobile</th>  
                      <th>Host</th>
                      <th>Host Member</th>
                      <th>Meet Purpose</th>
                      <th>Date</th>  
                      <th>In Time</th>
                      <th>Out Time</th>  
                      <th>Status</th> 
                      <th>Action</th>  
                    </tr>
                  </thead>
                  <tbody>
                        
                  </tbody>
                </table>
              </div>
              
              <!-- /.card -->
            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include('Footer.php');?>
  </div>
  <!-- ./wrapper -->
  <?php include('script.php');?>
  <!-- delete -->
  <script type="text/javascript">
    getTabledata();
    async function getTabledata() {
      var data = {getTabledata: 1};
      try {
          $(".loader").show();
          var res = await ajaxRequest('view_All_visitors.php', data, 'POST', 'json');
          $(".loader").hide();
          if(res.status) {
              $("#example1").DataTable().clear().destroy();
              $("#example1").find('tbody').html(res.data);
              reinitializetable();
          }
      }catch(error) {
          $(".loader").hide();
          Swal.fire('Error', "Something Went wrong", 'error');
      }
    }

    $("#example1").on("click", ".delete_record", function() {
        var id = $(this).data('id');

        if(id != "") {
            var data = {delete_data: 1, id: id};
            Swal.fire({
                title: 'Do you want to delete this record?',
                showDenyButton: true,
                confirmButtonText: `Yes`,
                denyButtonText: `No`,
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    try {
                        $(".loader").show();
                        ajaxRequest('view_All_visitors.php', data, 'POST', 'json').then((res)=> {
                            $(".loader").hide();
                            if(res.status) {
                                Swal.fire('Success', res.msg, 'success');
                                getTabledata();    
                            }else{
                                Swal.fire('Error', "Something Went wrong", 'error');
                            }
                        });
                        
                    }catch(error) {
                        $(".loader").hide();
                        Swal.fire('Error', "Something Went wrong", 'error');
                    }
                }
            })
            
        }else{
            Swal.fire('Error', "Something Went wrong", 'error');
        }
    })

    $('.del_record').on('click', function () {
    var oprn1 = $(this).val();
      alert(oprn1);
       $.ajax({
        type: 'POST', 
        url: 'delete.php',
         
        data: { 'deltype' : 'vendor',   'del_record' : oprn1},
        type: 'post',
        success: function (response) {
      
          if(response='1')
          {

            $("#result").addClass("alert alert-success fade show");
            $("#result").html("Update information added successfuly ");
          }
          else
          {
            $("#result").addClass("alert alert-danger fade show");
            $("#result").html("Please update proper data.");
          }

          $(".alert").delay(2000).slideUp(250, function() {
            $(this).alert('close');
          });
        },
        error: function (response) {
          alert(response);

          location.reload();
        }
      });
    });
  </script>
  <!-- delete -->
</body>
</html> 