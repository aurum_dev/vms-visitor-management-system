<?php
    include ('session_check.php');
    $user_id = $_SESSION['user_id'];
    $mem_id = $_SESSION['mem_id'];
    $mem_type = $_SESSION['mem_type'];
    include ('class/Curd.php');
    require '../vendor/sms/Pinnacle.php';

    // $sms = new Pinnacle();
    // $url = urlencode('http://localhost/vms-visitor-management');
    // $resp = $sms->sendVerificationSms('12345', "https://qparc.in/visitor/vabbq2a", '7021098759');
    // print_r($resp); die; 

    if(isset($_POST['get_host_mem']) && $_POST['get_host_mem'] == 1) {
        $html="";
        if(isset($_POST['id']) && !empty($_POST['id'])) {
            $mem_data = $obj_curd->getHostMemberByOrg($_POST['id']);
            if(!is_array($mem_data[0])) {
                $mem_data = [$mem_data];
            }
            $html = $obj_curd->createSelectOption($mem_data, 'id','user_name');
        }

        echo json_encode(array('status'=>1, 'data' => $html)); die;
    }
    
    $page_access = $obj_curd->checkPageAcces('CUSTOMIZE_PASS');
    if($mem_type != ADMIN && $mem_type != SECURITY && $page_access != 1) {
        header("Location:dashboard.php"); die;
    }
    
    if(isset($_POST['getTabledata']) && $_POST['getTabledata'] == 1) {
        $where = $action = "";
    
        if($mem_type == TENANT_ADMIN) {
            $host_id = $obj_curd->getHost($mem_type, $mem_id); 
            $where = ' AND vf.host_orgn = '.$host_id;
        }else if($mem_type == TENANT_USER) {
            $where = ' AND vf.host_name = '.$mem_id;
        }
        
        
        $qry = "SELECT vf.*, t.tenant_name, tu.user_name FROM visitor_info AS vf
                LEFT JOIN tenants AS t ON vf.host_orgn = t.id
                LEFT JOIN tenants_users AS tu ON tu.id = vf.host_name
                WHERE vf.type = 'CUSTOMIZE' AND vf.pass_status = 1 ".$where." GROUP BY vf.visitor_id";
        
        $data = $obj_curd->executeRawQuery($qry);
        $html="";
        $cnt = 1;

        foreach($data as $key => $val) {
            if($val['verified'] != 1) {
                $action = '<a data-id="'.$val['visitor_id'].'" class="edit_record"><i class="fas fa-edit text-success"></i></a><a data-id="'.$val['visitor_id'].'" class="delete_record"><i class="fas fa-trash text-danger"></i></a>';
            }
            if($mem_type == TENANT_ADMIN || $mem_type == ADMIN) {
                if($val['verified'] == 1) {
                    $action = '<a data-id="'.$val['visitor_id'].'" class="delete_record"><i class="fas fa-trash text-danger"></i></a>';
                }
                
            }

            // <td>'.(!empty($val['in_time']) ? date('d-m-y', strtotime($val['in_time'])) : "").'</td>
            //             <td>'.(!empty($val['in_time']) ? date('H:i:s', strtotime($val['in_time'])) : "").'</td>
            //             <td>'.(!empty($val['out_time']) ? date('H:i:s', strtotime($val['out_time'])) : "").'</td>
            
            $html .= '<tr>
                        <td>'.$cnt++.'</td>
                        <td>'.$val['visitor_id'].'</td>
                        <td>'.$val['visitor_name'].'</td>
                        <td>'.$val['email_id'].'</td>
                        <td>'.$val['mobile'].'</td>
                        <td>'.$val['tenant_name'].'</td>
                        <td>'.$val['user_name'].'</td>
                        <td>'.(!empty($val['from_date']) ? date('d-m-y', strtotime($val['from_date'])) : "").'</td>
                        <td>'.(!empty($val['to_date']) ? date('d-m-y', strtotime($val['to_date'])) : "").'</td>
                        <td>'.($val['verified'] > 0 ? "<span class='badge badge-success'>Yes</span>" : "<span class='badge badge-danger'>No</span>").'</td>
                        <td>
                            <a href="id_display.php?id='.$val['id'].'"<i class="fas fa-eye"></i></a>
                            '.$action.'
                        </td>
                    </tr>';
                                            
        }
        echo json_encode(array("data" => $html, "status" => 1)); die();
    }

    if(isset($_POST['delete_data']) && $_POST['delete_data'] == 1) {
        try{
            $id = $obj_curd->sanitizeData($_POST['id']);
            $status = 0;
            $msg = "Something went wrong";
            if(!empty($id)) {
                
                $data['pass_status'] = 0;
                $data['update_date'] = date('Y-m-d H:i:s');
                $action_col = array_keys($data);
                $data['visitor_id'] = $id;
                $where = "visitor_id = :visitor_id";
                
                $res = $obj_curd->pdoUpdatetData("visitor_info", $data, $action_col, $where);
                
                if($res == 1) {
                    $status = 1;
                    $msg = "Data Deleted Successfully";
                }else{
                    $msg = $res;
                }
            }
            echo json_encode(array('status'=>$status, 'msg' => $msg)); die();
        }catch(Exception $e) {
            echo json_encode(array('status'=>0, 'msg' => $e->getMessage())); die();
        }
        
    }

    if(isset($_POST['edit_data']) && $_POST['edit_data'] == 1) {
        try{
            $id = $obj_curd->sanitizeData($_POST['id']);
            $status = 0;
            $data = "";
            $msg = "Something went wrong";
            if(!empty($id)) {
                $data = ['visitor_id' => $id];
                $res = $obj_curd->pdoGetData('visitor_info', $data, ['visitor_id']);
                if(COUNT($res) > 0) {
                    $status = 1;
                    $msg = "";
                    $data = $res[0];
                }
            }
            echo json_encode(array('status'=>$status, 'msg' => $msg, 'data' => $data)); die();
        }catch(Exception $e) {
            echo json_encode(array('status'=>0, 'msg' => $e->getMessage())); die();
        }
        
    }

    

    if(isset($_POST['submit_customize_pass']) && $_POST['submit_customize_pass'] == 1) {
        try{
            if(empty($_POST['id'])) {
                unset($_POST['id']);
            }
            $status = 0;
            //print_r($_FILE['aadhar_file']); die;
            $err = []; $msg = $data = ""; $exist= false;
            unset($_POST['submit_customize_pass']);
            foreach($_POST as $key => $val) {
                if($key == 'email_id') {
                    if(!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i', $val)) {
                        $err['email_id'] = 'Email Id is invalid';
                    }else{
                        $value = trim(strip_tags($val));
                    }
                }else{
                    $value = $obj_curd->sanitizeData($val);
                }
                
                if(!empty($value)) {
                    $form_data[$key] = $value;
                }else{
                    $err[$key] = 'This field is Required.';
                }
            }
            if(isset($form_data['id']) && !empty($form_data['id'])) {
                $exist = $obj_curd->pdoGetData('visitor_info', ['visitor_id' => $form_data['id']], ['visitor_id']);
                if(COUNT($exist) > 0) {
                    $exist = true;
                }
            }else{
                if(empty($_FILES['aadhar_file'])) {
                    $err['aadhar_file'] = 'This field is Required.';
                }elseif( UPLOAD_ERR_OK !== $_FILES['aadhar_file']['error'] ){
                    $err['aadhar_file'] = 'File cannot be upload.';
            
                } elseif( $_FILES['aadhar_file']['size'] > 3145728 ){
                    $err['aadhar_file'] = 'File must be less than 3 mb.';
                } else {
                    $allowed = array('pdf', 'jpeg', 'jpg');
                    $filename = $_FILES['aadhar_file']['name'];
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if (!in_array($ext, $allowed)) {
                        $err['aadhar_file'] = 'File Must be PDF,JPG or JPEG.';
                    }
                }
                $otpCode = $obj_curd->generateNumericOTP(6);
                $form_data['created_at'] = date('Y-m-d H:i:s');
                $form_data['otp_code'] = $otpCode;
                $form_data['visitor_id'] = date('ymd', time()) . mt_rand(1, 999);
            }
            
            if(!empty($err)) {
                echo json_encode(array('status'=>$status, 'data' => $err, 'msg' => $msg)); die();
            }
            $frmdate = new DateTime($form_data['from_date']);
            $todate = new DateTime($form_data['to_date']);
            //comapare to date greater or equal to from date


            if($todate < $frmdate) {
                $err['to_date'] = "To Date must be greater than From Date";
                echo json_encode(array('status'=>$status, 'data' => $err, 'msg' => $msg)); die();
            }
            if($frmdate->diff($todate)->m + ($frmdate->diff($todate)->y*12) > 3) {
                $err['from_date'] = "Customiz Date is applicable for 3 months only";
                echo json_encode(array('status'=>$status, 'data' => $err, 'msg' => $msg)); die();
            }

            if($mem_type == TENANT_USER) {
                $form_data['host_name'] = trim($mem_id);
                $form_data['host_orgn'] = trim($obj_curd->getHostByMemId($mem_id, $mem_type)['id']);
            }
            
            $form_data['type'] = 'CUSTOMIZE';
            $form_data['pass_status'] = 1;
            $form_data['verified'] = 0;
            $form_data['from_date'] = date('Y-m-d H:i:s', strtotime($form_data['from_date']));
            $form_data['to_date'] = date('Y-m-d H:i:s', strtotime($form_data['to_date']));
            $form_data['created_by'] = $mem_id;
            $form_data['status'] = 1;
            

            // $where = "WHERE mobile = :mobile AND email_id = :email_id AND host_orgn = :host_orgn AND host_name = :host_name And status = :status AND type = :type";
            if(isset($form_data['id']) && !empty($form_data['id'])) {
                unset($form_data['email_id'], $form_data['mobile']);
                $form_data['update_date'] = date('Y-m-d H:i:s');
                $action_col = array_keys($form_data);
                $where = "id = :id AND pass_status = :pass_status AND verified = :verified";
                
                $res = $obj_curd->pdoUpdatetData("visitor_info", $form_data, $action_col, $where);
                if($res) {
                    $status = 1;
                    $msg = "Visitor Pass Updated Successfully.";
                }
            }else{
                $where_col_name = ['mobile', 'email_id', 'host_orgn', 'host_name', 'status', 'type', 'pass_status'];
                $cust_where = " AND '".date('Y-m-d', strtotime($form_data['from_date']))."' BETWEEN from_date AND to_date";
                $res = $obj_curd->pdoGetData('visitor_info', $form_data, $where_col_name, $cust_where);
                //print_r($res); die;

                if(COUNT($res) > 0) {
                    $msg = "Pass is already generated between given dates, delete current pass to generate new pass";
                    $exist = true;
                }else {
                    $filename = md5(time() . basename($_FILES['aadhar_file']['name']));
                    $targetPath = 'uploads/visitor_document/' .$filename;
                    if (move_uploaded_file($_FILES['aadhar_file']['tmp_name'], $targetPath)) {
                        $form_data['aadhar_file'] = $filename;
                        $res = $obj_curd->pdoInsertData('visitor_info', $form_data); 
                        if($res > 0) {
                            $shortURL = $obj_curd->createShortenerUrl($res);
                            
                            // $shortURL = str_replace('http://', '', $shortURL);
                            /*
                            * code to send Verification SMS 
                            */
                            $sms = new Pinnacle();
                            $resp = $sms->sendVerificationSms($otpCode, $shortURL, $form_data['mobile']);
                            if($resp['status'] != 'success') {
                                $noti_msg = "<br> Message has not delivered";
                            }
                            /*
                            * SMS code ends here
                            */
        
                            /*
                            * code to send Verification Email 
                            */ 
                            $to = $form_data['email_id'];
                            $subject ="Visitor Verification -Aurum Orize";
                            $message ="
                                <html>
                                <head>
                                <title>Visitor Id Details</title>
                                </head>
                                <body>
                                <p>Dear Sir/Madam,<br> Your OTP for verification to VMS Portal QPARC is ".$otpCode.". Valid for 30 minutes. Please do not share this OTP. Please <a href=".$shortURL.">click here</a> to verify your OTP.</p> 
                                </body>
                                </html>
                                ";
                            $a = $obj_curd->send_secure_mail($to, $message, $subject);
                        
                            /*
                            * Email code ends here 
                            */ 
                            $status = 1;
                            $msg = "Pass has been created Successfully. <br>Email and Message sent to visitor for verification";
        
                        }else{
                            $msg = $res;
                        }
                    } else {
                        $msg="Something wrong data not inserted";
                    }
                }
            }
            
            echo json_encode(array('status'=>$status, 'msg' => $msg, 'data' => $data, 'exist' => $exist)); die(); 
            
        }catch (Exception $e) {
            echo json_encode(array('status'=>0, 'msg' => $e->getMessage(), 'data' => '')); die(); 
        }
        
    }


    /**
     * Checking user-type and creating dropdown for host and host member accordingly.
     */
    $hostHtml = $hostMemHtml = "";
    if($mem_type == ADMIN || $mem_type == TENANT_ADMIN) {
        $org_data = $obj_curd->getHost($mem_type, $mem_id);

        if(!is_array($org_data[0])) {
            $org_data = [$org_data];
        }
        $hostHtml = '<div class="form-group col-sm-6 col-xs-12">
                        <label for="usr">Host:</label>
                            <select id="host_orgn" name="host_orgn" class="form-control input_style ">
                                <option value="">Select Host</option>
                                '.$obj_curd->createSelectOption($org_data, 'id','tenant_name').'
                            </select>
                        </div>';  

        $hostMemHtml = '<div class="form-group col-sm-6 col-xs-12">
                            <label for="usr">Host Member:</label>
                            <select id="host_name" name="host_name" class="form-control input_style ">
                                <option value="">Select Host Member</option>
                            </select>
                        </div>';                
    }
    
?>

<!DOCTYPE html>
<html>
<?php include('head.php');?>

<style>
    label.error {
        color: #e95c5c!important;
    }
    .form-group label:after {
        content:"*";
        color:red;
    }
</style>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="loader" >
        <img src="../images/loader.gif" style="margin-top: 300px;" width="100" height="100">
    </div>
    <div class="wrapper">
        <!-- Navbar -->
        <?php include('nav.php');?>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <?php include('side_menu.php');?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content"  style="padding: 0px">
                <div class="container-fluid">
                    <div class="row">
                        <h4 class="page-title">Customize Visitors Pass</h4>
                        <!-- /.card-header -->
                        <div class="col-sm-12">
                        <button type="button" class="btn btn-primary mb-4" data-toggle="modal" data-target="#exampleModalCenter">
                            Create Customize Pass
                        </button>
                        </div>
        
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <!-- SELECT `id`, `visitor_name`, `mobile`, `email_id`, `coming_from`, `visitor_org`, `photo`, `persons_alogn_with_visitor`, `host_orgn`, `host_name`, `meeting_purpose`, `vehicle_no`, `signature`, `in_time`, `out_time`, `verified`, `status`, `add_date`, `update_date`, `visitor_id` FROM `visitor_info` WHERE 1 -->
                                <tr> 
                                    <th>Sr. No.</th>
                                    <th>Visitor Id</th>  
                                    <th>Name</th>  
                                    <th>Email</th>  
                                    <th>Phone</th>  
                                    <th>Host</th>
                                    <th>Host Name</th>
                                    <!-- <th>Date</th>  
                                    <th>In Time</th>  
                                    <th>Out Time</th> -->
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Verified</th>  
                                    <th>Action</th>  
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php include('Footer.php');?>
    </div>
    <!-- ./wrapper -->
    

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Create Customize Pass</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button> 
            </div>
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <span id="result"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="row align-items-center justify-content-center" id="custom_pass_form" style="padding: 15px;">
                    <div class="col-md-12">   
                        <div class="row">
                            <div class="form-group col-sm-4 col-xs-11">
                                <label for="usr">Name:</label>
                                <input type="text" class="form-control input_style " id="visitor_name" name="visitor_name" placeholder="Name">
                            </div>

                            <div class="form-group col-sm-4 col-xs-11">
                                <label for="usr">Mobile No:</label>
                                <input type="text" class="form-control input_style " id="mobile" name="mobile" placeholder="Mobile">
                            </div>

                            <div class="form-group col-sm-4 col-xs-11">
                                <label for="usr">Email Id:</label>
                                <input type="email" class="form-control input_style " id="email_id" name="email_id" placeholder="Email">
                            </div>
                        </div>     
                    </div>    
                    
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group col-sm-4 col-xs-11">
                                <label for="usr">Coming from:</label>
                                <input type="text" class="form-control input_style " id="coming_from" name="coming_from" placeholder="Coming From">
                            </div>
                                    
                            <div class="form-group col-sm-8 col-xs-11">
                                <label for="usr">Visitor Organisation:</label>
                                <input type="text" class="form-control input_style " id="visitor_org" name="visitor_org" placeholder="Organisation">
                            </div>
                        </div>
                    </div>
                    

                    <div class="form-group col-sm-12 col-xs-12">
                        <label for="usr">Purpose of visit:</label>
                        <textarea class="form-control input_style " id="meeting_purpose" name="meeting_purpose" rows="1" placeholder="Visit purpose"></textarea>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="row">
                            <!--- START HOST Orgnazation HTML -->
                            <?php 
                                echo $hostHtml;
                            ?>
                            <!--- END HOST Orgnazation HTML -->

                            <!--- START HOST Member HTML -->
                            <?php 
                                echo $hostMemHtml;
                            ?>
                            <!--- END HOST Member HTML -->
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group col-sm-6 col-xs-11">
                                <label for="usr">Aadhaar No.:</label>
                                <input type="text" class="form-control input_style" name="aadhar_no" id="aadhar_no" placeholder="Enter Aadhaar No.">
                            </div>

                            <div class="form-group col-sm-6 col-xs-11">
                                <label for="usr">Upload Aadhaar card:</label>
                                <input type="file" class="form-control input_style" name="aadhar_file" id="aadhar_file" placeholder="Upload Aadhaar card">
                            </div>
                        </div>
                    </div>
                            

                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group col-sm-6 col-xs-11">
                                <label for="usr">From Date:</label>
                                <input type="text" class="form-control input_style  datepicker" name="from_date" id="from_date" placeholder="From Date">
                            </div>

                            <div class="form-group col-sm-6 col-xs-11">
                                <label for="usr">To Date:</label>
                                <input type="text" class="form-control input_style  datepicker" name="to_date" id="to_date" placeholder="To Date" disabled>
                            </div>
                        </div>
                    </div> 
                    <input type="hidden" name="id" id="id" value="">       
                </form>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary custom_pass_btn">Create</button>
            </div>
        </div>
    </div>
</div>
<?php include('script.php');?>
<script>
    $(function() {
        getTabledata();
        $('#exampleModalCenter').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
            $("#id").val("");
            $("label.error").remove();
            $("#result").html('').parent('.alert').hide();
            $("#email_id, #mobile").removeAttr('readonly');
        })

        $(".datepicker").datepicker({ 
            minDate: 0,
            dateFormat: 'dd-mm-yy',
            onSelect: function(selected) {
                var start_date = $("#from_date").val();
                start_date_part = start_date.split("-");

                start_date = new Date(+start_date_part[2], start_date_part[1] - 1, +start_date_part[0]);
                newstart_date = new Date(+start_date_part[2], start_date_part[1] - 1, +start_date_part[0]);

                start_date.setMonth(start_date.getMonth()+3);
                $("#to_date").prop('disabled', false);
                $("#to_date").datepicker("option","minDate", newstart_date);
                $("#to_date").datepicker("option","maxDate", start_date);
            }
        });
        $("#from_date").change(function() {
            if($("#from_date").val() == "") {
                $("#to_date").prop('disabled', true);
                $("#to_date").val('');
            }
        })

        $.validator.addMethod('filesize', function (value, element, arg) {
            //check file size in mb
            return this.optional(element) || (element.files[0].size <= arg)
            // if(element.files[0].size<=arg){
            //     return true;
            // }else{
            //     return false;
            // }
        }, 'File size must be less than 3 mb');

        var form = $("#custom_pass_form");
        form.validate({
            rules: {
                visitor_name: {
                    required: true
                },
                email_id: {
                    required: true,
                    email:true
                },
                mobile: {
                    required: true,
                    number: true,
                    minlength:10,
                    maxlength:10,
                    pattern: /^[6-9]\d{9}$/
                },
                coming_from: {
                    required: true
                },
                visitor_org: {
                    required: true
                },
                meeting_purpose: {
                    required: true
                },
                host_orgn: {
                    required: true
                },
                host_name: {
                    required: true
                },
                aadhar_no: {
                    required: true,
                    number: true,
                    minlength:12,
                    maxlength:12
                },
                aadhar_file: {
                    required: function(element) {
                        if($("#id").val() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    },
                    extension: "pdf|jpg|jpeg",
                    filesize : 3145728 //3mb in bytes 
                },
                to_date: {
                    required: true
                },
                from_date: {
                    required: true
                },
                to_date: {
                    required: true
                },
            },
            messages: {
                'aadhar_file' : {
                    extension : 'File must be pdf,jpg or jpeg',
                    filesize: 'File size must be less than 3 mb.'
                },
                'aadhar_no': {
                    minlength : 'Number must be 12 digit long',
                    maxlength : 'Number must be 12 digit long'
                },
                'mobile': {
                    minlength : 'Number must be 10 digit long',
                    maxlength : 'Number must be 10 digit',
                    pattern: 'Enter valid mobile number.'
                }
            }
        });

        $(".custom_pass_btn").click(async function(e) {
            e.preventDefault();
            if(form.valid()) {
                var data = new FormData(document.querySelector('form'));
                data.append('submit_customize_pass', '1');
                try {
                    $(".loader").show();
                    var res = await ajaxRequest('customize_pass.php', data, 'POST', 'json', true);
                    $(".loader").hide();
                    if(res.status) {
                        $("#exampleModalCenter").modal('toggle');
                        Swal.fire(
                            'Success',
                            res.msg,
                            'success'
                        )
                        getTabledata();    
                    }else{
                        if(res.data != "") {
                            Object.entries(res.data).forEach(
                                ([key, value]) => $("#"+key).addClass('error').after('<label for="'+key+'" class="error">'+value+'</label>')
                            );
                        }else{
                            if(res.exist) {
                                msg = res.msg
                            }else{
                                msg = "Something went wrong";
                            }
                            $("#result").html(msg).parent('.alert').show();
                        }
                    }
                }catch(error) {
                    $(".loader").hide();
                    $("#result").html('Something went wrong').parent('.alert').show();
                }
            }
        })

        async function getTabledata() {
            var data = {getTabledata: 1};
                try {
                    $(".loader").show();
                    var res = await ajaxRequest('customize_pass.php', data, 'POST', 'json');
                    $(".loader").hide();
                    if(res.status) {
                        $("#example1").DataTable().clear().destroy();
                        $("#example1").find('tbody').html(res.data);
                        reinitializetable();
                    }
                }catch(error) {
                    $(".loader").hide();
                    Swal.fire('Error', "Something Went wrong", 'error');
                }
        }

        $("#example1").on("click", ".delete_record", function() {
            var id = $(this).data('id');

            if(id != "") {
                var data = {delete_data: 1, id: id};
                Swal.fire({
                    title: 'Do you want to delete this record?',
                    showDenyButton: true,
                    confirmButtonText: `Yes`,
                    denyButtonText: `No`,
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        try {
                            $(".loader").show();
                            ajaxRequest('customize_pass.php', data, 'POST', 'json').then((res)=> {
                                $(".loader").hide();
                                if(res.status) {
                                    Swal.fire('Success', res.msg, 'success');
                                    getTabledata();    
                                }else{
                                    Swal.fire('Error', "Something Went wrong", 'error');
                                }
                            });
                            
                        }catch(error) {
                            $(".loader").hide();
                            Swal.fire('Error', "Something Went wrong", 'error');
                        }
                    }
                })
                
            }else{
                Swal.fire('Error', "Something Went wrong", 'error');
            }
        })

        $("#example1").on("click", ".edit_record", function() {
            try{
                var id = $(this).data('id');

                if(id != "") {
                    var data = {edit_data: 1, id: id};
                    $(".loader").show();
                    ajaxRequest('customize_pass.php', data, 'POST', 'json').then( async (res)=> {
                        console.log(res.data);
                        console.log(res.data.visitor_name);
                        $(".loader").hide();
                        if(res.status) {
                            
                               $("#exampleModalCenter").modal('toggle');
                                $("#visitor_name").val(res.data.visitor_name);
                                $("#email_id").val(res.data.email_id).attr('readonly', true);
                                $("#mobile").val(res.data.mobile).attr('readonly', true);
                                $("#coming_from").val(res.data.coming_from);
                                $("#visitor_org").val(res.data.visitor_org);
                                $("#meeting_purpose").val(res.data.meeting_purpose);
                                $("#host_orgn").val(res.data.host_orgn);
                                $("#aadhar_no").val(res.data.aadhar_no);
                                $("#to_date").val(res.data.to_date);
                                $("#from_date").val(res.data.from_date);
                                $("#to_date").val(res.data.to_date);
                                $("#id").val(res.data.id);
                                $("#aadhar_file").attr('required', false);
                                var data = {id: res.data.host_orgn, get_host_mem:1};
                                var host_name = await ajaxRequest('customize_pass.php', data, 'POST', 'json');
                                if(host_name.status) {
                                    $("#host_mem").html("");
                                    $("#host_name").html('<option value="">Select Host Member</option>');
                                    $("#host_name").append(host_name.data);
                                    $("#host_name").val(res.data.host_name);
                                }
                                
                        }else{
                            Swal.fire('Error', "Something Went wrong", 'error');
                        }
                    });
                }else{
                    Swal.fire('Error', "Something Went wrong", 'error');
                }
                
            }catch(error) {
                $(".loader").hide();
                Swal.fire('Error', "Something Went wrong", 'error');
            }
            
        })

    });
</script>




