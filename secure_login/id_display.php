<?php
  include ('session_check.php');
   
   include ('class/Curd.php');
   $otpn=trim($_GET['id']);
   
  //  //C:\xampp\htdocs\Harry\visitor\visitor\vendor\phpqrcode\temp\test72c52e1bcf992a23b9a6b2b255efeb06.png
  //  $PNG_QR_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR;
  //  $PNG_WEB_DIR = 'uploads/';
  //   if (!file_exists($PNG_QR_DIR))
  //       mkdir($PNG_QR_DIR);
  //   $filename = $PNG_QR_DIR.'visitor_qr.png';  
  //   $errorCorrectionLevel = 'L';
  //   $matrixPointSize = 4;
  //   $id = $obj_curd->encrypt_data($otpn);
  //   $text = 'https://qparc.in/visitor/qr_verification.php?'.$id;
  //   QRcode::png($text, $filename, $errorCorrectionLevel, $matrixPointSize, 2); 
  //   //echo '<img src="'.$PNG_WEB_DIR.basename($filename).'" /><hr/>'; die();
  ?>
<!DOCTYPE html>
<html>
  <?php include('head.php');?>
  <style media="print" type="text/css">
    @media print {
    body {-webkit-print-color-adjust: exact;}
    .visitor_bg {
    background-color: #000 !important; border:1px solid #000; text-align: center;
    }
    }
  </style>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <!-- Navbar -->
      <?php include('nav.php');?>
      <!-- /.navbar -->
      <!-- Main Sidebar Container -->
      <?php include('side_menu.php');?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <!-- left column -->
              <div class="col-md-12 col-sm-12 d-flex justify-content-center">
                <!-- general form elements -->
                <div class="card col-md-6" id="printableArea">
                  <?php
                    $about_data = array('id' =>$otpn);
                     $variable=$obj_curd->display_all_record("visitor_info",$about_data);
                                    foreach ($variable as $row) {
                    /*SELECT `id`, `visitor_name`, `mobile`, `email_id`, `coming_from`, `visitor_org`, `photo`, `persons_alogn_with_visitor`, `host_orgn`, `host_name`, `meeting_purpose`, `vehicle_no`, `signature`, `in_time`, `out_time`, `verified`, `status`, `add_date`, `update_date`, `visitor_id` FROM `visitor_info` WHERE 1*/
                    
                    $qr_code = $obj_curd->generateQrCode($row['visitor_id'], 'secure_login/visitor_check_in_out.php');
                    
                    ?>
                    
                  <table>
                    <tr>
                      <td class="visitor_bg" style="background: #000; color: #fff; font-weight: 700; font-size: 20px; line-height: 23px;padding: 10px 7px; " bgcolor="#000">V<br>I<br>S<br>I<br>T<br>O<br>R</td>
                      <td style="padding: 15px">
                        <table>
                          <!--<tr >
                            <td colspan="2" class="text-left"><h3 style="margin-bottom: 5px; font-weight: 700; font-size:24px">ID: <?php echo $row['visitor_id']; ?></h3></td>
                            
                            </tr>-->
                          <tr>
                            
                            <td class="text-left">
                              <ul style="font-size: 15px; margin-left: 15px;" class="list-unstyled">
                                <li><strong>Id:</strong><?php echo $row['visitor_id']; ?></strong></li>
                                <li><strong>Name: </strong><?php echo $row['visitor_name']; ?></li>
                                <li><strong>Mobile: </strong><?php echo $row['mobile']; ?></li>
                                <li><strong>From: </strong><?php echo $row['visitor_org'].', '.$row['coming_from']; ?></li>
                                <li><strong>Meets: </strong><?php 
                                  $where11 = array('id' =>$row['host_name']);
                                  
                                     $variable11=$obj_curd->display_all_record("tenants_users",$where11);
                                     foreach ($variable11 as $row11) {
                                  echo $row11['user_name'];
                                     } ?></li>
                                <li><strong>Host: </strong>
                                  <?php 
                                    $about_data2 = array('id' =>$row['host_orgn']);
                                        $variable2=$obj_curd->display_all_record("tenants",$about_data2);
                                                       foreach ($variable2 as $row2) {
                                         echo $row2['tenant_name']; 
                                    }
                                         ?>
                                </li>
                                <li><strong>Purpose: </strong><?php echo $row['meeting_purpose']; ?></li>
                                <li><strong>Intime: </strong><?php  echo (!empty($row['in_time']) && $row['verified'] == 1 ? $row['in_time'] : "");         
                                  //echo date_format($row['in_time'],"d/m/Y H:i:s"); 
                                  ?></strong></li>
                                  <li><strong>Outtime: </strong><?php  echo (!empty($row['out_time']) && $row['verified'] == 1 ? $row['out_time'] : "");         
                                  //echo date_format($row['in_time'],"d/m/Y H:i:s"); 
                                  ?></strong></li>
                                <li>
                                  <div class="mt-3">
                                      <img src="<?php echo $qr_code; ?> " style="width:100px;" /> 
                                  </div>
                                </li>  
                              </ul>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="d-flex" style="flex-direction: column; align-items: center;">
                          <div class="mt-3">
                            <img src="<?php echo (!empty($row['photo']) ? $row['photo'] : 'uploads/profile.png')?>"  style="width: 150px; height: 160px"> 
                          </div>
                          
                          <div>
                            <?php 
                              $verify = ($row['verified'] == 1) ? "../images/verified.jpg" : "";
                            ?>
                            <img src="<?php echo $verify; ?>" style="width:100px;">
                          </div>
                          
                      </td>
                    </tr>
                  </table>
                  <?php } ?>
                </div>
                <!-- /.card -->
              </div>
              <div class="col-md-12 col-sm-12 text-center">
                <input type="button" onclick="printDiv('printableArea')" value="Print card!" />
              </div>
              
              <!--/.col (left) -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <?php 
        include('Footer.php');
        include('modal.php');
        
        ?>
    </div>
    <!-- ./wrapper -->
    <?php include('script.php');?>
    <!-- Get user data-->
    <script type="text/javascript">
      function printDiv(divName) {
         var printContents = document.getElementById(divName).innerHTML;
         var originalContents = document.body.innerHTML;
      
         document.body.innerHTML = printContents;
      
         window.print();
      
         document.body.innerHTML = originalContents;
      }
    </script>
    <!-- Get user data-->
  </body>
</html>