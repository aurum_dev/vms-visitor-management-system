<?php
    include ('session_check.php');
    $user_id = $_SESSION['user_id'];
    $mem_id = $_SESSION['mem_id'];
    $mem_type = $_SESSION['mem_type'];
    
    include ('class/Curd.php');
    require '../vendor/sms/Pinnacle.php';
    
    if($mem_type != ADMIN && $mem_type != SECURITY) {
        header("Location:dashboard.php"); die;
    }
    $table_display = 'style="display:none;"';
    $q = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
    if(!empty($q)) {
        $id = $obj_curd->decrypt_data(trim($q));
        $res = $obj_curd->generatePass($id);
        if($res['status'] == 1) {
            $scan_id = $id; 
            $html = $res['data'];
            $table_display = 'style="display:flex;"';
        }else{
            die('<div style="text-align:center"><h1>'.$res['data'].'</h1></div>');
        }
        
    }

    if(isset($_POST['checkScan']) && $_POST['checkScan'] == 1) {
        $html="";
        $msg = "";
        $status = 0;
        $q = parse_url($_POST['id'], PHP_URL_QUERY);
        if(!empty($q)) {
            $id = $obj_curd->decrypt_data(trim($q));
            $res = $obj_curd->generatePass($id);
            if($res['status'] == 1) {
                $status = 1;
                $html = $res['data'];
            }else{
                $msg = $res['data'];
            }
            echo json_encode(array('status'=>$status, 'data' => $html, 'msg' => $msg)); die;
        }else{
            echo json_encode(array('status'=>0, 'data' => "", 'msg' => "Invalid Pass")); die;
        }
        
    }
    
    if(isset($_POST['check_pass']) && $_POST['check_pass'] == 1) {
        $html="";
        $msg = "";
        $status = 0;
        $res = $obj_curd->generatePass($_POST['id']);
        if($res['status'] == 1) {
            $status = 1;
            $html = $res['data'];
        }else{
            $msg = $res['data'];
        }
        echo json_encode(array('status'=>$status, 'data' => $html, 'msg' => $msg)); die;
    }
    

    if(isset($_POST['checkInOut']) && $_POST['checkInOut'] == 1) {
        $html="";
        $msg = "";
        $status = 0;

        if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['type']) && !empty($_POST['type']) ) {
            $qry="SELECT * FROM visitor_info WHERE id = ".$_POST['id'];
            $data = $obj_curd->executeRawQuery($qry);
            if(count($data) > 0) {
                $data = $data[0];
                if($_POST['type'] == 'out') {
                    $uptdata['out_time'] = date('Y-m-d H:i:s');
                    $uptdata['status'] = 4;
                    $uptdata['id'] = $_POST['id'];
                    $uptdata['check_out_by'] = $mem_id;
                    $where = "id = :id";
                    $action_col = ['out_time','check_out_by','status'];
                    if($obj_curd->pdoUpdatetData('visitor_info', $uptdata, $action_col, $where)) {
                        $status = 1;
                        $data = $data['visitor_id'];
                    }
                }else if($_POST['type'] == 'in') {
                    $uptdata['id'] = $_POST['id'];
                    if(!empty($data['in_time'])) {
                        $uptdata = $data;
                        $uptdata['in_time'] = date('Y-m-d H:i:s');
                        $uptdata['created_at'] = date('Y-m-d H:i:s');
                        $uptdata['check_in_by'] = $mem_id;
                        $uptdata['status'] = 1;
                        unset($uptdata['id']);
                        unset($uptdata['out_time']);
                        unset($uptdata['updated_at']);
                        if($obj_curd->pdoInsertData('visitor_info', $uptdata)) {
                            $status = 1;
                            $data = $data['visitor_id'];
                        }
                    }else{
                        $uptdata['in_time'] = date('Y-m-d H:i:s');
                        $uptdata['id'] = $_POST['id'];
                        $uptdata['check_in_by'] = $mem_id;
                        $uptdata['status'] = 1;
                        $where = "id = :id";
                        $action_col = ['in_time','check_in_by','status'];
                        if($obj_curd->pdoUpdatetData('visitor_info', $uptdata, $action_col, $where)) {
                            $status = 1;
                            $data = $data['visitor_id'];
                        }
                    }
                }
            }else{
                $msg = "Data Not found";
            }
        }

        echo json_encode(array('status'=>$status, 'data' => $data, 'msg' => $msg)); die;
    }
?>

<!DOCTYPE html>
<html>
<?php include('head.php');?>

<style>
    label.error {
        color: #e95c5c!important;
    }
    
</style>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="loader" >
        <img src="../images/loader.gif" style="margin-top: 300px;" width="100" height="100">
    </div>
    <div class="wrapper">
        <!-- Navbar -->
        <?php include('nav.php');?>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <?php include('side_menu.php');?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content main_page"  style="padding: 0px">
                <div class="container-fluid">
                    
                    <h4 class="page-title">Visitor Check In/Out</h4>
                    <!-- /.card-body -->
                    <div class="row align-items-center justify-content-center mt-4">
                        <!-- left column -->
                        <div class="col-md-4">
                            <!-- general form elements -->
                            <div class="card card-primary text-center">
                                <form id="visitor_id_form">
                                    <div class="col-md-12">
                                        <div class="row mt-3">
                                            <div class="form-group col-sm-6 col-xs-11">
                                                <input type="text" class="form-control numberonly" name="visitor_id" id="visitor_id" placeholder="Enter Visitor Id" value="<?php echo $scan_id; ?>">
                                            </div>
                                            <div class="form-group col-sm-6 col-xs-11">
                                                <button type="button" class="btn btn-primary check_btn">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="scan_div" style="display: none;">
                                    <div class="separator text-center mb-3">
                                        <h5 style="font-size: 12px;">OR</h5>
                                    </div>
                                    <div class="mb-2"> 
                                        <button type="button" class="btn btn-primary scanCode">Scan QRCode</button>
                                    </div>
                                </div>    
                            </div>
                        </div> 
                    </div>

                    <div class="row align-items-center justify-content-center visitor_pass_block" <?php echo $table_display;?>>
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="card card-primary" id="visitor_pass">
                                <?php echo $html;?>
                            </div>
                        </div> 
                    </div>
                </div>
            </section>
            <div class="row align-items-center justify-content-center mt-4 scanner_block" style="display: none;">
                <video id="preview" width="100%"></video>
                <button class="btn btn-danger back_scan col-md-12">Back</button>
            </div>
        <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php include('Footer.php');?>
    </div>
    <!-- ./wrapper -->

<?php include('script.php');?>
<!-- <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js"></script>

<script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
<script>
    $(function() {
        var form = $("#visitor_id_form");
        form.validate({
            rules: {
                visitor_id: {
                    required: true,
                    number: true
                }
            }
        });

        $(".check_btn").click(async function(e) {
            e.preventDefault();
            $(".error").html("");
            $("#visitor_pass").html('');
            $(".visitor_pass_block").hide();
            if(form.valid()) {
                var data = {id: $("#visitor_id").val(), check_pass: 1};
                try {
                    $(".loader").show();
                    var res = await ajaxRequest('visitor_check_in_out.php', data, 'POST', 'json');
                    $(".loader").hide();
                    if(res.status) {
                        $("#visitor_pass").html(res.data);
                        $(".visitor_pass_block").show();
                    }else{
                        $("#visitor_id").addClass('error').after('<label for="visitor_id" class="error">'+res.msg+'</label>');  
                        $("#visitor_pass").html('');
                        $(".visitor_pass_block").hide(); 
                    }
                }catch(error) {
                    $(".loader").hide();
                    Swal.fire('Error', "Something Went wrong", 'error');
                }
            }
        })

        $("#visitor_pass").on('click','.check_in_out', async function(e) {
            var id = $('.check_in_out').data('id');
            var type = $('.check_in_out').data('value');

            if(id != "" && type != "") {
                var data = {id: id, type: type, checkInOut: 1};
                try {
                    $(".loader").show();
                    var res = await ajaxRequest('visitor_check_in_out.php', data, 'POST', 'json');
                    $(".loader").hide();
                    if(res.status) {
                        if(res.data !="") {
                            $("#visitor_id").val(res.data);
                            $(".check_btn").trigger('click');
                        }
                    }else{
                        Swal.fire('Error', "Something Went wrong", 'error');
                    }
                }catch(error) {
                    $(".loader").hide();
                    Swal.fire('Error', "Something Went wrong", 'error');
                }
            }
        })

        // async function onScanSuccess(qrCodeMessage) {
        //     console.log(qrCodeMessage);
        //     if(qrCodeMessage != "") {
        //         var data = {id: qrCodeMessage, checkScan: 1};
        //         try {
        //             $(".loader").show();
        //             var res = await ajaxRequest('visitor_check_in_out.php', data, 'POST', 'json');
        //             $(".loader").hide();
        //             if(res.status) {
        //                 if(res.data !="") {
        //                     $("#visitor_pass").html(res.data);
        //                     $(".visitor_pass_block").show();
        //                 }
        //             }else{
        //                 Swal.fire('Error', res.msg, 'error');
        //             }
        //         }catch(error) {
        //             $(".loader").hide();
        //             Swal.fire('Error', "Something Went wrong", 'error');
        //         }
        //     }
        // }

        // function onScanError(errorMessage) {
        //     Swal.fire('Error', "Something Went wrong", 'error');
        // }

        // var html5QrcodeScanner = new Html5QrcodeScanner("scancode", { fps: 10, qrbox: 250 });
        // html5QrcodeScanner.render({ facingMode: { exact: "environment"} }, onScanSuccess, onScanError);
        // $("#scancode span:first-child").remove();
        //$(".loader").show();
        $(".loader").show();
        var $device_camera = null;
        let scanner = new Instascan.Scanner({ video: document.getElementById('preview'), mirror: false});
        Instascan.Camera.getCameras().then(function(cameras){
             $(".loader").hide();
            device_camera = cameras;
            $(".scan_div").show();
        }).catch(function(e) {
            $(".loader").hide(); 
            
        }); 
           
        $(".scanCode").on("click", function () {
            if(device_camera.length > 0 ){
                $(".loader").show();
                scanner.start(device_camera[device_camera.length-1]).then(function(cameras){
                    $(".loader").hide();
                    $('.main_page').hide();
                    $(".scanner_block").show();    
                }).catch(function(e) {
                    $(".loader").hide(); 
                    Swal.fire('Error', 'unable to open camera', 'error');
                });
                
            } else{
                $(".loader").hide();
                Swal.fire('Error', 'No cameras found', 'error');
            }
        })

        scanner.addListener('scan', async function(qrCodeMessage){
            if(qrCodeMessage != "") {
                var data = {id: qrCodeMessage, checkScan: 1};
                try {
                    $(".loader").show();
                    var res = await ajaxRequest('visitor_check_in_out.php', data, 'POST', 'json');
                    $(".loader").hide();
                    if(res.status) {
                        if(res.data !="") {
                            $("#visitor_pass").html(res.data);
                            $(".visitor_pass_block").show();
                            $('.main_page').show();
                            $(".scanner_block").hide();
                            scanner.stop();
                        }
                    }else{
                        Swal.fire('Error', res.msg, 'error');
                    }
                }catch(error) {
                    $(".loader").hide();
                    Swal.fire('Error', "Something Went wrong", 'error');
                }
            }
        });

        $(".back_scan").on("click", function() {
            scanner.stop();
            $(".scanner_block").hide();
            $('.main_page').show();  
        })
    });
</script>




