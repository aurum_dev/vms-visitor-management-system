<?php
include ('session_check.php');
$oprn=$_GET['opr'];
include('class/Curd.php');
if($oprn=='add')
{
$rest_name="";
$rest_address="";
$location="";
$owner_name="";
$mobile_number="";
$email_id="";
$pan_no="";
$gst_regn="";
$opening_time="";
$closing_time="";
$rest_type="";
$is_fevt="";
$is_top="";
$img_add="";
$img_add2="";
 $set_fvt='';
 $set_top='';
$st='checked';
}
else
{
/*SELECT `id`, `rest_name`, `rest_address`, `location`, `owner_name`, `mobile_number`, `email_id`, 
`restn_image`, `pan_no`, `fssai_regn`, `gst_regn`, `opening_time`, `closing_time`, `rest_type`,
 `add_date`, `update_date`, `status`, `is_fevt`, `is_top` FROM `vendor` WHERE 1*/
$id=$_GET['id'];
$where=array("id"=>$id);
$variable=$obj_curd->display_all_record("vendor",$where);
foreach ($variable as $row) {
$rest_name=$row['rest_name'];
$rest_address=$row['rest_address'];
$location=$row['location'];
$owner_name=$row['owner_name'];
$mobile_number=$row['mobile_number'];
$email_id=$row['email_id']; 
$pan_no=$row['pan_no']; 
$gst_regn=$row['gst_regn'];
$opening_time=$row['opening_time'];
$closing_time=$row['closing_time'];
$rest_type=$row['rest_type'];
$is_fevt=$row['is_fevt'];
$is_top=$row['is_top']; 
$img_add="<img src='".substr($row['restn_image'], 0, -2)."' height='50px' style='margin-top:10px;'>";
$img_add2="<img src='".substr($row['fssai_regn'], 0, -2)."' height='50px' style='margin-top:10px;'>";
$rest_img=$row['restn_image'];
$fssai_img=$row['fssai_regn'];

$status=$row['status']; 
if($status==1)
{
$st="checked";
}
else
{
$st='';
}

if($is_fevt==1)
{
$set_fvt="checked";
}
else
{
$set_fvt='';
}

if($is_top==1)
{
$set_top="checked";
}
else
{
$set_top='';
}
}
}
?>
<!DOCTYPE html>
<html>
  <?php include('head.php');?>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <!-- Navbar -->
      <?php include('nav.php');?>
      <!-- /.navbar -->
      <!-- Main Sidebar Container -->
      <?php include('side_menu.php');?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <!-- left column -->
    
              <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title"><?php echo $oprn?> sub category</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form  id="slider_form" method="post" action="" enctype="multipart/form-data">
                    <div class="card-body">
                      <div class="col-sm-12">
                        <center><div id="result"></div></center>
                      </div>
                      
                      
                      <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="card-title m-0">Restraurent_info</h5>
              </div>
              <div class="card-body">
                <div class="row">
                 <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        <label for="rest_name">Restn/hotel name</label>
                        
                        <input type="text" id="rest_name" name="rest_name" value="<?php echo $rest_name;?>" class="form-control">
                      </div>
                      
                      
                      <div class="form-group col-lg-9 col-md-9 col-sm-9">
                        <label for="rest_address">Restraurent address</label>
                        
                        <input type="text" id="rest_address" name="rest_address" value="<?php echo $rest_address;?>" class="form-control">
                      </div>
                      
                      
                      <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="location">location</label>
                         
                         <select id="location" name="location" class="form-control">
                          <option value="">Select location</option>
<?php

$where=array("status"=>'1');
$variable=$obj_curd->display_all_record("area",$where);
foreach ($variable as $row) {
	if($row['id']==$location)
	{
		$select_location='selected';
	}
	else
	{
		$select_location='';
	}
?>
<option value="<?php echo $row['id'];?>" <?php echo $select_location;?>><?php echo $row['area_name'];?></option>
<?php
}

?>
                        </select>
                      </div>
                      
                      <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="cat_name">hotel/restn photo</label>
                        
                       <input type="file" class="form-control"  id="multiFiles"   multiple  accept="image/x-png,image/jpeg"/>
                        <?php echo $img_add;?>
                      </div>

                       <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="opening_time">Shop open at</label>
                        
                          <select id="opening_time" name="opening_time" class="form-control">
                          <option value="">Select opening time</option>
<?php
$where=array("status"=>'1');
$variable=$obj_curd->display_all_record("opening_hours",$where);
foreach ($variable as $row) {
 
	if($row['id']=$opening_time)
	{
		$select_time='selected';
	}
	else
	{
		$select_time='';
	}
?>
<option value="<?php echo $row['id'];?>" <?php echo $select_time;?>><?php echo $row['hours'];?></option>
<?php
}

?>
                        </select>
                      </div>
                      
                      <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="closing_time">shop close at</label>
                         
                         <select id="closing_time" name="closing_time" class="form-control">
                          <option value="">Select opening time</option>
<?php
$where3=array("status"=>'1');
$variable3=$obj_curd->display_all_record("opening_hours",$where3);
foreach ($variable3 as $row3) {
	if($row['id']=$closing_time)
	{
		$select_time2='selected';
	}
	else
	{
		$select_time2='';
	}
?>
<option value="<?php echo $row3['id'];?>" <?php echo $select_time2;?>><?php echo $row3['hours'];?></option>
<?php
}

?>
                        </select>
                      </div>
                      
                      <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="rest_type">Restraurent type</label>
                         
                        
   <select id="rest_type" name="rest_type" class="form-control">
                          <option value="">Select opening time</option>
<?php
$where=array("status"=>'1');
$variable=$obj_curd->display_all_record("main_category",$where);
foreach ($variable as $row) {
	if($row['id']=$rest_type)
	{
		$select_cat='selected';
	}
	else
	{
		$select_cat='';
	}
?>
<option value="<?php echo $row['id'];?>" <?php echo $select_cat;?>><?php echo $row['name'];?></option>
<?php
}

?>
                        </select>
                      </div>
                    </div>
              </div>
            </div>
                      
                      



 <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="card-title m-0">Restraurent_info</h5>
              </div>
              <div class="card-body">

 <div class="row">
                      <div class="form-group col-lg-4 col-md-4 col-sm-4">
                        <label for="owner_name">contact person</label>
                        
                        <input type="text" id="owner_name" name="owner_name" value="<?php echo $owner_name;?>" class="form-control">
                      </div>
                      
                      
                      <div class="form-group col-lg-4 col-md-4 col-sm-4">
                        <label for="mobile_number">mobile number</label>
                        
                        <input type="text" id="mobile_number" name="mobile_number" value="<?php echo $mobile_number;?>" class="form-control">
                      </div>
                      
                      
                      <div class="form-group col-lg-4 col-md-4 col-sm-4">
                        <label for="email_id">email</label>
                        
                        <input type="text" id="email_id" name="email_id" value="<?php echo $email_id;?>" class="form-control">
                      </div>
                      
                      
                      </div>
                      </div>
                    </div>

                      
                       <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="card-title m-0">Restraurent_info</h5>
              </div>
              <div class="card-body">
                       <div class="row">
                      <div class="form-group col-lg-4 col-md-4 col-sm-4">
                        <label for="pan_no">pan number</label>
                        
                        <input type="text" id="pan_no" name="pan_no" value="<?php echo $pan_no;?>" class="form-control">
                      </div>
                      
                      
                      <div class="form-group col-lg-4 col-md-4 col-sm-4">
                        <label for="multiFiles2">faasi registration</label>
                         
                          <input type="file" class="form-control"  id="multiFiles2"   multiple  accept="image/x-png,image/jpeg"/>
                        <?php echo $img_add2;?>
                      </div>
                      
                      <div class="form-group col-lg-4 col-md-4 col-sm-4">
                        <label for="gst_regn">GST number</label>
                        
                        <input type="text" id="gst_regn" name="gst_regn" value="<?php echo $gst_regn;?>" class="form-control">
                      </div>
                      </div>
                      </div>
                    </div>
                     
                      
                       <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="card-title m-0">Restraurent_info</h5>
              </div>
              <div class="card-body">
                 <div class="row" style="padding: 0px 20px;">
                      <div class="form-group col-lg-4 col-md-4 col-sm-4">
                   
                         
                        <input type="checkbox" class="form-check-input" value="1" name="is_fevt" id="is_fevt" <?php echo $set_fvt;?>  >
                        <label class="form-check-label"  for="is_fevt"> Mark faverit</label>

                      </div>
                      
                      <div class="form-group col-lg-4 col-md-4 col-sm-4">
                       
                         
                        <input type="checkbox" class="form-check-input" value="1" name="is_top" id="is_top" <?php echo $set_top;?> >
                        <label class="form-check-label"  for="is_top"> set to top </label>
                      </div>
                      
                      
                      </div>
                      </div>
                    </div>
                      
                     
                      
                      
                      <div class="form-check">
                        <input type="checkbox" class="form-check-input" value="1" name="status" id="status" <?php echo $st;?>>
                        <label class="form-check-label"  for="status">Active</label>
                      </div>
                      
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                      <input type="hidden" value="<?php echo $oprn; ?>"  id="oprn" name="oprn">
                      <?php
                      if($oprn=='edit')
                      {
                      ?>
                      <input type="hidden" value="<?php echo $id; ?>"  id="rid" name="rid">
                      <input type="hidden" value="<?php echo $rest_img;?>"   id="rest_img" name="rest_img">
                      <input type="hidden" value="<?php echo $fssai_img;?>"   id="fssai_img" name="fssai_img">
                      <?php
				 
                      }
                      ?>
                      <button type="submit" class="btn btn-primary sub_testi">Submit</button>
                    </div>
                  </form>
                </div>
                <!-- /.card -->
                
              </div>
              <!--/.col (left) -->
              
            </div>
            <!-- /.row -->
            </div><!-- /.container-fluid -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php include('Footer.php');?>
        
      </div>
      <!-- ./wrapper -->
      <?php include('script.php');?>
      <script type="text/javascript">
      $('.sub_testi').on('click', function (e) {
      e.preventDefault();
      $("#upload").css("display","none");
      $("#loader").css("display","block");
      var form_data = new FormData();
      var ins = document.getElementById('multiFiles').files.length;
      for (var x = 0; x < ins; x++) {
      form_data.append("files[]", document.getElementById('multiFiles').files[x]);
      
      }
      var ins2 = document.getElementById('multiFiles2').files.length;
      for (var x = 0; x < ins; x++) {
      form_data.append("files2[]", document.getElementById('multiFiles2').files[x]);
      
      }
     
      form_data.append("rest_name", document.getElementById('rest_name').value);
      form_data.append("rest_address", document.getElementById('rest_address').value); 
      form_data.append("location", document.getElementById('location').options[document.getElementById('location').selectedIndex].value);
      form_data.append("owner_name", document.getElementById('owner_name').value);
      form_data.append("mobile_number", document.getElementById('mobile_number').value);
      form_data.append("email_id", document.getElementById('email_id').value);
      form_data.append("pan_no", document.getElementById('pan_no').value);
      form_data.append("gst_regn", document.getElementById('gst_regn').value);
      form_data.append("opening_time", document.getElementById('opening_time').options[document.getElementById('opening_time').selectedIndex].value);
      form_data.append("closing_time", document.getElementById('closing_time').options[document.getElementById('closing_time').selectedIndex].value);
      form_data.append("is_fevt", document.getElementById('is_fevt').value);
      form_data.append("is_top", document.getElementById('is_top').value);
      form_data.append("rest_type", document.getElementById('rest_type').options[document.getElementById('rest_type').selectedIndex].value);
 
      form_data.append("status", document.getElementById('status').value);
      form_data.append("oprn", document.getElementById('oprn').value);
      /*edit*/
      <?php
      if($oprn=='edit')
      {
      ?>
      
      form_data.append("rid", document.getElementById('rid').value);
      form_data.append("fssai_img", document.getElementById('fssai_img').value);
      form_data.append("rest_img", document.getElementById('rest_img').value);
      <?php
      }
      ?>
      /*edit*/
      $.ajax({
      url: 'vendor_regn_conf.php',
      dataType: 'text',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function (response) {
      alert(response);
      if(response='1')
      {
        document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      $("#result").addClass("alert alert-success fade show");
      $("#result").html("Update information added successfuly ");
      }
      else
      {
      $("#result").addClass("alert alert-danger fade show");
      $("#result").html("Please update proper data.");
      }
      $('#description').val('');
      $(".alert").delay(2000).slideUp(250, function() {
      $(this).alert('close');
      });
      
  location.reload();
      },
      error: function (response) {
      alert(response);
      
      location.reload();
      }
      });
      });
      </script>
    </body>
  </html>