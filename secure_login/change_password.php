<?php

include ('session_check.php');

include('class/Curd.php');

?>

<!DOCTYPE html>
<html>
<?php include('head.php');?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <?php include('nav.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 <?php include('side_menu.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
 

    <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-sm-3"></div>
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Change Password</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

<?php
 
            
             /*SELECT `id`, `mem_id`, `user_id`, `mobile_no`, `user_pwd`, `user_otp`, `mem_type`, `status` FROM `users` WHERE 1*/
              $where3=array("mem_id"=>$mem_id, "mem_type"=>$mem_type );
             $variable3=$obj_curd->display_all_record("users",$where3);
              foreach ($variable3 as $row3) { 
                        $old_password=$row3['user_pwd'];
                     $id=$row3['id'];
                      }
?>

              <form  id="slider_form" method="post" action="" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="col-sm-12">
                    <center><div id="result"></div></center>
                  </div>
               
               
                  <div class="form-group">
                    <label for="cat_name">Old Password</label>
                    
                    <input type="password" id="old_password" name="old_password" value="<?php echo $old_password;?>" class="form-control"> 
                  </div>
                  <div class="form-group">
                    <label for="cat_name">New Password</label>
                    
                    <input type="password" id="new_password" name="new_password"   class="form-control"> 
                  </div>
                 <!--  <div class="form-group">
                    <label for="cat_name">Confirm Password</label>
                    
                    <input type="password" id="conf_password" name="conf_password"  class="form-control"> 
                  </div>
                -->
              
                  
                   
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                <input type="hidden" value="<?php echo $id; ?>"  id="rid" name="rid">
                  <button type="submit" class="btn btn-primary sub_testi">Update</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

   
          </div>
          <!--/.col (left) -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('Footer.php');?>

  
</div>
<!-- ./wrapper -->

<?php include('script.php');?>
<script type="text/javascript">
    $('.sub_testi').on('click', function (e) {
         e.preventDefault();
 $("#upload").css("display","none");
                  $("#loader").css("display","block");
    var form_data = new FormData();
  
   
  form_data.append("new_password", document.getElementById('new_password').value);  
 
 form_data.append("rid", document.getElementById('rid').value); 
 

    $.ajax({
    url: 'update_password.php',
    dataType: 'text',
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
     success: function (response) {
    
       if(response='1')
        {
       
              $("#result").addClass("alert alert-success fade show");
              $("#result").html("Update information added successfuly ");

          }
          else
          {
             $("#result").addClass("alert alert-danger fade show");
              $("#result").html("Please update proper data.");
          } 

      $('#description').val('');  
       $(".alert").delay(2000).slideUp(250, function() {
    $(this).alert('close');
}); 
       location.reload();
    },
    error: function (response) {
      alert(response);
       
                            location.reload();
    }
    });
});   

</script>
</body>
</html>
