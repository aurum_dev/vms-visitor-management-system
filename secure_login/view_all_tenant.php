<?php
include ('session_check.php');
include('class/Curd.php');
?>
<!DOCTYPE html>
<html>
<?php include('head.php');?>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <!-- Navbar -->
    <?php include('nav.php');?>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <?php include('side_menu.php');?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content"  style="padding: 0px">
          <div class="container-fluid">
            <div class="row">
               <h4 class="page-title ">View all tenants</h4>


    
               
                  <!-- /.card-header -->
                  <div class="col-sm-12">
                    <center><div id="result"></div></center>
                  </div>
                  <!-- form start -->
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  
                    <tr> 
             
                      <th>Name</th>  
                      <th>Contact No</th>  
                      <th>Email Id</th>  
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $variable=$obj_curd->display_all_record("tenants",'');
                    foreach ($variable as $row) {
             
                   ?>
                      <tr>
                        <td><?php echo $row['tenant_name'];?></td>
                        <td><?php echo $row['tenant_contact'];?></td>
                        <td><?php echo $row['tenant_email'];?></td>
                         
                       
                        <td><?php 
                        if($row['status']=="1")
                        {
                          ?>
<a href="" class="btn   bg-gradient-success btn-xs"><i class="fas fa-check"></i></a>

                          <?php
                        }
                        else
                        {
                        ?>
                          <a href="" class="btn   bg-gradient-danger  btn-xs"><i class="fas fa-times"></i></a>
                          <?php

}
                          ?>
                        </td> 
                        <td>
                          <a href="tenant.php?opr=edit&id=<?php echo $row['id'];?>" class="btn   bg-gradient-primary btn-sm">Edit</a>
                          <button class="del_record btn  bg-gradient-danger btn-sm" value="<?php echo $row['id'];?>"  >Delete</button>
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tfoot>
                </table>
              </div>
              </div>
              <!-- /.card -->
            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include('Footer.php');?>
  </div>
  <!-- ./wrapper -->
  <?php include('script.php');?>
  <!-- delete -->
  <script type="text/javascript">
    $('.del_record').on('click', function () {
    var oprn1 = $(this).val();
   
       $.ajax({
        type: 'POST', 
        url: 'delete.php',
         
        data: { 'deltype' : 'tanant',   'del_record' : oprn1},
        type: 'post',
        success: function (response) {
      
          if(response='1')
          {

            $("#result").addClass("alert alert-success fade show");
            $("#result").html("Update information added successfuly ");
          }
          else
          {
            $("#result").addClass("alert alert-danger fade show");
            $("#result").html("Please update proper data.");
          }

          $(".alert").delay(2000).slideUp(250, function() {
            $(this).alert('close');
          });
            location.reload();
        },
        error: function (response) {
          alert(response);

          location.reload();
        }
      });
    });
  </script>
  <!-- delete -->
</body>
</html> 