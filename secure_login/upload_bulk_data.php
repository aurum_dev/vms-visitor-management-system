<?php
include ('session_check.php');
include('class/Curd.php');
?>
<!DOCTYPE html>
<html>
  <?php include('head.php');?>
  <link rel="stylesheet" href="../css/validationEngine.jquery.css" type="text/css"/>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <!-- Navbar -->
      <?php include('nav.php');?>
      <!-- /.navbar -->
      <!-- Main Sidebar Container -->
      <?php include('side_menu.php');?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <!-- left column -->
              
              <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Upload visitors</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form method="post" name="frmExcelImport" id="frmExcelImport" action="" enctype="multipart/form-data">
                    <div class="card-body">
                      <div class="col-sm-12">
                        <center><div id="result"></div></center>
                      </div>
                      
                      
                      <div class="card card-primary card-outline">
                        <div class="card-header">
                          <h5 class="card-title m-0">Visitor info</h5>
                        </div>

                        <div class="card-body">
                          <span class="err"></span>
                          <div class="row">
                            <!-- <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data"> -->

                              <div class="form-group col-lg-4 col-md-6 col-sm-6">
                                <label>Choose Excel
                                File</label>
                                <div class="input-group">
                                  <input type="file" name="file" id="file" accept=".xls,.xlsx" class="validate[required]">
                                  <span>Downlaod sample datafile for format <a href="uploads/Visitors.xlsx" download>Download</a>
                                  </span>
                                </div>
                                
                              </div>
                              <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label>Select visit date</label>
                                <div class="input-group">
                                  <input type="text" class="form-control float-right validate[required]" name="visit_date" id="datepicker">
                                </div>
                                
                              </div>
                              <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                
                                <button type="submit" id="submit" name="import"  class="btn-submit">Import</button>
                              </div>
                              
                            <!-- </form> -->
                            
                            
                            
                            
                          </div>
                        </div>
                      </div>
                      
                      
                      
                      
                    </div>
                    
                  </form>
                </div>
                <!-- /.card -->
                
              </div>
              <!--/.col (left) -->
              
            </div>
            <!-- /.row -->
            </div><!-- /.container-fluid -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php include('Footer.php');?>
        
      </div>
      <!-- ./wrapper -->
      <?php include('script.php');?>
      <script src="../js/jquery.validationEngine.min.js" type="text/javascript" charset="utf-8"></script>
      <script src="../js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>       
      <script>
      $( function() {
        $( "#datepicker" ).datepicker({ minDate: 0 });

        $("#frmExcelImport").on('submit', function(e){
          e.preventDefault();
          if(!$("#frmExcelImport").validationEngine('validate', {promptPosition : "inline", scroll: false})){
            return false;
          }
          $.ajax({
            type: 'POST',
            url: 'upload_bulk_data_conf.php',
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('#submit').attr("disabled","disabled");
            },
            success: function(response){
                if(response.status) {
                  $(".err").html(response.msg).css("color","green");
                }else{
                  $(".err").html(response.msg).css("color","red");
                }
                $("#submit").removeAttr("disabled");
            }
          });
          

          return false;
          //upload_bulk_data_conf.php
        });
      });
      </script>
    </body>
  </html>