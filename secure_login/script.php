<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script> 
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote --> 
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script> 
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js" integrity="sha512-AIOTidJAcHBH2G/oZv9viEGXRqDNmfdPVPYOYKGy3fti0xIplnlgMHUGfuNRzC6FkzIo0iIxgFnr9RikFxK+sw==" crossorigin="anonymous"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<!-- <script>"https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"</script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/additional-methods.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
 







 

<!-- Load modal conten-->
<script>
$('.openBtn').on('click',function(){
 var getvalue=$(this).prop("value");
    $('.modal-body').load('order_details.php?oid='+getvalue,function(){
        $('#myModal').modal({show:true});
    });
});
</script>
<!-- Load modal conten-->

    <script>
	jQuery('#datetimepicker').datetimepicker({
		format:'Y-m-d H:i:s',
    minDate:new Date()
	});
	jQuery('.datetimepicker').datetimepicker({
		format:'Y-m-d H:i:s',
    
	});
	
	$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var min = parseInt( $('#min').val(), 10 );
        var max = parseInt( $('#max').val(), 10 );
        var age = parseFloat( data[3] ) || 0; // use data for the age column
 
        if ( ( isNaN( min ) && isNaN( max ) ) ||
             ( isNaN( min ) && age <= max ) ||
             ( min <= age   && isNaN( max ) ) ||
             ( min <= age   && age <= max ) )
        {
            return true;
        }
        return false;
    }
);
	
     $(document).ready(function() {
  

        var table =  $('#example1').DataTable({
        
          "language": {
            search: "",
            searchPlaceholder: "Search records",
            lengthMenu: "_MENU_ Entries",
        },
          lengthChange: true,
          dom: 'lBfrtip',
          buttons: [ 
                'excel' 
        
        ]
    } );
 
 /*      'copy', 'csv', 'excel', 'pdf', 'print'*/
 
    // table.buttons( 1, null ).container().appendTo(
    // '#example_wrapper .col-md-6:eq(0)'
    // );
      
	  
	   $('#min, #max').keyup( function() {
        table.draw();
    } );
	  
      } );

    </script>

    <script>
      $(function() {
        //allow only number
        $('.numberonly').keypress(function (e) {    
            var charCode = (e.which) ? e.which : event.keyCode    
            if (String.fromCharCode(charCode).match(/[^0-9]/g))    
            return false;                        
        });  

        //Get host member using host id
        $("#host_orgn").on("change", async function(){
          var id = $(this).val();
          var data = {id:id, get_host_mem:1};
          try {
            var res = await ajaxRequest('customize_pass.php', data, 'POST', 'json');
            if(res.status) {
              $("#host_name").html('<option value="">Select Host Member</option>');
              $("#host_name").append(res.data);
            }
          }catch(error) {
            console.log(error);
          }
        });
      })

      function ajaxRequest(url, data, type="POST",dataType="JSON", file_param=false) {
        try{
          var ajaxParam = {
            type: type,
            url: url,
            data: data,
            dataType: dataType
          };
          
          if(file_param) {
            ajaxParam.contentType = false; 
            ajaxParam.processData = false;
          }

          return $.ajax(ajaxParam); 
        }catch(error) {
          console.log(error);
        }
        
      }

      function reinitializetable() {
        var table =  $('#example1').DataTable({
            "language": {
                search: "",
                searchPlaceholder: "Search records",
                lengthMenu: "_MENU_ Entries",
            },
            lengthChange: true,
            dom: 'lBfrtip',
            buttons: [ 
              'excel' 
            ],
            scrollY:        "500px",
            scrollX:        true,
            scrollCollapse: true,
            // paging:         true,
            // columnDefs: [
            //     { width: 200, targets: 0 }
            // ],
            // fixedColumns: false
        });

        /*'copy', 'csv', 'excel', 'pdf', 'print'*/

        // table.buttons( 1, null ).container().appendTo(
        //   '#example_wrapper .col-md-6:eq(0)'
        // );
          
        $('#min, #max').keyup( function() {
            table.draw();
        });
      }

    </script>