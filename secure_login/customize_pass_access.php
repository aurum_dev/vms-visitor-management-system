<?php
    include ('session_check.php');
    $user_id = $_SESSION['user_id'];
    $mem_id = $_SESSION['mem_id'];
    $mem_type = $_SESSION['mem_type'];
    include ('class/Curd.php');
    
    if($mem_type != ADMIN) {
        header("Location:dashboard.php"); die;
    }

    if(isset($_POST['getTabledata']) && $_POST['getTabledata'] == 1) {
        $where = $action = "";
        if($mem_type == TENANT_ADMIN) {
            $host_id = $obj_curd->getHost($mem_type, $mem_id);
            $where = ' AND ra.host_id = '.$host_id['id'];
        }
        if($mem_type == TENANT_ADMIN || $mem_type == ADMIN) {
            $action = '<a href="id_display.php?id='.$val['id'].'"<i class="fas fa-trash text-danger"></i></a>';
        }
        $qry = "SELECT ra.*, t.tenant_name, tu.user_name FROM role_access AS ra
                LEFT JOIN tenants AS t ON ra.host_id = t.id
                LEFT JOIN tenants_users AS tu ON tu.id = ra.user_id
                WHERE ra.access_page = 'CUSTOMIZE_PASS' AND ra.status=1 ".$where." ORDER BY ra.id DESC";
        
        $data = $obj_curd->executeRawQuery($qry);
        
        $html="";
        $cnt = 1;
        foreach($data as $key => $val) {
            $html .= '<tr>
                        <td>'.$cnt++.'</td>
                        <td>'.$val['tenant_name'].'</td>
                        <td>'.$val['user_name'].'</td>
                        <td>
                            <a href="#" data-id="'.$val['id'].'" class="delete_record"><i class="fas fa-trash text-danger"></i></a>
                        </td>
                    </tr>';
                                            
        }
        echo json_encode(array("data" => $html, "status" => 1)); die();
    }

    if(isset($_POST['delete_data']) && $_POST['delete_data'] == 1) {
        try{
            $id = $obj_curd->sanitizeData($_POST['id']);
            $status = 0;
            $msg = "Something went wrong";
            if(!empty($id)) {
                $data['status'] = 0;
                $data['updated_by'] = $mem_id;
                $data['updated_at'] = date('Y-m-d H:i:s');
                $action_col = array_keys($data);
                $data['id'] = $id;
                $where = "id = :id";
                
                $res = $obj_curd->pdoUpdatetData("role_access", $data, $action_col, $where);
                
                if($res == 1) {
                    $status = 1;
                    $msg = "Data Deleted Successfully";
                }
            }
            echo json_encode(array('status'=>$status, 'msg' => $msg)); die();
        }catch(Exception $e) {
            echo json_encode(array('status'=>0, 'msg' => $e->getMessage())); die();
        }
        
    }


    if(isset($_POST['submit_customize_pass_access']) && $_POST['submit_customize_pass_access'] == 1) {
        try{
            $status = 0;
            if(!empty($_POST['data'])) {
                $err = []; $msg = $data = ""; $exist= false;
                foreach($_POST['data'] as $key => $val) {
                    $value = $obj_curd->sanitizeData($val['value']);
                    if(!empty($value)) {
                        $form_data[$val['name']] = $value;
                    }else{
                        $err[$val['name']] = 'This field is Required.';
                    }
                }

                if(!empty($err)) {
                    echo json_encode(array('status'=>$status, 'data' => $err, 'msg' => $msg)); die();
                }
                
                $form_data['status'] = 1;
                $form_data['access_page'] = 'CUSTOMIZE_PASS';

                $where_col_name = ['host_id', 'user_id', 'status', 'access_page'];
                $res = $obj_curd->pdoGetData('role_access', $form_data, $where_col_name);
                
                if(COUNT($res) > 0) {
                    $msg = "This User already have access";
                    $exist = true;
                }else {
                    $res= $obj_curd->pdoGetData('users', ['mem_id' => $form_data['user_id']], ['mem_id']);
                    if(COUNT($res) > 0) {
                        $form_data['role_id'] = $res[0]['mem_type'];
                    }

                    $form_data['created_by'] = $mem_id;
                    $form_data['created_at'] = date('Y-m-d H:i:s');

                    $res = $obj_curd->pdoInsertData('role_access', $form_data); 
                    if($res > 0) {
                        $status = 1;
                        $msg = "Access has been provided Successfully.";

                    }else{
                        $msg = $res;
                    }
                }
                
                echo json_encode(array('status'=>$status, 'msg' => $msg, 'data'=>$data)); die(); 
            }
        }catch (Exception $e) {
            echo json_encode(array('status'=>0, 'msg' => $e->getMessage())); die(); 
        }
        
    }


    /**
     * Checking user-type and creating dropdown for host and host member accordingly.
     */
    $hostHtml = $hostMemHtml = "";
    if($mem_type == ADMIN || $mem_type == TENANT_ADMIN) {
        $org_data = $obj_curd->getHost($mem_type, $mem_id);

        if(!is_array($org_data[0])) {
            $org_data = [$org_data];
        }

        $hostHtml = '<div class="form-group col-sm-6 col-xs-12">
                        <label for="usr">Host:</label>
                            <select id="host_orgn" name="host_id" class="form-control input_style ">
                                <option value="">Select Host</option>
                                '.$obj_curd->createSelectOption($org_data, 'id','tenant_name').'
                            </select>
                        </div>';  

        $hostMemHtml = '<div class="form-group col-sm-6 col-xs-12">
                            <label for="usr">Host Member:</label>
                            <select id="host_name" name="user_id" class="form-control input_style ">
                                <option value="">Select Host Member</option>
                            </select>
                        </div>';                
    }

    /**
     * Getting cutomize data
     */
    
?>

<!DOCTYPE html>
<html>
<?php include('head.php');?>

<style>
    label.error {
        color: #e95c5c!important;
    }
    .form-group label:after {
        content:"*";
        color:red;
    }
</style>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="loader" >
        <img src="../images/loader.gif" style="margin-top: 300px;" width="100" height="100">
    </div>
    <div class="wrapper">
        <!-- Navbar -->
        <?php include('nav.php');?>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <?php include('side_menu.php');?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content"  style="padding: 0px">
                <div class="container-fluid">
                    <div class="row">
                        <h4 class="page-title">Customize Pass Access</h4>
                        <!-- /.card-header -->
                        <div class="col-sm-12">
                        <button type="button" class="btn btn-primary mb-4" data-toggle="modal" data-target="#exampleModalCenter">
                            Provide Customize Pass Access
                        </button>
                        </div>
        
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>Sr. No.</th>    
                                    <th>Organisation</th>
                                    <th>Organisation Member</th> 
                                    <th>Action</th>  
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php include('Footer.php');?>
    </div>
    <!-- ./wrapper -->
    

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Provide Customize Pass Access</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button> 
            </div>
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <span id="result"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="row align-items-center justify-content-center" id="custom_pass_access_form" style="padding: 15px;">
                    <div class="col-md-12">
                        <div class="row">
                            <!--- START HOST Orgnazation HTML -->
                            <?php 
                                echo $hostHtml;
                            ?>
                            <!--- END HOST Orgnazation HTML -->

                            <!--- START HOST Member HTML -->
                            <?php 
                                echo $hostMemHtml;
                            ?>
                            <!--- END HOST Member HTML -->
                        </div> 
                    </div>           
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary custom_pass_btn">Create</button>
            </div>
        </div>
    </div>
</div>
<?php include('script.php');?>
<script>
    $(function() {
        getTabledata();
        $('#exampleModalCenter').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
            $("label.error").remove();
            $("#result").html('').parent('.alert').hide();
        })

        var form = $("#custom_pass_access_form");
        form.validate({
            rules: {
                host_id: {
                    required: true
                },
                user_id: {
                    required: true
                },
            }
        });

        $(".custom_pass_btn").click(async function(e) {
            e.preventDefault();
            if(form.valid()) {
                var data = {data: $('#custom_pass_access_form').serializeArray(), submit_customize_pass_access: 1};
                try {
                    $(".loader").show();
                    var res = await ajaxRequest('customize_pass_access.php', data, 'POST', 'json');
                    $(".loader").hide();
                    if(res.status) {
                        $("#exampleModalCenter").modal('toggle');
                        Swal.fire(
                            'Success',
                            res.msg,
                            'success'
                        )
                        getTabledata();
                    }else{
                        if(res.data != "") {
                            Object.entries(res.data).forEach(
                                ([key, value]) => $("#"+key).addClass('error').after('<label for="'+key+'" class="error">'+value+'</label>')
                            );
                        }else{  
                            msg = res.msg;
                            $("#result").html(msg).parent('.alert').show();
                        }
                    }
                }catch(error) {
                    $(".loader").hide();
                    $("#result").html('Something went wrong').parent('.alert').show();
                }
            }
        })

        $("#example1").on("click", ".delete_record", function() {
            var id = $(this).data('id');

            if(id != "") {
                var data = {delete_data: 1, id: id};
                Swal.fire({
                    title: 'Do you want to delete this record?',
                    showDenyButton: true,
                    confirmButtonText: `Yes`,
                    denyButtonText: `No`,
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        try {
                            $(".loader").show();
                            ajaxRequest('customize_pass_access.php', data, 'POST', 'json').then((res)=> {
                                $(".loader").hide();
                                if(res.status) {
                                    Swal.fire('Success', res.msg, 'success');
                                    getTabledata();    
                                }else{
                                    Swal.fire('Error', "Something Went wrong", 'error');
                                }
                            });
                            
                        }catch(error) {
                            $(".loader").hide();
                            Swal.fire('Error', "Something Went wrong", 'error');
                        }
                    }
                })
                
            }else{
                Swal.fire('Error', "Something Went wrong", 'error');
            }
        })

        async function getTabledata() {
            var data = {getTabledata: 1};
                try {
                    $(".loader").show();
                    var res = await ajaxRequest('customize_pass_access.php', data, 'POST', 'json');
                    $(".loader").hide();
                    if(res.status) {
                        $("#example1").DataTable().clear().destroy();
                        $("#example1").find('tbody').html(res.data);
                
                        reinitializetable();
                    }
                }catch(error) {
                    $(".loader").hide();
                    console.log(error);
                    Swal.fire('Error', "Something Went wrong", 'error');
                }
        }

    });
</script>




