<?php
   require_once ('vendor/php-excel-reader/excel_reader2.php');
  require_once ('vendor/SpreadsheetReader.php');
  require '../vendor/sms/Pinnacle.php';
  include ('session_check.php');
  include ('class/Curd.php');

  if(isset($_POST['visit_date']) && !empty($_POST['visit_date'])) {
    if(isset($_FILES['file']) && !empty($_FILES['file'])) {
      $date_pick = date("Y-m-d", strtotime($_POST['visit_date']));
      $mem_id = $_SESSION['mem_id'];
      $where = array('id' => $mem_id);
      $variable = $obj_curd->display_all_record("tenants_users", $where);
      //print_r($variable); die();
      $tenant_id = $variable[0]['tenant_id'];
      $host_mobile_number = $variable[0]['mobile_no'];
      $allowedFileType = ['application/vnd.ms-excel', 'text/xls', 'text/xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
      if (in_array($_FILES["file"]["type"], $allowedFileType)) {
          $targetPath = 'uploads/visitor_upload' . $_FILES['file']['name'];
          if(move_uploaded_file($_FILES['file']['tmp_name'], $targetPath)) {
              $reader = new SpreadsheetReader($targetPath);
              $err_status = false;
              $err=[];
              $data = [];
              foreach ($reader as $key => $Row) {
                  if ($key != '0') {
                      $uniqueId = date('ymd', time()) . mt_rand(1, 999);
                      $visitor_name = "";
                      if (isset($Row[0]) && !empty($Row[0])) {
                          $visitor_name = trim($Row[0]);
                      } else {
                          $err[] = 'Visitor name should not be empty in row '.$key;
                          $err_status = true;
                      }

                      $mobile = "";
                      if (isset($Row[1])) {
                          if(preg_match('/^[0-9]+$/', $Row[1])) {
                              if(strlen($Row[1]) == 10) {
                                  $mobile = trim($Row[1]);
                              }else{
                                  $err[] = 'Invalid mobile number in row '.$key;
                                  $err_status = true;
                              }
                              
                          }else{
                              $err[] = 'Invalid mobile number in row '.$key;
                              $err_status = true;
                          }
                      } else {
                          $err[] = 'Visitor Mobile number should not be empty in row '.$key;
                          $err_status = true;
                      }
                      $email_id = "";
                      if (isset($Row[2])) {
                          $email_id = trim($Row[2]);
                      } else {
                          $err[] = 'Visitor email should not be empty in row '.$key;
                          $err_status = true;
                      }
                      $coming_from = "";
                      if (isset($Row[3])) {
                          $coming_from = trim($Row[3]);
                      }else {
                          $err[] = 'Visitor Coming from required in row '.$key;
                          $err_status = true;
                      }
                      $visitor_org = "";
                      if (isset($Row[4])) {
                          $visitor_org = trim($Row[4]);
                      }else {
                          $err[] = 'Visitor Organisation should not be empty in row '.$key;
                          $err_status = true;
                      }

                      $meeting_purpose = "";
                      if (isset($Row[5])) {
                          $meeting_purpose = trim($Row[5]);
                      }else {
                          $err[] = 'Purpose of visit is required in row '.$key;
                          $err_status = true;
                      } 
                      $vehicle_no = "";
                      if (isset($Row[6])) {
                          $vehicle_no = trim($Row[6]);
                      } 
                      $data[] = [
                        'visitor_id' => $uniqueId,
                        'visitor_name' => $visitor_name,
                        'mobile' => $mobile,
                        'email_id' => $email_id,
                        'coming_from' => $coming_from,
                        'visitor_org' => $visitor_org,
                        'meeting_purpose' => $meeting_purpose,
                        'host_name' => $mem_id,
                        'host_orgn' => $tenant_id,
                        'host_mobile_number' => $host_mobile_number,
                        'vehicle_no' => $vehicle_no,
                        'verified' => 0,
                        'status' => 3,
                        'in_time' => $date_pick,
                        'type' => 'SCHEDULE',
                        'pass_status' => 1
                      ]; 
                  }
              }
          
              if($err_status) {
                  unlink($targetPath);
                  echo json_encode(array('status' => 0, 'msg'=>implode('<br>', $err))); die();
              }else{
                  unlink($targetPath);
                  if(!empty($data)) {
                      // $shortener = new Shortener($db);
                      foreach($data as $key => $val) {
                          $otpCode=$obj_curd->generateNumericOTP(6);
                          $val['otp_code'] = $otpCode;
                          $variable = $obj_curd->add_record1("visitor_info", $val);

                          $shortURL = $obj_curd->createShortenerUrl($variable);
                          //$id = $obj_curd->encrypt_data($variable);
                          
                          // $url = "https://qparc.in/visitor/schedule_visitor_verification.php?".$id; 
                          // $longURL = $url;
                          // // Prefix of the short URL
                          // $shortURL_Prefix = 'https://qparc.in/visitor/'; // without URL rewrite
                          // // Get short code of the URL
                          // $shortCode = $shortener->urlToShortCode($longURL);
                          
                          // // Create short URL
                          // $shortURL = "https://qparc.in/visitor/".$shortCode;
                          //echo $url; die();
                          
                          /*
                          * code to send Verification SMS 
                          */
                          $sms = new Pinnacle();
                          $resp = $sms->sendVerificationSms($otpCode, $shortURL, $val['mobile']);
                          /*
                          * SMS code ends here
                          */
                      }
                      echo json_encode(array('status'=>1,'msg'=>'Data inserted successfully')); die();
                  }
                  echo json_encode(array('status'=>0,'msg'=>'Uploaded File is empty!!!!')); die();
              }
          }else{
            echo json_encode(array('status'=>0,'msg'=>'Something went wrong file not Uploaded!!')); die();
          }          
      }else{
          echo json_encode(array('status'=>0,'msg'=>'File should be excel only!!')); die();
      }
    }
  }


error_reporting(E_ALL);
ini_set('display_errors', 1);
?>


<!DOCTYPE html>
<html>
  <?php include ('head.php'); ?>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <!-- Navbar -->
      <?php include ('nav.php'); ?>
      <!-- /.navbar -->
      <!-- Main Sidebar Container -->
      <?php include ('side_menu.php'); ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <div class="row">
              <!-- left column -->
    
              <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Upload visitors</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <table class="table table-striped">
                    <?php
                      echo"hello"; die();
                      print_r($_POST); die();
                      if (isset($_POST["import"])) {
                          $err = false;
                          if(empty($_POST['visit_date'])) {
                            $err = true;
                            $msg = "Date field is required";
                          }

                          if(isset($_FILES['file']) && !empty($_FILES['file'])) {
                            $err = true;
                            $msg = "Excel field is required";
                          }

                          
                          $date_pick = date("Y-m-d", strtotime($_POST['visit_date']));
                          $mem_id = $_SESSION['mem_id'];
                          /*id*/
                          /*SELECT `id`, `user_name`, `mobile_no`, `mem_email`, `tenant_id`, `status` FROM `tenants_users` WHERE 1*/
                          $where8 = array('id' => $mem_id);
                          $variable8 = $obj_curd->display_all_record("tenants_users", $where8);
                          foreach ($variable8 as $row12) {
                              $tent_id = $row12['tenant_id'];
                          }
                          $where26 = array('id' => $tent_id);
                          $variable26 = $obj_curd->display_all_record("tenants", $where26);
                          foreach ($variable26 as $row26) {
                              echo $allow_accs = $row26['allow_access'];
                          }
                          $status_gt = '3';
                          /*id*/
                          $allowedFileType = ['application/vnd.ms-excel', 'text/xls', 'text/xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
                          if (in_array($_FILES["file"]["type"], $allowedFileType)) {
                              $targetPath = 'uploads/' . $_FILES['file']['name'];
                              move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
                              $Reader = new SpreadsheetReader($targetPath);
                              $sheetCount = count($Reader->sheets());
                              for ($i = 0;$i < $sheetCount;$i++) {
                                  $Reader->ChangeSheet($i);
                                  foreach ($Reader as $key => $Row) {
                                      $uniqueId = date('ymd', time()) . mt_rand(1, 999);
                                      if ($key != '0') {
                                          $visitor_name = "";
                                          if (isset($Row[0])) {
                                              $visitor_name = mysqli_real_escape_string($conn, $Row[0]);
                                          } else {
                                              echo 'no data found';
                                          }
                                          $mobile = "";
                                          if (isset($Row[1])) {
                                              $mobile = mysqli_real_escape_string($conn, $Row[1]);
                                          }
                                          $email_id = "";
                                          if (isset($Row[2])) {
                                              $email_id = mysqli_real_escape_string($conn, $Row[2]);
                                          }
                                          $coming_from = "";
                                          if (isset($Row[3])) {
                                              $coming_from = mysqli_real_escape_string($conn, $Row[3]);
                                          }
                                          $visitor_org = "";
                                          if (isset($Row[4])) {
                                              $visitor_org = mysqli_real_escape_string($conn, $Row[4]);
                                          }
                                          $host_orgn = "";
                                          if (isset($Row[5])) {
                                              $host_orgn = mysqli_real_escape_string($conn, $Row[5]);
                                          }
                                          $host_name = "";
                                          if (isset($Row[6])) {
                                              $host_name = mysqli_real_escape_string($conn, $Row[6]);
                                          }
                                          $meeting_purpose = "";
                                          if (isset($Row[7])) {
                                              $meeting_purpose = mysqli_real_escape_string($conn, $Row[7]);
                                          }
                                          if (!empty($visitor_name) || !empty($visitor_name) || !empty($mobile) || !empty($email_id) || !empty($coming_from) || !empty($host_name) || !empty($meeting_purpose)) {
                                              // echo '<tr><td>'.$date_pick.'</td><td>'.$visitor_name.'</td><td>'.$mobile.'</td><td>'.$email_id.'</td><td>'.$coming_from.'</td><td>'.$host_name.'</td><td>'.$meeting_purpose.'</td><!--<td><span class="badge bg-success">Verified</span></td>--></tr>';
                                              $query = "insert into visitor_info(visitor_id,visitor_name,mobile,email_id,coming_from,visitor_org,host_name,meeting_purpose,verified,status, in_time) values('" . $uniqueId . "','" . $visitor_name . "','" . $mobile . "','" . $email_id . "','" . $coming_from . "','" . $visitor_org . "','" . $mem_id . "','" . $meeting_purpose . "','1','3','" . $date_pick . "');";
                                              $result = mysqli_query($conn, $query);
                                              if (!empty($result)) {
                                                  echo '<tr><td>' . $date_pick . '</td><td>' . $visitor_name . '</td><td>' . $mobile . '</td><td>' . $email_id . '</td><td>' . $coming_from . '</td><td>' . $host_name . '</td><td>' . $meeting_purpose . '</td><td><span class="badge bg-success">Verified</span></td>--></tr>';
                                                  $from = 'itsupport@orize.co.in';
                                                  $to2 = $email_id;
                                                  //$to2='hemant@prolifiquetech.in';
                                                  $subject2 = "Visitor Id";
                                                  $message2 = "
                                                              <html>
                                                              <head>
                                                              <title>Visitor Id Details</title>
                                                              </head>
                                                              <body>
                                                              <p>Dear Sir/Madam,<br> Greetings from Orize !!! Thanks for visit. please use following visitor id to verify as security desk.</p>
                                                              <table style='margin: 0 auto 30px;
                                                                  display: block;
                                                                  max-width: 420px;
                                                                  border: 1px solid #000;
                                                                  border-radius: 10px;
                                                              '>
                                                                              <tr>
                                                                                <td   style='background: #000; color: #fff; font-weight: 700; font-size: 20px; line-height: 23px;padding: 10px 7px;  ' >V<br>I<br>S<br>I<br>T<br>O<br>R</td>
                                                                                <td style='padding: 15px'>
                                                                                  <table>
                                                                                   <!-- <tr >
                                                                                      <td colspan='2' class='text-left'><h3 style='margin-bottom: 5px; font-weight: 700; font-size:24px'>ID: " . $uniqueId . "</h3></td>
                                                                                    
                                                                                    </tr>
                                                                                    <tr>-->
                                                                                    <tr >
                                                                                      <td colspan='2' class='text-left'><h3 style='margin-bottom: 5px; font-weight: 700; font-size:24px' class='text-success'>Verified</h3></td>
                                                                                    
                                                                                    </tr>
                                                                                      <td>
                                                                                        <img src='https://qparc.in/visitor/secure_login/'  style='width: 120px;'>
                                                                                      </td>
                                                                                      <td class='text-left'>
                                                                                        
                                                                                    
                                                                                          <ul style='font-size: 15px; margin-left: 15px; list-style:none'>
                                                                                            <li>Id: <strong>" . $uniqueId . "</strong></li> 
                                                                                            <!--<li>Name: <strong>" . $visitor_name . "</strong></li> -->
                                                                                            <li>From: <strong>" . $coming_from . "</strong></li> 
                                                                                            <li>Meets: <strong>" . $mem_id . "</strong></li> 
                                                                                            <li>Host company: <strong>" . $visitor_org . " </strong></li> 
                                                                                            <li>Purpose: <strong>" . $meeting_purpose . "</strong></li> 
                                                                                            <li>Intime: <strong>" . date_format($date_pick, "d/m/Y") . "</strong></li> 
                                                                                          </ul>
                                                                                     
                                                                                      </td> 
                                                                                    </tr>
                                                                                  </table>
                                                                                </td>
                                                                              </tr>
                                                                            </table>
                                                              </body>
                                                              </html>";
                                                  $headers2 = "MIME-Version: 1.0" . "\r\n";
                                                  $headers2.= "Content-type:text/html;charset=UTF-8" . "\r\n";
                                                  // More headers
                                                  $headers2.= 'From: <' . $from . '>';
                                                  $obj_curd->send_secure_mail($to2, $message2, $subject2);
                                                  //      $message='Hello, your visitor id is '.$uniqueId;
                                                  // $obj_curd->send_sms($message,$mobile);

                                                  /*
                                                  * code to send Visitor Id SMS 
                                                  */
                                                  $sms = new Pinnacle();
                                                  $resp = $sms->sendVisitorId(trim($uniqueId), trim($mobile));
                                                  /*
                                                  * SMS Code Ends here
                                                  */
                                                  
                                              } else {
                                                  $type = "error";
                                                  $message = "Problem in Importing Excel Data";
                                              }
                                          }
                                      }
                                  }
                              }
                          } else {
                              $type = "error";
                              $message = "Invalid File Type. Upload Excel File.";
                          }
                      }
                    ?>
                  </table>
                </div>
                <!-- /.card -->
                
              </div>
              <!--/.col (left) -->
              
            </div>
            <!-- /.row -->
            </div><!-- /.container-fluid -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php include ('Footer.php'); ?>
        
      </div>
      <!-- ./wrapper -->
      <?php include ('script.php'); ?>
 
    </body>
  </html>

