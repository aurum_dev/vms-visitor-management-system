<?php
include ('session_check.php');
include('class/Curd.php');
?>
<!DOCTYPE html>
<html>
<?php include('head.php');?>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <!-- Navbar -->
    <?php include('nav.php');?>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <?php include('side_menu.php');?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">View </h3>
                </div>
                <!-- /.card-header -->
                <div class="col-sm-12">
                    <center><div id="result"></div></center>
                  </div>
                <!-- form start -->
                <div class="col-sm-12">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <!-- SELECT `id`, `rest_name`, `rest_address`, `location`, `owner_name`, `mobile_number`,
					`email_id`, `restn_image`, `pan_no`, `fssai_regn`, `gst_regn`, `opening_time`, `closing_time`,
					`rest_type`, `add_date`, `update_date`, `status`, `is_fevt`, `is_top` FROM `vendor` WHERE 1 -->
                    <tr>
                      <th>Image</th> 
                      <th>Name & address</th>
             
                      <th>Location</th>
                      <th>Contact details</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $variable=$obj_curd->display_all_record("vendor",'');
                    foreach ($variable as $row) {
						if($row['restn_image']!='')
						{
                      $img_add="<img src='".substr($row['restn_image'], 0, -2)."' height='50px' style='margin-top:10px;'>";
						}
						else
						{
						$img_add="";	
						}
                      ?>
                      <tr>
                        <td><?php echo $img_add;?></td>
                        
                        <td><?php echo $row['rest_name'];?><br><?php echo $row['rest_address'];?></td>
                        <td><?php 
						  $where3=array("id"=>$row['location']);
$variable3=$obj_curd->display_all_record("area",$where3);


  foreach ($variable3 as $row3) {
  echo $row3['area_name'];
  }
						
						?></td>
                        <td>
						<?php echo $row['owner_name'];?><br>
						<?php echo $row['mobile_number'];?> 
						</td>
                        <td><?php 
                        if($row['status']=="1")
                        {
                          ?>
<a href="" class="btn   bg-gradient-success btn-xs"><i class="fas fa-check"></i></a>

                          <?php
                        }
                        else
                        {
                        ?>
                          <a href="" class="btn   bg-gradient-danger  btn-xs"><i class="fas fa-times"></i></a>
                          <?php

}
                          ?>
                        </td> 
                        <td>
                          <a href="vendor_regn.php?opr=edit&id=<?php echo $row['id'];?>" class="btn   bg-gradient-primary btn-sm">Edit</a>
                          <button class="del_record btn  bg-gradient-danger btn-sm" value="<?php echo $row['id'];?>"  >Delete</button>
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tfoot>
                </table>
              </div>
              </div>
              <!-- /.card -->
            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include('Footer.php');?>
  </div>
  <!-- ./wrapper -->
  <?php include('script.php');?>
  <!-- delete -->
  <script type="text/javascript">
    $('.del_record').on('click', function () {
    var oprn1 = $(this).val();
    alert(oprn1);
       $.ajax({
        type: 'POST', 
        url: 'delete.php',
         
        data: { 'deltype' : 'vendor',   'del_record' : oprn1},
        type: 'post',
        success: function (response) {
      
          if(response='1')
          {

            $("#result").addClass("alert alert-success fade show");
            $("#result").html("Update information added successfuly ");
          }
          else
          {
            $("#result").addClass("alert alert-danger fade show");
            $("#result").html("Please update proper data.");
          }

          $(".alert").delay(2000).slideUp(250, function() {
            $(this).alert('close');
          });
        },
        error: function (response) {
          alert(response);

          location.reload();
        }
      });
    });
  </script>
  <!-- delete -->
</body>
</html> 