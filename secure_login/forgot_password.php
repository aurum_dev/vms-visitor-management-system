<?php
 // @ob_start();
// session_start();
 //session_unset(); 
 //session_destroy();
?>

<!DOCTYPE html>
<html>
 <?php 
 error_reporting(0);


include ('head.php');
 ?>
<body class="hold-transition login-page" style="background:#172b4d!important;">
<div class="login-box">

  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body"> 
        <div class="login-logo text-center">
   <img src="dist/logo-icon.png">
  </div>
      <h3 class="text-center">Forgot Password</h3>
 <div class="col-sm-12">
                    <center><div id="result"></div></center>
                  </div>
      <form action="" method="post" >
  <div class="input-group mb-3">
       
         <select class="form-control" id="visitor_type">
           <option>Select User Type</option>
           
           <!--<option value="1">2</option>-->
           <option value="1">Security Desk</option>
           <option value="5">User</option>
         </select>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" id="user_id" placeholder="User Id">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
      
     
        <div class="row">
          <div class="col-6">
             
          </div>
          <!-- /.col -->
          <div class="col-6">
            <button type="submit" class="btn btn-primary btn-block btn-flat sub_testi">Reset Password</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
 
 
  
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
 <?php 
include ('script.php');
 ?>
 <script type="text/javascript">
    $('.sub_testi').on('click', function (e) {
         e.preventDefault();
 $("#upload").css("display","none");
                  $("#loader").css("display","block");
    var form_data = new FormData();
 
   
  form_data.append("user_id", document.getElementById('user_id').value); 
  form_data.append("visitor_type", document.getElementById('visitor_type').value); 
 
 

    $.ajax({
    url: 'forgot_pwd.php',
    dataType: 'text',
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
     success: function (response) {
 
       if(response==1)
        {
       
              $("#result").addClass("alert alert-success fade show");
              $("#result").html("Update information added successfuly ");
window.location = 'verify_otp.php';
          }
          else
          {
             $("#result").addClass("alert alert-danger fade show");
              $("#result").html("Please update proper data.");
          } 

      $('#description').val('');  
       $(".alert").delay(2000).slideUp(250, function() {
    $(this).alert('close');
}); 
    },
    error: function (response) {
      alert(response);
       
                            location.reload();
    }
    });
});   

</script>

</body>
</html>
