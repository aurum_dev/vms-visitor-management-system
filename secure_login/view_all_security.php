<?php
include ('session_check.php');
include('class/Curd.php');
?>
<!DOCTYPE html>
<html>
<?php include('head.php');?>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <!-- Navbar -->
    <?php include('nav.php');?>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <?php include('side_menu.php');?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Main content -->
     <section class="content"  style="padding: 0px">
          <div class="container-fluid">
            <div class="row">
               <h4 class="page-title ">View all securities</h4>


    
               
                  <!-- /.card-header -->
                  <div class="col-sm-12">
                    <center><div id="result"></div></center>
                  </div>
                  <!-- form start -->
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <!-- SELECT `id`, `security_location`, `contact_no`, `email`, `status`, `add_date`, `update_date` FROM `security` WHERE 1-->
                    <tr> 
             
                      <th>Location</th> 
                      <th>Contact No</th> 
                      <th>Email</th> 
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $variable=$obj_curd->display_all_record("security",'');
                    foreach ($variable as $row) {
						 
                   ?>
                      <tr><!-- SELECT `id`, `location_name`, `status` FROM `locations` WHERE 1 -->
                        <td><?php
						
						 $where = array('id' =>$row['security_location']);
   $variable2=$obj_curd->display_all_record("locations",$where);
                    foreach ($variable2 as $row2) {
						echo $row2['location_name'];
					} 

						?></td>
                        <td><?php echo $row['contact_no'];?></td>
                        <td><?php echo $row['email'];?></td>
                         
                       
                        <td><?php 
                        if($row['status']=="1")
                        {
                          ?>
<a href="" class="btn   bg-gradient-success btn-xs"><i class="fas fa-check"></i></a>

                          <?php
                        }
                        else
                        {
                        ?>
                          <a href="" class="btn   bg-gradient-danger  btn-xs"><i class="fas fa-times"></i></a>
                          <?php

}
                          ?>
                        </td> 
                        <td>
                          <a href="register_security_desk.php?opr=edit&id=<?php echo $row['id'];?>" class="btn   bg-gradient-primary btn-sm">Edit</a>
                          <button class="del_record btn  bg-gradient-danger btn-sm" value="<?php echo $row['id'];?>"  >Delete</button>
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tfoot>
                </table>
              </div>
              </div>
              <!-- /.card -->
            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include('Footer.php');?>
  </div>
  <!-- ./wrapper -->
  <?php include('script.php');?>
  <!-- delete -->
  <script type="text/javascript">
    $('.del_record').on('click', function () {
    var oprn1 = $(this).val();
   
       $.ajax({
        type: 'POST', 
        url: 'delete.php',
         
        data: { 'deltype' : 'security',   'del_record' : oprn1},
        type: 'post',
        success: function (response) {
      
          if(response='1')
          {

            $("#result").addClass("alert alert-success fade show");
            $("#result").html("Update information added successfuly ");
          }
          else
          {
            $("#result").addClass("alert alert-danger fade show");
            $("#result").html("Please update proper data.");
          }

          $(".alert").delay(2000).slideUp(250, function() {
            $(this).alert('close');
          });
          location.reload();
        },
        error: function (response) {
          alert(response);

          location.reload();
        }
      });
    });
  </script>
  <!-- delete -->
</body>
</html> 