<?php
include ('session_check.php');
$user_id = $_SESSION['user_id'];
$mem_id = $_SESSION['mem_id'];
$mem_type = $_SESSION['mem_type'];
$oprn = $_GET['opr'];
include ('class/Curd.php');
if ($oprn == 'add')
{
    $area_name = "";
    $delivery_charge = "";

    $st = 'checked';

}
else
{
    /*SELECT `id`, `tenant_name`, `tenant_email`, `tenant_contact`, `status`, `add_info_date`, `update_info_date` FROM `tenants` WHERE 1*/
    $id = $_GET['id'];

    $where = array(
        "id" => $id
    );

    if ($mem_type == '5')
    {
        /*SELECT `id`, `user_name`, `mobile_no`, `mem_email`, `tenant_id`, `status` FROM `tenants_users` WHERE 1*/
        $variable = $obj_curd->display_all_record("tenants_users", $where);
        foreach ($variable as $row)
        {
            $tenant_name = $row['user_name'];
            $tenant_email = $row['mem_email'];
            $tenant_contact = $row['mobile_no'];
            $tenant_id = $row['tenant_id'];

            $status = $row['status'];
            if ($status == '1')
            {
                $st = "checked";
            }
            else
            {
                $st = '';
            }
        }
    }
    else
    {
        $variable = $obj_curd->display_all_record("tenants", $where);
        foreach ($variable as $row)
        {
            $tenant_name = $row['tenant_name'];
            $tenant_email = $row['tenant_email'];
            $tenant_contact = $row['tenant_contact'];

            $status = $row['status'];
            if ($status == '1')
            {
                $st = "checked";
            }
            else
            {
                $st = '';
            }
        }
    }
}

    /**
     * Checking user-type and creating dropdown for host and host member accordingly.
     */
    $dropdown = "";
    if($mem_type == ADMIN || $mem_type == TENANT_ADMIN) {
        $org_data = $obj_curd->getHost($mem_type, $mem_id);
        
        if(!is_array($org_data[0])) {
            $org_data = [$org_data];
        }

        $dropdown = '<div class="form-group col-sm-12 col-xs-11">
                      <label for="usr">Host organisation: </label>
                      <select id="host_orgn" name="host_org" class="form-control input_style validate[required]">
                        <option value="">Select Organization</option>'
                          .$obj_curd->createSelectOption($org_data, "id","tenant_name").
                        '</select>
                      </div>  
                      <div class="form-group col-sm-12 col-xs-11">
                      <label for="host_org">Host Name: </label>
                      <select id="host_name" name="host_name" class="form-control input_style validate[required]" >
                        <option value="">Select Host</option>
                      </select>  
                    </div>';
                      
    }
?>
<!DOCTYPE html>
<html>
<style>
    .loader {
  
        margin:auto;
        position:fixed;
        z-index:  999;
        text-align: center;
        width: 100%;
        height: 100%;
        background: grey;
        opacity: 0.5;
        display: none;
      }
  </style>
<?php include ('head.php'); ?>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="loader" >
        <img src="../images/loader.gif" style="margin-top: 300px;" width="100" height="100">
    </div>
  <div class="wrapper">
    <!-- Navbar -->
    <?php include ('nav.php'); ?>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <?php include ('side_menu.php'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row align-items-center justify-content-center">
            <!-- left column -->
            
            <div class="col-md-8">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title"><?php echo $oprn ?> Visitor</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <div id="result" class=""></div>
                <form class="row align-items-center justify-content-center" id="slider_form" method="post" action="" enctype="multipart/form-data" style="padding:  15px;">
                    
                    <div class="form-group col-sm-4 col-xs-11">
                      <label for="usr">Name: </label>
                      <input type="text" class="form-control input_style validate[required]" id="vist_name" placeholder="Name">
                    </div>
                    <div class="form-group col-sm-4 col-xs-11">
                      <label for="usr">Mobile No:</label>
                      <input type="text" class="form-control input_style validate[required]" id="vist_mobile" placeholder="Mobile">
                    </div>
                    <div class="form-group col-sm-4 col-xs-11">
                      <label for="usr">Email Id:</label>
                      <input type="email" class="form-control input_style validate[required]" id="vist_email" placeholder="Email">
                    </div>
                    <div class="form-group col-sm-4 col-xs-11">
                      <label for="usr">Coming from:</label>
                      <input type="text" class="form-control input_style validate[required]" id="vist_comming_from" placeholder="Coming From">
                    </div>
                    <div class="form-group col-sm-8 col-xs-11">
                      <label for="usr">Your Organisation:</label>
                      <input type="text" class="form-control input_style validate[required]" id="vist_org" placeholder="Organisation">
                    </div>
                    <div class="col-sm-12">
                      <div class="row ">
                        <div class="col-sm-7" style="padding: 0px">
                          <div class="form-group col-sm-12 col-xs-11">
                            <label for="usr">Your purpose of visit:</label>
                            <textarea class="form-control input_style validate[required]" id="visit_purpose" rows="1" placeholder="Visit purpose"></textarea> 
                          </div>
                          <div class="form-group col-sm-12 col-xs-11">
                            <label>Select visit date</label>
                              <input type="text" class="form-control float-right input_style validate[required]" name="visit_date" id="datetimepicker" placeholder="Date">
                          </div>
                        </div>
                        <div class="col-sm-5" style="padding: 0px">
                          <?php echo $dropdown;?>
                        </div>
                      </div>  
                    </div>
                    <div class="form-group col-sm-12 text-right">
                      <button  type="submit" class="btn btn-primary sub_testi">Book Visit</button>
                    </div>
                  </form>
              </div>
              <!-- /.card -->

            </div>
            <!--/.col (left) -->

          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include ('Footer.php'); ?>

  </div>
  <!-- ./wrapper -->
  <?php include ('script.php'); ?>
    <script src="../js/jquery.validationEngine.min.js" type="text/javascript" charset="utf-8"></script>
      <script src="../js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
    
  <script type="text/javascript">
      $('.sub_testi').on('click', function (e) {
        e.preventDefault();
          if(!$("#slider_form").validationEngine('validate', {promptPosition : "inline", scroll: false})){
          return false;
        }
        
        $("#upload").css("display","none");
        $("#loader").css("display","block");
        var form_data = new FormData();
        
        form_data.append("vist_name", document.getElementById('vist_name').value);
        form_data.append("vist_mobile", document.getElementById('vist_mobile').value);
        form_data.append("vist_email", document.getElementById('vist_email').value);
        form_data.append("vist_comming_from", document.getElementById('vist_comming_from').value);
        form_data.append("vist_org", document.getElementById('vist_org').value);
        form_data.append("vistdate", document.getElementById('datetimepicker').value);
        form_data.append("host_org", document.getElementById('host_org').options[document.getElementById('host_org').selectedIndex].value);
        // form_data.append("host_name", document.getElementById('host_name').options[document.getElementById('host_name').selectedIndex].value);
        var myEle = document.getElementById("host_name");
        if(myEle){
            form_data.append("host_name", document.getElementById('host_name').options[document.getElementById('host_name').selectedIndex].value);
        }else{
            form_data.append("host_name", document.getElementById('host_person').value);
        }
        
        
        form_data.append("host_person", document.getElementById('host_person').value);
        form_data.append("visit_purpose", document.getElementById('visit_purpose').value);
        form_data.append("add_visitor", 1);
        $(".loader").show();
        $.ajax({
          url: 'visitor_conf.php',
          dataType: 'json',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,
          type: 'post',
          success: function (res) {
            $(".loader").hide();
            // For Chrome, Firefox, IE and Opera
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0;
            if(res.status == 1)
            {
              $("#result").addClass("alert alert-success fade show");
              $("#result").html(res.msg);
              $("#slider_form")[0].reset();
            }
            else if(res.status == 0) {
              $("#result").addClass("alert alert-error fade show");
              $("#result").html(res.msg);
            }
            else
            {
              $("#result").addClass("alert alert-danger fade show");
              $("#result").html("Please update proper data.");
            }
          },
          error: function (response) {
            $(".loader").hide();
            $("#result").addClass("alert alert-error fade show");
            $("#result").html("Something went wrong, Kindly inform to admin.");
          }
        });
      });
    </script>
    <script>
      $( function() {
      $( "#datepicker" ).datepicker({ minDate: 0 });
      } );
      </script>
</body>
</html>
