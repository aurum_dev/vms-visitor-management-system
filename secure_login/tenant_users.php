<?php
include ('session_check.php');
$oprn=$_GET['opr'];
include('class/Curd.php');
if($oprn=='add')
{
$member_name="";
$tenant_name="";
$mem_email="";
$mobile_no="";
$st='checked';
}
else
{
/*SELECT `id`, `user_name`, `mobile_no`, `mem_email`, `tenant_id`, `status` FROM `tenants_users` WHERE 1*/
$id=$_GET['id'];
$where=array("id"=>$id);
$variable=$obj_curd->display_all_record("tenants_users",$where);
foreach ($variable as $row) {
$member_name=$row['user_name'];
$tenant_name=$row['tenant_id'];
$mem_email=$row['mem_email'];
$mobile_no=$row['mobile_no'];
$status=$row['status'];
if($status=='1')
{
$st="checked";
}
else
{
$st='';
}
}
}
?>
<!DOCTYPE html>
<html>
  <?php include('head.php');?>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <!-- Navbar -->
      <?php include('nav.php');?>
      <!-- /.navbar -->
      <!-- Main Sidebar Container -->
      <?php include('side_menu.php');?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <div class="row align-items-center justify-content-center">
              <!-- left column -->
              
              <div class="col-md-8">
                <!-- general form elements -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title"><?php echo $oprn?> Tenant User</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form class="row align-items-center justify-content-center" id="slider_form" method="post" action="" enctype="multipart/form-data" style="padding:  15px;">
                    <div class="col-sm-12" style="margin-top:30px;"><div id="result"></div></div>
                    <div class="form-group col-sm-4 col-xs-11">
                      <label for="usr">Name: </label>
                      
                      <input type="text" class="form-control input_style validate[required]" id="tenant_name" value="<?php echo $member_name;?>">
                    </div>
                    <div class="form-group col-sm-4 col-xs-11">
                      <label for="usr">Mobile No:</label>
                      <input type="number" class="form-control input_style validate[required]" id="tenant_mobile" value="<?php echo $mobile_no;?>">
                    </div>
                    <div class="form-group col-sm-4 col-xs-11">
                      <label for="usr">Email Id:</label>
                      <input type="email" class="form-control input_style validate[required]" id="tenant_email" value="<?php echo $mem_email;?>">
                    </div>
                    
                    
                    <div class="col-sm-12">
                      <div class="row ">
                        <div class="col-sm-5" style="padding: 0px">
                          <div class="form-group col-sm-12">
                            <label for="usr">User organisation:</label>
                            <select id="host_org" name="host_org" class="form-control input_style validate[required]" <?php if($mem_type=='2'){?> disabled <?php }?>>
                              <option value="">Select Organization</option>
                              <?php
                              /*SELECT `id`, `tenant_name`, `tenant_email`, `tenant_contact`, `status`, `add_info_date`, `update_info_date` FROM `tenants` WHERE 1*/
                              $where=array("status"=>'1');
                              $variable=$obj_curd->display_all_record("tenants",$where);
                              foreach ($variable as $row) {
                              if($mem_type=='2')
                              {
                              if($row['id']==$mem_id)
                              {
                              $select_location='selected';
                              }
                              else
                              {
                              $select_location='';
                              }
                              }
                              else
                              {
                              if($row['id']==$tenant_name)
                              {
                              $select_location='selected';
                              }
                              else
                              {
                              $select_location='';
                              }
                              }
                              ?>
                              <option value="<?php echo $row['id'];?>" <?php echo $select_location;?>><?php echo $row['tenant_name'];?></option>
                              <?php
                              }
                              ?>
                            </select>
                          </div>
                          
                        </div>
                        
                      </div>
                    </div>
                    
                    
                    <div class="form-group col-sm-12 text-right">
                      <input type="hidden" value="<?php echo $oprn; ?>"  id="oprn" name="oprn">
                      <?php
if($oprn=='edit')
{
  ?>
 <input type="hidden" value="<?php echo $id; ?>"  id="rid" name="rid"> 
  <?php
}

                  ?>
                      <button  type="submit" class="btn btn-primary sub_testi">Submit</button>
                    </div>
                  </form>
                </div>
                <!-- /.card -->
              </div>
              <!--/.col (left) -->
            </div>
            <!-- /.row -->
            </div><!-- /.container-fluid -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php include('Footer.php');?>
      </div>
      <!-- ./wrapper -->
      <?php include('script.php');?>
       <script src="../js/jquery.validationEngine.min.js" type="text/javascript" charset="utf-8"></script>
      <script src="../js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
 
      <script type="text/javascript">
      $('.sub_testi').on('click', function (e) {
      e.preventDefault();
        if(!$("#slider_form").validationEngine('validate', {promptPosition : "inline", scroll: false})){
      return false;
      }
      $("#upload").css("display","none");
      $("#loader").css("display","block");
      var form_data = new FormData();
      
      form_data.append("tenant_name", document.getElementById('tenant_name').value);
      form_data.append("tenant_mobile", document.getElementById('tenant_mobile').value);
      form_data.append("tenant_email", document.getElementById('tenant_email').value);
      form_data.append("host_org", document.getElementById('host_org').options[document.getElementById('host_org').selectedIndex].value);
      form_data.append("oprn", document.getElementById('oprn').value);
      form_data.append("oprn", document.getElementById('oprn').value);
      <?php
if($oprn=='edit')
{
  ?>
  form_data.append("rid", document.getElementById('rid').value);
      
      
       <?php
}

                  ?> 
                  document.getElementById("result").innerHTML='<center><img src="../images/Spin-1s-98px.gif" style="width:100px"></center>';
      $.ajax({
      url: 'tenant_user_conf.php',
      dataType: 'text',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function (response) {
     // $("#result").html(response);
    
      var res=response.trim();
    
      if(res=='1')
      {
              document.body.scrollTop=0; // For Safari
              document.documentElement.scrollTop=0; // For Chrome, Firefox, IE and Opera
              $("#result").addClass("alert alert-success fade show");
              $("#result").html("Tenant User Information added successfuly!");
              alert("Tenant User Information added successfuly!");
      }
      else if(res=='2')
      {
              $("#result").addClass("alert alert-danger fade show");
              $("#result").html("This data already Exist. Please try again");
              alert("This data already Exist. Please try again");
      }
      else
      {
              $("#result").addClass("alert alert-danger fade show");
              $("#result").html("Please update proper data.");
              alert("Please update proper data.");
      }
      $('#description').val('');
      $(".alert").delay(2000).slideUp(250, function() {
      $(this).alert('close');
      }); 
       location.reload(); 
      },
       error: function (response) {      
       location.reload();
      } 
      });
      });
      </script>
      <script>
      $( function() {
      $( "#datepicker" ).datepicker({ minDate: 0 });
      } );
      </script>
    </body>
  </html>