<?php
include ('session_check.php');
$user_id = $_SESSION['user_id'];
$mem_id = $_SESSION['mem_id'];
$mem_type = $_SESSION['mem_type'];
include ('class/Curd.php');
require '../vendor/sms/Pinnacle.php';

if(isset($_POST['add_visitor']) && $_POST['add_visitor'] == 1) {

	$err_status = false;
	$err = [];

	if (!empty($_POST['vist_mobile'])) {
		$mob = trim($_POST['vist_mobile']);
    if(preg_match('/^[7-9][0-9]{9}+$/', $mob)) {
      if(strlen($mob) == 10) {
          $mobile = $mob;
      }else{
          $err[] = 'Visitor Mobile number is not valid';
          $err_status = true;
      }
          
    }else{
      $err[] = 'Visitor Mobile number is not valid';
      $err_status = true;
    }
  } else {
      $err[] = 'Visitor Mobile number is required';
      $err_status = true;
  }

  if (!empty($_POST['vist_email'])) {
      $email_id = trim($_POST['vist_email']);
      if(!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i', $email_id)) {
      		$err[] = 'Visitor email is invalid';
      		$err_status = true;
      }
  } else {
      $err[] = 'Visitor email is required';
      $err_status = true;
  }

  if($err_status) {

  	echo json_encode(array("status" => 0, "msg" => implode("<br>", $err))); die();
  }

  	if($mem_type == TENANT_USER) {
		$form_data['host_name'] = trim($mem_id);
		$form_data['host_org'] = trim($obj_curd->getHostByMemId($mem_id, $mem_type)['id']);
	}

	$uniqueId= date('ymd', time()).mt_rand(1,999);
	$otpCode=$obj_curd->generateNumericOTP(6);
	// $image_name=$obj_curd->img_new_fun();
	// $image_name2=$obj_curd->img_new_fun2();
	$date_pick=date("Y-m-d h:i:s", strtotime($_POST['vistdate']));
	$about_data = array(
		'visitor_id' =>$uniqueId,
		'visitor_name' =>$_POST['vist_name'],
		'mobile' =>$mobile,
		'email_id' =>$email_id,
		'coming_from' =>$_POST['vist_comming_from'],
		'visitor_org' =>$_POST['vist_org'],
		'host_orgn' =>$_POST['host_org'],
		'in_time' =>$date_pick,
		'host_name' =>$_POST['host_name'],
		'meeting_purpose' =>$_POST['visit_purpose'],
		'verified' => 0,
		'status' => 3,
		'otp_code' => $otpCode,
		'type' => 'SCHEDULE',
		'pass_status' => 1
	);

	$id = $obj_curd->add_record1("visitor_info", $about_data);

	if(!empty($id)) {
		$shortURL = $obj_curd->createShortenerUrl($id);
	// 	$shortener = new Shortener($db);
		
	// 	$id = $obj_curd->encrypt_data($id);
		
	// 	//$shortURL_Prefix = 'https://vsitr_uat.recx.in/visitor/'; // without URL rewrite
	//   	$shortURL_Prefix = 'https://qparc.in/visitor/'; // without URL rewrite

	// 	// $url = "https://vsitr_uat.recx.in/visitor/schedule_visitor_verification.php?".$id; 
	// 	$url = $shortURL_Prefix."schedule_visitor_verification.php?".$id;
	//   	$longURL = $url;
	//   // Prefix of the short URL
	  
	//   // Get short code of the URL
	//   $shortCode = $shortener->urlToShortCode($longURL);
	      
	//   // Create short URL
	//   // $shortURL = "https://vsitr_uat.recx.in/visitor/".$shortCode;
	//   $shortURL = $shortURL_Prefix.$shortCode;

	/*
	* code to send Verification SMS 
	*/
	$sms = new Pinnacle();
	$resp = $sms->sendVerificationSms($otpCode, $shortURL, $mobile);
	/*
	* SMS code ends here
	*/

  	/*
		 * code to send email
    */
  	
   	$from = 'itsupport@orize.co.in'; 
		$to = $email_id;
		$subject ="Visitor Verification -Aurum Orize";
		$message ="
			<html>
			<head>
			<title>Visitor Id Details</title>
			</head>
			<body>
			<p>Dear Sir/Madam,<br> Your OTP for verification to VMS Portal QPARC is ".$otpCode.". Valid for 30 minutes. Please do not share this OTP. Please <a href=".$shortURL.">click here</a> to verify your OTP.</p> 
			</body>
			</html>
			";

		$headers2 = "MIME-Version: 1.0" . "\r\n";
		$headers2 .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers2 .= 'From: <'.$from.'>';
		$a = $obj_curd->send_secure_mail($to, $message, $subject);
		//print_r($a); die();
		echo json_encode(array("status" => 1, "msg" => "Visitor inserted successfully. <br>Email and Message sent to visitor for verification")); die();
      	//echo $text; die();
      	
	}else{
		echo json_encode(array("status" => 0, "msg" => "Something went wrong data not inserted")); die();
	}
} 
?>