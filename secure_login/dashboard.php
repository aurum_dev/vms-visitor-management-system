<?php
include ('session_check.php');
include('class/Curd.php');
?>
<!DOCTYPE html>
<html>
  <?php
  include ('head.php');
  ?>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <!-- Navbar -->
      <?php include('nav.php');?>
      <!-- /.navbar -->
      <?php
      include('side_menu.php');
      ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                  </ol>
                  </div><!-- /.col -->
                  </div><!-- /.row -->
                  </div><!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->
                <!-- Main content -->
                <section class="content">
                  <div class="container-fluid">
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                      <div class="col-sm-12 clearfix">
                        
                      </div>
                      
                      <!-- ./col -->
                      <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                          <div class="inner">
                            <h3> <?php
                      if($mem_type=='2')
                    {
                      
                      $where = array('host_orgn' =>$mem_id, 'status' =>'0', 'verified' =>'0');
                    }
					else if($mem_type=='5')
					{
						$where = array('host_name' =>$mem_id, 'status' =>'0', 'verified' =>'0');
					}
                  else
                  {
                     $where = array('status' =>'0', 'verified' =>'0');
                  }
                    $variable=$obj_curd->display_all_recordCount("visitor_info",$where);
                    echo $variable;
             
                   ?> </h3>
                            <p>Visitor Request</p>
                          </div>
                          <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                          </div>
                          <a href="new_request.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                      </div>
                      <!-- ./col -->
                      <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                          <div class="inner">
                            <h3> <?php
                    if($mem_type=='2')
                    {
                      $where = array('host_orgn' =>$mem_id, 'status' =>'1', 'verified' =>'1');
                    }
					else if($mem_type=='5')
					{
						$where = array('host_name' =>$mem_id, 'status' =>'1', 'verified' =>'1');
					}
                  else
                  {
                     $where =array('status' =>'1', 'verified' =>'1');
                  }
                    $variable=$obj_curd->display_all_recordCount("visitor_info",$where);
                    echo $variable;
             
                   ?></h3>
                            <p>current Visitors</p>
                          </div>
                          <div class="icon">
                            <i class="ion ion-person-add"></i>
                          </div>
                          <a href="active_visitors.php" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                      </div>
                      <!-- ./col -->
                   
                      <!-- ./col -->
                    </div>
                    <!-- /.row -->
                    
                    </div><!-- /.container-fluid -->
                  </section>
                  <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->
                <?php
                include ('footer.php');
                ?>
                
                <!-- /.control-sidebar -->
              </div>
              <!-- ./wrapper -->
              <!-- jQuery -->
              <?php
              include ('script.php');
              ?>

              <script type="text/javascript" src="https://cdn.onesignal.com/sdks/OneSignalSDKWorker.js"></script>  
              <script>
              var OneSignal = window.OneSignal || [];
              OneSignal.push(function() {
              OneSignal.init({
              appId: "06c52fb2-3c07-48c2-8105-c2c3876f6265",
              });
              OneSignal.getUserId(function(userId) {
              console.log("OneSignal User ID:", userId);
              
              var dataString  = { 'user_os_id': userId };
              $.ajax({
              type: "POST",
              url: "update_user_id_one.php",
              data: "user_os_id=" + userId,
              success: function (response) {
              
              //alert(response);
              
              
              },
              error: function (response) {
              
              
              
              }
              });
              
              });
              });
              </script>
            </body>
          </html>