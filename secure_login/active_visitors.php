<?php
include ('session_check.php');
include('class/Curd.php');
?>
<!DOCTYPE html>
<html>
<?php include('head.php');?>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <!-- Navbar -->
    <?php include('nav.php');?>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <?php include('side_menu.php');?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Main content -->
     <section class="content"  style="padding: 0px">
          <div class="container-fluid">
            <div class="row">
               <h4 class="page-title ">Current Visitors</h4>
                  <!-- /.card-header -->
                  <div class="col-sm-12">
                    <center><div id="result"></div></center>
                  </div>
                  <!-- form start -->
		 <div class="col-sm-12">
				  <form action="#" method="POST">
<div class="row" style="margin-bottom:25px;">

<div class="col-sm-5"> 
<input type="text" name="start_date" id="start_date" required placeholder="Start Date" class="form-control datetimepicker">
</div>
<div class="col-sm-5"> 
<input type="text" name="end_date" id="end_date"  placeholder="End Date" class="form-control datetimepicker">
</div>
<div class="col-sm-2"><button type="submit" class="btn btn-success">Submit</button></div>
 </div>
 </form>
 </div>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <!-- SELECT `id`, `visitor_name`, `mobile`, `email_id`, `coming_from`, `visitor_org`, `photo`, `persons_alogn_with_visitor`, `host_orgn`, `host_name`, `meeting_purpose`, `vehicle_no`, `signature`, `in_time`, `out_time`, `verified`, `status`, `add_date`, `update_date`, `visitor_id` FROM `visitor_info` WHERE 1 -->
                    <tr> 
             
                      <th>Sr. No.</th>
                      <th>Visitor ID</th>  
                      <th>Name</th>  
                      <th>Contact Info</th>  
                      <th>Phone</th>  
                      <th>Host</th>
                      <th>Meet to</th>
                      <th>Meet purpose</th>
                      <th>Date</th>  
                      <th>In Time</th>  
                      <th>Out Time</th>  
                      <th>Status</th>  
                    </tr>
                  </thead>
                  <tbody>
                    <?php
					$i=0;
                    if($mem_type=='2')
                    {
                      $where = array('host_orgn' =>$mem_id, 'status' =>'1', 'verified' =>'1');
                    }
					          else if($mem_type=='5')
                    {
                      
                      $where = array('host_name' =>$mem_id, 'status' =>'1', 'verified' =>'1');
                    }
                    else
                    {
                      $where =array('status' =>'1', 'verified' =>'1');
                    }
                    $variable=$obj_curd->display_all_record("visitor_info",$where, 'desc', 'id');
                    foreach ($variable as $row) {
             if(isset($_POST['start_date']) && isset($_POST['end_date']))
			 {  
				/* echo $_POST['start_date'];*/
				if((strtotime($_POST['start_date'])<=strtotime($row['in_time'])) && (strtotime($_POST['end_date'])>=strtotime($row['in_time'])) )
				{
					//echo 'Call';
				 ?>
				 <tr>
					  <td><?php echo $i+1;?></td>
                        <td><?php echo $row['visitor_id'];?></td>
                        <td><?php echo $row['visitor_name'];?></td>
                        <td><?php echo $row['email_id'];?> </td>
                        <td><?php echo $row['mobile'];?> </td>
                         
                        <td><?php 
                        //SELECT `id`, `tenant_name`, `tenant_email`, `tenant_contact`, `status`, `add_info_date`, `update_info_date` FROM `tenants` WHERE 1
                          $where2 = array('id' =>$row['host_orgn']);
                        $variable2=$obj_curd->display_all_record("tenants",$where2);
                        foreach ($variable2 as $row2) {
                            echo $row2['tenant_name'];
                        }

                        ?></td>
                        <td><?php  $where11 = array('id' =>$row['host_name']);
              
                        $variable11=$obj_curd->display_all_record("tenants_users",$where11);
                        foreach ($variable11 as $row11) {
                          echo $row11['user_name'];
                        }?></td>
                      <td> <?php echo $row['meeting_purpose'];?></td>
                      <td><?php echo (!empty($row['in_time']) ? date('d-m-Y', strtotime($row['in_time'])) : "");?> </td>
                      <td><?php echo (!empty($row['in_time']) ? date('H:i:s', strtotime($row['in_time'])) : "");?> </td>
                      <td><?php echo (!empty($row['out_time']) ? date('H:i:s', strtotime($row['out_time'])) : "");?> </td>
                        
                       
                        <td><?php 
                        if($row['status']=="1")
                        {
                          ?>
                        <a href="id_display.php?id=<?php echo $row['id'];?>" class="btn bg-gradient-success btn-xs"><i class="fas fa-check"></i> ID CARD</a>

                          <?php
                        }
                        else
                        {
                        ?>
                          <a href="" class="btn   bg-gradient-danger  btn-xs"><i class="fas fa-times"></i></a>
                          <?php

}
                          ?>
                        </td> 
                       
                      </tr>
				 <?php
			 }
			 else
			 {
				 /*echo 'No call';*/
		 }
			 }
			 
			 else
			 {
				?>
<tr>
					  <td><?php echo $i+1;?></td>
                        <td><?php echo $row['visitor_id'];?></td>
                        <td><?php echo $row['visitor_name'];?></td>
                        <td><?php echo $row['email_id'];?> </td>
                        <td><?php echo $row['mobile'];?> </td>
                        <td><?php 
                        //SELECT `id`, `tenant_name`, `tenant_email`, `tenant_contact`, `status`, `add_info_date`, `update_info_date` FROM `tenants` WHERE 1
$where2 = array('id' =>$row['host_orgn']);
 $variable2=$obj_curd->display_all_record("tenants",$where2);
                    foreach ($variable2 as $row2) {
echo $row2['tenant_name'];
                    }

                        ?> 
                        </td>
                        <td><?php  $where11 = array('id' =>$row['host_name']);
              
                    $variable11=$obj_curd->display_all_record("tenants_users",$where11);
                    foreach ($variable11 as $row11) {
echo $row11['user_name'];
                    }?> 
                        </td>
                        <td> <?php echo $row['meeting_purpose'];?>
                      </td>
                      <td><?php echo (!empty($row['in_time']) ? date('d-m-Y', strtotime($row['in_time'])) : "");?> </td>
                      <td><?php echo (!empty($row['in_time']) ? date('H:i:s', strtotime($row['in_time'])) : "");?> </td>
                      <td><?php echo (!empty($row['out_time']) ? date('H:i:s', strtotime($row['out_time'])) : "");?> </td>
                        
                       
                        <td><?php 
                        if($row['status']=="1")
                        {
                          ?>
<a href="id_display.php?id=<?php echo $row['id'];?>" class="btn bg-gradient-success btn-xs"><i class="fas fa-check"></i> ID CARD</a>
                          <?php
                        }
                        else
                        {
                        ?>
                          <a href="" class="btn   bg-gradient-danger  btn-xs"><i class="fas fa-times"></i></a>
                          <?php

}
                          ?>
                        </td> 
                       
                      </tr>
<?php				
			 }
                    
					  $i++;
                    }
                    ?>
                  </tbody>
                </table>
              </div>
              </div>
              <!-- /.card -->
            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include('Footer.php');?>
  </div>
  <!-- ./wrapper -->
  <?php include('script.php');?>
  <!-- delete -->
  <script type="text/javascript">
    $('.del_record').on('click', function () {
    var oprn1 = $(this).val();
    alert(oprn1);
       $.ajax({
        type: 'POST', 
        url: 'delete.php',
         
        data: { 'deltype' : 'vendor',   'del_record' : oprn1},
        type: 'post',
        success: function (response) {
      
          if(response='1')
          {

            $("#result").addClass("alert alert-success fade show");
            $("#result").html("Update information added successfuly ");
          }
          else
          {
            $("#result").addClass("alert alert-danger fade show");
            $("#result").html("Please update proper data.");
          }

          $(".alert").delay(2000).slideUp(250, function() {
            $(this).alert('close');
          });
        },
        error: function (response) {
          alert(response);

          location.reload();
        }
      });
    });
  </script>
  <!-- delete -->
</body>
</html> 