<?php
include ('session_check.php');
include('class/Curd.php');
?>
<!DOCTYPE html>
<html>
<?php include('head.php');?>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <!-- Navbar -->
    <?php include('nav.php');?>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <?php include('side_menu.php');?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content"  style="padding: 0px">
          <div class="container-fluid">
            <div class="row">
               <h4 class="page-title ">View all tenant users</h4>


    
               
                  <!-- /.card-header -->
                  <div class="col-sm-12">
                    <center><div id="result"></div></center>
                  </div>
                  <!-- form start -->
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  
                    <tr> 
             
                      <th>Name</th>  
                      <th>Contact Details</th>  
                      <th>Tenant</th> 
                      <?php if($mem_type=='1')
        {
        ?>
 <th>Login Details</th> 
        <?php }?> 
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    /*SELECT `id`, `user_name`, `mobile_no`, `mem_email`, `tenant_id`, `status` FROM `tenants_users` WHERE 1*/
                    $variable=$obj_curd->display_all_record("tenants_users",'');
                    foreach ($variable as $row) {
             
                   ?>
                      <tr>
                        <td><?php echo $row['user_name'];?></td>
                        <td><?php echo $row['mem_email'];?></td>
                        <td><?php echo $row['mobile_no'];?></td>
                        <td> 
                          

                          <?php
            
             $where = array('id' =>$row['tenant_id']);
   $variable2=$obj_curd->display_all_record("tenants",$where);
                    foreach ($variable2 as $row2) {
            echo $row2['tenant_name'];
          } 

            ?>
                        </td>
                          <?php if($mem_type=='1')
        {
        ?>
 <td>
<?php
$where3=array('mem_id' =>$row['id'], 'mem_type'=>'5');

$variable3=$obj_curd->display_all_record("users",$where3);
$i=1;
 foreach ($variable3 as $row3) {
	 if($i==1)
	 {
	 
	 
echo 'UId:<b>'.$row3['user_id'].'</b>';
echo '<br>Password:<b>'.$row3['user_pwd'].'</b>';
}
$i++;

 }
?>

 </td> 
        <?php }?>
                       
                        <td>
 
                          <?php 
                        if($row['status']=="1")
                        {
                          ?>
<button class="btn   bg-gradient-success btn-xs change_status" value="<?php echo $row['id'].'-'.$row['status'];?>"><i class="fas fa-check"></i></button>

                          <?php
                        }
                        else
                        {
                        ?>
                      
                          <button class="btn   bg-gradient-danger btn-xs change_status" value="<?php echo $row['id'].'-'.$row['status'];?>"><i class="fas fa-times"></i></button>
                          <?php

}
                          ?>
                        </td> 
                        <td>
                      
                          <button class="del_record btn  bg-gradient-danger btn-sm " value="<?php echo $row['id'];?>"  >Delete</button>
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tfoot>
                </table>
              </div>
              </div>
              <!-- /.card -->
            </div>
            <!--/.col (left) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include('Footer.php');?>
  </div>
  <!-- ./wrapper -->
  <?php include('script.php');?>
  <!-- delete -->
  <script type="text/javascript">
    $('.del_record').on('click', function () {
    var oprn1 = $(this).val();
  
       $.ajax({
        type: 'POST', 
        url: 'delete.php',
         
        data: { 'deltype' : 'tenant_user',   'del_record' : oprn1},
        type: 'post',
        success: function (response) {
      
          if(response='1')
          {

            $("#result").addClass("alert alert-success fade show");
            $("#result").html("Update information added successfuly ");
          }
          else
          {
            $("#result").addClass("alert alert-danger fade show");
            $("#result").html("Please update proper data.");
          }

          $(".alert").delay(2000).slideUp(250, function() {
            $(this).alert('close');
          });
           location.reload();
        },
        error: function (response) {
          alert(response);

          location.reload();
        }
      });
    });
  </script>
  <script type="text/javascript">
    $('.change_status').on('click', function () {
    var oprn1 = $(this).val(); 
  
       $.ajax({
        type: 'POST', 
        url: 'change_status.php',
         
        data: { 'deltype' : 'tenant_user',   'change_status' : oprn1},
        type: 'post',
        success: function (response) {
     
         if(response='1')
          {

            $("#result").addClass("alert alert-success fade show");
            $("#result").html("Update information added successfuly ");
          }
          else
          {
            $("#result").addClass("alert alert-danger fade show");
            $("#result").html("Please update proper data.");
          }

          $(".alert").delay(2000).slideUp(250, function() {
            $(this).alert('close');
          });
           location.reload(); 
        },
        error: function (response) {
          alert(response);

          location.reload();
        }
      });
    });
  </script>
  <!-- delete -->
</body>
</html> 