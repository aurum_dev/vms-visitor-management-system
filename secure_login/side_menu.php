<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="dashboard.php" class="brand-link" style="padding: 10px; border-bottom: 0px;">
   
   <center><img src="dist/logo-icon.png" style="height: 50px;"></center>
  </a>
  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
     <ul class="list-unstyled text-center mt-2 mb-3">
       <li>Hello, <b><?php echo  $user_name; ?></b></li>
       <li><b><?php echo  $user_email; ?></b></li>
     </ul>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
        with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="dashboard.php" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Dashboard
 
            </p>
          </a>
        </li>
        <!-- START Visitor Checin/Out MENU -->
        <?php if($mem_type == SECURITY) {?>
        <li class="nav-item">
          <a href="visitor_check_in_out.php" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Visitor Check In/Out
            </p>
          </a>
        </li>
        <?php } ?>
        <!--END Visitor Checin/Out MENU -->
        <?php if($mem_type=='1')
        {
        ?>
        <li class="nav-item has-treeview ">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Locations
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="location.php?opr=add" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add location</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="view_all_locations.php" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>View all locations</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview ">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Security
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="register_security_desk.php?opr=add" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Security desk</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="view_all_security.php" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>View all Securities desk</p>
              </a>
            </li>
          </ul>
        </li>
        
        
        <!-- START Visitor Checin/Out MENU -->
        <?php if($mem_type == ADMIN || $mem_type == SECURITY) {?>
        <li class="nav-item">
          <a href="visitor_check_in_out.php" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Visitor Check In/Out
            </p>
          </a>
        </li>
        <?php } ?>
        <!--END Visitor Checin/Out MENU -->

        
        <li class="nav-item has-treeview ">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Tenant
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="tenant.php?opr=add" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Tenant</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="view_all_tenant.php" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>View all tenant</p>
              </a>
            </li>
          </ul>
        </li>
        <?php } ?>
       <?php if($mem_type=='1' || $mem_type=='2')
        {
        ?>
        <li class="nav-item has-treeview ">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Tenant User
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="tenant_users.php?opr=add" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add User</p>
              </a>
            </li>
          <!--   <li class="nav-item">
              <a href="upload_bulk_data_ten_ser.php" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Bulk User</p>
              </a>
            </li> -->

      <?php
        if($mem_type=='1'){
          $action='view_all_tenant_admin_users.php';
        }else{
          $action='view_All_tenant_users.php';
        }
      ?>
            <li class="nav-item">
              <a href="<?php echo $action; ?>" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>View All User</p>
              </a>
            </li>
          </ul>
        </li>
		  <?php } ?>

        <li class="nav-item has-treeview ">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Visitors
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="add_visitor.php?opr=add" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Visitor</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="upload_bulk_data.php" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Bulk Visitor</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="view_All_visitors.php" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Visits Schedule</p>
              </a>
            </li>
          </ul>
        </li>
    
        <!-- START CUSTOMIZE PASS MENU -->
        <?php if($mem_type == ADMIN || ($obj_curd->checkPageAcces('CUSTOMIZE_PASS') == 1)) {?>
        <li class="nav-item has-treeview ">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Customize Visitor Pass
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="customize_pass.php" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Create Pass</p>
              </a>
            </li>
            <?php if($mem_type == ADMIN) {?>
            <li class="nav-item">
              <a href="customize_pass_access.php" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Customize Pass Permission</p>
              </a>
            </li>
            <?php } ?>
          </ul>
        </li>
        <?php } ?>
        <!--END CUSTOMIZE PASS MENU -->

        <li class="nav-item">
          <a href="active_visitors.php" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Current visitors <span class="badge badge-pill badge-primary pull-right">
          <?php
                    if($mem_type=='2')
                    {
                      $where = array('host_orgn' =>$mem_id, 'status' =>'1', 'verified' =>'1');
                    }
					else if($mem_type=='5')
					{
						$where = array('host_name' =>$mem_id, 'status' =>'1', 'verified' =>'1');
					}
                  else
                  {
                     $where =array('status' =>'1', 'verified' =>'1');
                  }
                    $variable=$obj_curd->display_all_recordCount("visitor_info",$where);
                    echo $variable;
             
                   ?>
              </span>
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="past_vitors.php" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Past visitors <span class="badge badge-pill badge-primary pull-right">
 <?php
                   if($mem_type=='2')
                    {
                      $where = array('host_orgn' =>$mem_id, 'status' =>'4', 'verified' =>'1');
                    }
					else if($mem_type=='5')
					{
						$where = array('host_name' =>$mem_id, 'status' =>'4', 'verified' =>'1');
					}
                  else
                  {
                     $where ='';
                     $where =array('status' =>'4', 'verified' =>'1');
                  }
                    $variable=$obj_curd->display_all_recordCount("visitor_info",$where);
                    echo $variable;
             
                   ?>
              </span>
            </p>
          </a>
        </li>
        
        <li class="nav-item">
          <a href="new_request.php" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Visitors requests <span class="badge badge-pill badge-primary pull-right">
 <?php
                      if($mem_type=='2')
                    {
                      
                      $where = array('host_orgn' =>$mem_id, 'status' =>'0', 'verified' =>'0');
                    }
					else if($mem_type=='5')
					{
						$where = array('host_name' =>$mem_id, 'status' =>'0', 'verified' =>'0');
					}
                  else
                  {
                     $where = array('status' =>'0', 'verified' =>'0');
                  }
                    $variable=$obj_curd->display_all_recordCount("visitor_info",$where);
                    echo $variable;
             
                   ?>
              </span>
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="planed_visitors.php" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
             Scheduled visits <span class="badge badge-pill badge-primary pull-right">
 <?php
                      if($mem_type=='2')
                    {
                      
                      $where = array('host_orgn' =>$mem_id, 'pass_status' =>1, 'type' => 'SCHEDULE');
                    }
					else if($mem_type=='5')
					{
						$where = array('host_name' =>$mem_id, 'pass_status' =>1, 'type' => 'SCHEDULE');
					}
                  else
                  {
                     $where = array('pass_status' =>1, 'type' => 'SCHEDULE');
                  }
                    $variable=$obj_curd->display_all_recordCount("visitor_info",$where);
                    echo $variable;
             
                   ?>
              </span>
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="change_password.php" class="nav-link">
            <i class="nav-icon fas fa-lock"></i>
            <p>
              Reset Password
 
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>