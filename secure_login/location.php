<?php

include ('session_check.php');
$oprn=$_GET['opr'];
include('class/Curd.php');
if($oprn=='add')
{
$area_name="";
$delivery_charge="";
 
$st='checked';
}
else
{
 /*SELECT `id`, `location_name`, `status` FROM `locations` WHERE 1*/
  $id=$_GET['id'];
  
  $where=array("id"=>$id);
$variable=$obj_curd->display_all_record("locations",$where);


  foreach ($variable as $row) {
  $location_name=$row['location_name']; 
 
  $status=$row['status'];
  if($status=='1')
  {
    $st="checked";
  }
  else
  {
    $st='';
  }
  }

}
?>

<!DOCTYPE html>
<html>
<?php include('head.php');?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <?php include('nav.php');?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 <?php include('side_menu.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
 

    <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-sm-3"></div>
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><?php echo $oprn?> location</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form  id="slider_form" method="post" action="" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="col-sm-12">
                    <center><div id="result"></div></center>
                  </div>
               
               
                  <div class="form-group">
                    <label for="cat_name">Location name</label>
                    
                    <input type="text" id="location_name" name="location_name" value="<?php echo $location_name;?>" class="form-control"> 
                  </div>
               
              
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" value="1" name="status" id="status" <?php echo $st;?>>
                    <label class="form-check-label"  for="status">Active</label>
                  </div>
                   
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <input type="hidden" value="<?php echo $oprn; ?>"  id="oprn" name="oprn">
                      <?php
if($oprn=='edit')
{
  ?>
 <input type="hidden" value="<?php echo $id; ?>"  id="rid" name="rid"> 
  <?php
}

                  ?>
                  <button type="submit" class="btn btn-primary sub_testi">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

   
          </div>
          <!--/.col (left) -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('Footer.php');?>

  
</div>
<!-- ./wrapper -->

<?php include('script.php');?>
<script type="text/javascript">
    $('.sub_testi').on('click', function (e) {
         e.preventDefault();
 $("#upload").css("display","none");
                  $("#loader").css("display","block");
    var form_data = new FormData();
  
   
  form_data.append("location_name", document.getElementById('location_name').value);  
  form_data.append("status", document.getElementById('status').value);
  form_data.append("oprn", document.getElementById('oprn').value);
  /*edit*/
     <?php
if($oprn=='edit')
{
  ?>
 
  form_data.append("rid", document.getElementById('rid').value); 
  <?php 
}
  ?>
  /*edit*/

    $.ajax({
    url: 'location_conf.php',
    dataType: 'text',
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
     success: function (response) {
    
       if(response='1')
        {
       
              $("#result").addClass("alert alert-success fade show");
              $("#result").html("Update information added successfuly ");

          }
          else
          {
             $("#result").addClass("alert alert-danger fade show");
              $("#result").html("Please update proper data.");
          } 

      $('#description').val('');  
       $(".alert").delay(2000).slideUp(250, function() {
    $(this).alert('close');
}); 
       location.reload();
    },
    error: function (response) {
      alert(response);
       
                            location.reload();
    }
    });
});   

</script>
</body>
</html>
