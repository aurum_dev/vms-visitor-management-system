<!DOCTYPE html>
<html>
<head>
<title>Orize Securities</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
  <link href="css/jquery.signaturepad.css" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body  >
 
<section class=" container-fluid  ">
 
  <div class="container">
    <diV class="row  align-items-center justify-content-center text-center ">
      
     <div class="col-sm-6" style="margin-top: 20%">
       <h1>Thank You!</h1>
       <p>for signin our security will confirm your visit soon!</p>
     </div>
    
  </div>



</section>
<!--Script -->
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
   <script src="js/jquery.signaturepad.js"></script>
 
  <script>
   $(document).ready(function() {
      $('#slider_form').signaturePad({drawOnly:true});
    });
  </script> 
 


 <script type="text/javascript">
      $('.sub_testi').on('click', function (e) {
      e.preventDefault();
      $("#upload").css("display","none");
      $("#loader").css("display","block");
      var form_data = new FormData();
      var ins = document.getElementById('multiFiles').files.length;
      for (var x = 0; x < ins; x++) {
      form_data.append("files[]", document.getElementById('multiFiles').files[x]);
      
      }
    form_data.append("vist_name", document.getElementById('vist_name').value);
      form_data.append("vist_mobile", document.getElementById('vist_mobile').value); 
      form_data.append("vist_email", document.getElementById('vist_email').value); 
      form_data.append("vist_comming_from", document.getElementById('vist_comming_from').value); 
      form_data.append("vist_org", document.getElementById('vist_org').value); 
      form_data.append("persons_count", document.getElementById('persons_count').value); 
      form_data.append("host_org", document.getElementById('host_org').options[document.getElementById('host_org').selectedIndex].value);
    
     
      form_data.append("host_person", document.getElementById('host_person').value);
      form_data.append("sign_output", document.getElementById('sign_output').value);
      form_data.append("visit_purpose", document.getElementById('visit_purpose').value);
      form_data.append("vehicle_no", document.getElementById('vehicle_no').value); 
   
     
      $.ajax({
      url: 'sign_in_conf.php',
      dataType: 'text',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function (response) {
     // alert(response);
   
  if(response='1')
      {
        document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      $("#result").addClass("alert alert-success fade show");
      $("#result").html("Visitor information added successfuly!");
       window.location.replace("thanks.php");
      }
      else
      {
      $("#result").addClass("alert alert-danger fade show");
      $("#result").html("Please update proper data.");
      } 
      $('#description').val('');
      $(".alert").delay(2000).slideUp(250, function() {
      $(this).alert('close');
      });
      
  //location.reload();
      },
      error: function (response) {
      alert(response);
      
     // location.reload();
      }
      });
      });

      $(function () {
  setTimeout(function() {
    window.location.replace("index.html");
  }, 3000);
}); 
      </script>
<!--Script -->
</body>
</html>