<?php
 
 
include ('secure_login/class/Curd.php');?>
<!DOCTYPE html>
<html>
  <head>
    <title>Visitor registration</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="css/jquery.signaturepad.css" rel="stylesheet">
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="webcam/bootstrap.min.css">
    <link rel="stylesheet" href="webcam/layout.css">
    <link rel="stylesheet" href="webcam/font-awesome.min.css">  
    <link rel="stylesheet" href="webcam/cropper.css">
  </head>
  <body  >
    
    <section class=" container-fluid  ">
      <header class="row  innerpage_header ">
        
        <h2 class="">Visitor registration </h2>
        
        
      </header>
      <div class="container">
        <diV class="row  align-items-center justify-content-center">
          
          <h1 class="title_heading">Please fill up your information</h1>
          
          <form class="row align-items-center justify-content-center" id="slider_form" method="post" action="" enctype="multipart/form-data">
            <div id="result"></div>
            <div class="form-group col-sm-10 col-10">
              <label for="usr">Name: </label>
              <input type="text" class="form-control input_style validate[required]" id="vist_name" data-errormessage-value-missing="Enter your name." 
  >
            </div>
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Mobile No:</label>
              <input type="text" class="form-control input_style validate[required, custom[onlyNumberSp],minSize[10],maxSize[10]]" maxlength="10" id="vist_mobile" data-errormessage-value-missing="Mobile number is required!" data-errormessage-value-missing="Mobile number is required!"  >
            </div>
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Email Id:</label>
              <input type="text" class="form-control input_style validate[required]" id="vist_email" data-errormessage-value-missing="Email id is required!">
            </div>
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Coming from:</label>
              <input type="text" class="form-control input_style validate[required]" id="vist_comming_from">
            </div>
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Your Organisation:</label>
              <input type="text" class="form-control input_style validate[required]" id="vist_org">
            </div>
        <div class="form-group col-sm-10  col-10">
              <label for="usr">Upload your photo:</label>
              <input type="file" accept="image/*" class="form-control input_style validate[required]" id="multiFiles">
            </div>
           <!-- <div class="imagearea col-lg-5 col-md-5 col-sm-12">
            <div class="avatarimage" id="drop-area"> 
            <img src="./webcam/avatar.jpg" alt="avatar" id="avatarimage">
            <p>Drop your avatar here</p>
            </div>
            <div class="buttonarea"> 
            <button class="btn upload" data-toggle="modal" data-target="#myModal" onclick="cropImage()"><i class="fa fa-upload"></i> &nbsp; Upload</button> -->
            
            <label class="btn upload"> <i class="fa fa-upload"></i> &nbsp; Upload<input type="file" class="sr-only" id="input" name="image" accept="image/*"></label>
            <button class="btn camera" data-backdrop="static" data-toggle="modal" data-target="#cameraModal"><i class="fa fa-camera"></i> &nbsp;  Camera</button>
            </div>
            <div class="alert" role="alert"></div>
          </div>-->

            <div class="form-group col-sm-10  col-10">
              <label for="usr">Persons along with you:</label>
              <input type="text" class="form-control input_style validate[required]" id="persons_count">
            </div>
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Host organisation:</label>
              
			   <select class="form-control input_style validate[required]" id="host_org"  
                onChange="getEmployee(this.value);">
                      <option value="">Select Organization</option>
                      <?php
                      /*SELECT `id`, `tenant_name`, `tenant_email`, `tenant_contact`, `status`, `add_info_date`, `update_info_date` FROM `tenants` WHERE 1*/
                      $where2=array("status"=>'1');
                      $variable2=$obj_curd->display_all_record("tenants",$where2);
                      foreach ($variable2 as $row2) {
                         
                        ?>
                        <option value="<?php echo $row2['id'];?>"><?php echo $row2['tenant_name'];?></option>
                        <?php
                      }
                      ?>
                    </select>
            </div>
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Whome you are visiting:</label>
          
                <select class="form-control input_style validate[required]" id="host_person" >
                      <option value="">Select Employee</option>
                    </select>
            </div>
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Your purpose of visit:</label>
              <input type="text" class="form-control input_style validate[required]" id="visit_purpose">
            </div>
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Vehicle number:</label>
              <input type="text" class="form-control input_style " id="vehicle_no">
            </div>
            <div class="form-group col-sm-10  col-10 ">
              <label for="usr">Make signature:</label>
              
              
              <div class="typed"></div>
              <canvas class="pad"  height="150" style="display: block; background: #fbfbfb; width: 300px; border:1px solid #ccc;"></canvas>
              <input type="hidden" name="output" class="output" id="sign_output">
              
            </div>
             <div class="form-group col-sm-10  col-10" id="otpfield" style="display:none;">
              <label for="usr">OTP:</label>
              <input type="number" class="form-control input_style validate[required]" id="otp" >
               <input type="hidden" class="form-control input_style " id="otpverified" value="0">
                <input type="hidden" class="form-control input_style " id="otpcode" >
            </div>
            
            <div class="form-group col-sm-10  col-10" id="finalsubmit" >
              <button  type="submit" class="btn btn-primary sub_testi">Sign In</button>
            </div>
          </form>
        </div>
      </section>
      <!--Script -->
     <!-- <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>-->
      <script src="webcam/jquery-1.12.4.min.js.download"></script>
      <script src="js/jquery.signaturepad.js"></script>
      <script src="js/jquery.validationEngine.min.js" type="text/javascript" charset="utf-8"></script>
      <script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
     <!-- <script type="text/javascript" src="js/bootstrap.min.js"></script>-->
     
      
      <script src="webcam/bootstrap.min.js.download"></script>
      <script src="webcam/dropzone.js.download"></script>
      <!-- Script -->
      <script src="webcam/webcam.min.js.download"></script>
      <script src="webcam/cropper.js.download"></script>
      <script>
      
      jQuery(document).ready(function () {
      $('#slider_form').signaturePad({drawOnly:true});
      
      });
      
      </script>
      
      
      <script type="text/javascript">
      $('.sub_testi').on('click', function (e) {
      alert(document.getElementById('avatarimage').value);
      e.preventDefault();
      
      if(!$("#slider_form").validationEngine('validate', {promptPosition : "inline", scroll: false})){
      return false;
      }
      
      $("#upload").css("display","none");
      $("#loader").css("display","block");
      var form_data = new FormData();
      var ins = document.getElementById('multiFiles').files.length;
      for (var x = 0; x < ins; x++) {
      form_data.append("files[]", document.getElementById('multiFiles').files[x]);
      
      }
      form_data.append("vist_name", document.getElementById('vist_name').value);
      form_data.append("vist_mobile", document.getElementById('vist_mobile').value);
      form_data.append("vist_email", document.getElementById('vist_email').value);
      form_data.append("vist_comming_from", document.getElementById('vist_comming_from').value);
      form_data.append("vist_org", document.getElementById('vist_org').value);
      form_data.append("persons_count", document.getElementById('persons_count').value);
      form_data.append("host_org", document.getElementById('host_org').options[document.getElementById('host_org').selectedIndex].value);
      form_data.append("host_person", document.getElementById('host_person').options[document.getElementById('host_person').selectedIndex].value);
      
       
      form_data.append("sign_output", document.getElementById('sign_output').value);
      form_data.append("visit_purpose", document.getElementById('visit_purpose').value);
      form_data.append("vehicle_no", document.getElementById('vehicle_no').value);
      
      if(document.getElementById('otp').value==""){
        alert('test');
         document.getElementById('otpfield').style.display="block";
      

        $.ajax({
    type: "POST",
    url: "visitorOtpVerify.php",
    data:'visitorMobileNo='+document.getElementById('vist_mobile').value,
    beforeSend: function() {
     // $("#shost_person").addClass("loader");
    },
    success: function(data){
      alert(data);
      document.getElementById('otpcode').value=data;
    //  $("#host_person").html(data); 
     // $("#host_person").removeClass("loader");
    }
  });
          
       }
   else if (document.getElementById('otp').value!="" && document.getElementById('otpverified').value=="0"){

          if(document.getElementById('otpcode').value==document.getElementById('otp').value){
            document.getElementById('otpverified').value=1;
             $.ajax({
              url: 'sign_in_conf.php',
              dataType: 'text',
              cache: false,
              contentType: false,
              processData: false,
              data: form_data,
              type: 'post',
              success: function (response) {
            // $("#result").html(response);
              
            if(response=='1')
              {
              document.body.scrollTop = 0; // For Safari
              document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
              $("#result").addClass("alert alert-success fade show");
              $("#result").html("Visitor information added successfuly!");
              window.location.replace("thanks.php");
              }
              else
              {
              $("#result").addClass("alert alert-danger fade show");
              $("#result").html("Please update proper data.");
              }
              $('#description').val('');
              $(".alert").delay(2000).slideUp(250, function() {
              $(this).alert('close'); 
              });
              
              location.reload();
              },
              error: function (response) {
              //alert(response);
              
               location.reload();
              }
              });
             document.getElementById('otpcode').value='';
          }

          else{
            alert("Invalid OTP. Please Try Again");
            document.getElementById('otpverified').value=0;
             $.ajax({
    type: "POST",
    url: "visitorOtpVerify.php",
    data:'visitorMobileNo='+document.getElementById('vist_mobile').value,
    beforeSend: function() {
     // $("#shost_person").addClass("loader");
    },
    success: function(data){
      alert(data);
      document.getElementById('otpcode').value=data;
    //  $("#host_person").html(data); 
     // $("#host_person").removeClass("loader");
    }
  });
          }
       }
   
      
      });

      function getEmployee(val) {

        $.ajax({
          type: "POST",
          url: "get-employee.php",
          data:'company_id='+val,
          beforeSend: function() {
            $("#shost_person").addClass("loader");
          },
          success: function(data){
            $("#host_person").html(data); 
            $("#host_person").removeClass("loader");
          }
        });
      }
      </script>
      <!--Script -->

<!-- The Make Selection Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Make a selection</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div id="cropimage">
    <img id="imageprev" src="./Image Upload and Crop_files/bg.png">
    </div>
    
    <div class="progress">
      <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
    </div>
      </div>
  
      <!-- Modal footer -->
      <div class="modal-footer">
    <div class="btngroup">
        <button type="button" class="btn upload1 float-left" data-dismiss="modal">Close</button>
    <button type="button" class="btn btnsmall" id="rotateL" title="Rotate Left"><i class="fa fa-undo"></i></button>
        <button type="button" class="btn btnsmall" id="rotateR" title="Rotate Right"><i class="fa fa-repeat"></i></button>
    <button type="button" class="btn btnsmall" id="scaleX" title="Flip Horizontal"><i class="fa fa-arrows-h"></i></button>
    <button type="button" class="btn btnsmall" id="scaleY" title="Flip Vertical"><i class="fa fa-arrows-v"></i></button>  
    <button type="button" class="btn btnsmall" id="reset" title="Reset"><i class="fa fa-refresh"></i></button> 
    <button type="button" class="btn camera1 float-right" id="saveAvatar">Save</button>
    </div>
      </div>

    </div>
  </div>
</div>

<!-- The Camera Modal -->
<div class="modal" id="cameraModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Take a picture</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      <div id="my_camera"></div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn upload" data-dismiss="modal">Close</button>
        <button type="button" class="btn camera" onclick="take_snapshot()">Take a picture</button>
      </div>

    </div>
  </div>
</div>

      <script language="JavaScript">
    
    // Configure a few settings and attach camera
    function configure(){
      Webcam.set({
        width: 640,
        height: 480,
        image_format: 'jpeg',
        jpeg_quality: 100
      });
      Webcam.attach('#my_camera');
    }
    // A button for taking snaps
    
    function take_snapshot() {
      // take snapshot and get image data
      Webcam.snap( function(data_uri) {
        // display results in page
        $("#cameraModal").modal('hide');
        $("#myModal").modal({backdrop: "static"});
        $("#cropimage").html('<img id="imageprev" src="'+data_uri+'"/>');
        cropImage();
        //document.getElementById('cropimage').innerHTML = ;
      } );

      Webcam.reset();
    }

    function saveSnap(){
      // Get base64 value from <img id='imageprev'> source
      var base64image =  document.getElementById("imageprev").src;
alert('1upload');
       Webcam.upload( base64image, 'upload.php', function(code, text) {
         console.log('Save successfully');
         //console.log(text);
            });

    }
  
$('#cameraModal').on('show.bs.modal', function () {
  configure();
})

$('#cameraModal').on('hide.bs.modal', function () {
  Webcam.reset();
  $("#cropimage").html("");
})

$('#myModal').on('hide.bs.modal', function () {
 $("#cropimage").html('<img id="imageprev" src="assets/img/bg.png"/>');
})


/* UPLOAD Image */  
var input = document.getElementById('input');
var $alert = $('.alert');


/* DRAG and DROP File */
$("#drop-area").on('dragenter', function (e){
  e.preventDefault();
});

$("#drop-area").on('dragover', function (e){
  e.preventDefault();
});

$("#drop-area").on('drop', function (e){
  var image = document.querySelector('#imageprev');
  var files = e.originalEvent.dataTransfer.files;
  
  var done = function (url) {
          input.value = '';
          image.src = url;
          $alert.hide();
      $("#myModal").modal({backdrop: "static"});
      cropImage();
        };
    
  var reader;
        var file;
        var url;

        if (files && files.length > 0) {
          file = files[0];

          if (URL) {
            done(URL.createObjectURL(file));
          } else if (FileReader) {
            reader = new FileReader();
            reader.onload = function (e) {
              done(reader.result);
            };
            reader.readAsDataURL(file);
          }
        } 
  
  e.preventDefault(); 
  
});

/* INPUT UPLOAD FILE */
input.addEventListener('change', function (e) {
    var image = document.querySelector('#imageprev');
        var files = e.target.files;
        var done = function (url) {
          input.value = '';
          image.src = url;
          $alert.hide();
      $("#myModal").modal({backdrop: "static"});
      cropImage();
      
        };
        var reader;
        var file;
        var url;

        if (files && files.length > 0) {
          file = files[0];

          if (URL) {
            done(URL.createObjectURL(file));
          } else if (FileReader) {
            reader = new FileReader();
            reader.onload = function (e) {
              done(reader.result);
            };
            reader.readAsDataURL(file);
          }
        }
      });
/* CROP IMAGE AFTER UPLOAD */ 
function cropImage() {
      var image = document.querySelector('#imageprev');
      var minAspectRatio = 0.5;
      var maxAspectRatio = 1.5;
    
      var cropper = new Cropper(image, {
    aspectRatio: 11 /12,  
    minCropBoxWidth: 220,
    minCropBoxHeight: 240,

        ready: function () {
          var cropper = this.cropper;
          var containerData = cropper.getContainerData();
          var cropBoxData = cropper.getCropBoxData();
          var aspectRatio = cropBoxData.width / cropBoxData.height;
          //var aspectRatio = 4 / 3;
          var newCropBoxWidth;
      cropper.setDragMode("move");
          if (aspectRatio < minAspectRatio || aspectRatio > maxAspectRatio) {
            newCropBoxWidth = cropBoxData.height * ((minAspectRatio + maxAspectRatio) / 2);

            cropper.setCropBoxData({
              left: (containerData.width - newCropBoxWidth) / 2,
              width: newCropBoxWidth
            });
          }
        },

        cropmove: function () {
          var cropper = this.cropper;
          var cropBoxData = cropper.getCropBoxData();
          var aspectRatio = cropBoxData.width / cropBoxData.height;

          if (aspectRatio < minAspectRatio) {
            cropper.setCropBoxData({
              width: cropBoxData.height * minAspectRatio
            });
          } else if (aspectRatio > maxAspectRatio) {
            cropper.setCropBoxData({
              width: cropBoxData.height * maxAspectRatio
            });
          }
        },
    
    
      });
    
    $("#scaleY").click(function(){ 
    var Yscale = cropper.imageData.scaleY;
    if(Yscale==1){ cropper.scaleY(-1); } else {cropper.scaleY(1);};
    });
    
    $("#scaleX").click( function(){ 
    var Xscale = cropper.imageData.scaleX;
    if(Xscale==1){ cropper.scaleX(-1); } else {cropper.scaleX(1);};
    });
    
    $("#rotateR").click(function(){ cropper.rotate(45); });
    $("#rotateL").click(function(){ cropper.rotate(-45); });
    $("#reset").click(function(){ cropper.reset(); });
    
    $("#saveAvatar").click(function(){
      var $progress = $('.progress');
      var $progressBar = $('.progress-bar');
      var avatar = document.getElementById('avatarimage');
      var $alert = $('.alert');
          canvas = cropper.getCroppedCanvas({
            width: 220,
            height: 240,
          });
          
          $progress.show();
          $alert.removeClass('alert-success alert-warning');
          canvas.toBlob(function (blob) {
            var formData = new FormData();

            formData.append('avatar', blob, 'avatar.jpg');
            $.ajax('upload.php', {
              method: 'POST',
              type: 'POST',
              data: formData,
              processData: false,
              contentType: false,

              xhr: function () {
                var xhr = new XMLHttpRequest();

                xhr.upload.onprogress = function (e) {
                  var percent = '0';
                  var percentage = '0%';

                  if (e.lengthComputable) {
                    percent = Math.round((e.loaded / e.total) * 100);
                    percentage = percent + '%';
                    $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);
                  }
                };

                return xhr;
              },

              success: function () {
                //$alert.show().addClass('alert-success').text('Upload success');
              },

              error: function () {
                avatar.src = initialAvatarURL;
                $alert.show().addClass('alert-warning').text('Upload error');
              },

              complete: function () {
        $("#myModal").modal('hide');  
                $progress.hide();
        initialAvatarURL = avatar.src;
        avatar.src = canvas.toDataURL();
              },
            });
          });
    
    });
};  

</script>
    
    </body>
  </html>