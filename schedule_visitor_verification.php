<?php
    include ('secure_login/class/Curd.php');
    require __DIR__ . '/vendor/sms/Pinnacle.php';
    // $q = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
    // //$data = $obj_curd->encrypt_data('334'); 
    // $q="ouh7acxcDYbcHgvwjqEXm2vA7GmuQfxyAvgL8qP/KVw=qjC9zAp";
    // // if(isset($_GET['q']) && !empty($_GET['q'])) {
    // $status = false;  
    // if(isset($q) && !empty($q)) {
    //     $data = $obj_curd->decrypt_data(trim($q));
    //     echo $data;
    //     if(!empty($data)) {
    //         if(preg_match('/^[0-9]+$/', $data)) {
    //             $where = $where = array('id' => $data, 'verified' => 0, 'status' => 3);
    //             $data = $obj_curd->display_all_record('visitor_info', $where);
    //             if(count($data) <= 0) {
    //               echo "<h1>Invalid url</h1>"; die();
    //             }else{
    //               $status = true;
    //             }
    //         }
    //     }
    // }
    //220909934
    $status = false;  
    if(isset($_POST['url_id'])  && !empty($_POST['url_id'])) {
      $q = $obj_curd->getLongUrl($_POST['url_id']);
      $qry = "SELECT * FROM visitor_info WHERE verified = 0 AND pass_status = 1 AND id = ".$q;
      $data = $obj_curd->executeRawQuery($qry);
      
      if(!empty($data)) {
        foreach($data as $key =>$val) {
          $current_date = strtotime(date('Y-m-d'));
          if($val['type'] == 'CUSTOMIZE') {
            $from_date = strtotime(date('Y-m-d', strtotime($val['from_date'])));
            $to_date = strtotime(date('Y-m-d', strtotime($val['to_date'])));
            if (($current_date >= $from_date) || ($current_date <= $to_date)){
              $status = true;
            }
            
          }else if($val['type'] == 'SCHEDULE') {
            if($current_date <= strtotime($val['in_time'])) {
              $status = true;
            }
          }
        }
      }
    }

    if(isset($_POST['otp_code']) && !empty($_POST['otp_code'])) {

      if(isset($_POST['user']) && !empty($_POST['user'])) {
        //$id = $obj_curd->decrypt_data(trim($_POST['user']));
        $id = trim($_POST['user']);  
        if(!preg_match('/^[0-9]+$/', $id)) {
              echo json_encode(array('status' => 0, "msg" => "Invalid OTP")); die();
        }
        if(empty(trim($_POST['img']))) {
              echo json_encode(array('status' => 0, "msg" => "Image is required")); die();
        }

        $otp = trim($_POST['otp_code']);
        if(preg_match('/^[0-9]+$/', $otp)) {
            $query = "SELECT vf.*, tu.user_name, t.tenant_name FROM visitor_info as vf 
                  LEFT JOIN tenants_users as tu ON vf.host_name = tu.id
                  LEFT JOIN tenants as t ON vf.host_orgn = t.id
                  WHERE vf.otp_code = $otp AND vf.id = $id AND vf.verified = 0";
            
            $data = $obj_curd->executeRawQuery($query);
            
            if(count($data) > 0) {
                $data = $data[0];
                $where = array('id' => $id);
                $fields=array('verified' =>'1', 'status' =>'1', 'photo' => trim($_POST['img']));
                if($obj_curd->update_records("visitor_info",$where,$fields))
                {
                    if($data['type'] == "CUSTOMIZE") {
                      $data['in_time'] = 'From: <strong>'.date('d-m-Y', strtotime($data['from_date'])). '</strong> To: <strong>'.date('d-m-Y', strtotime($data['to_date'])).'</strong>';
                    }else{
                      $data['in_time'] = "Intime: <strong>".$data['in_time']."</strong>";
                    }
                    /*
                    * code to send Email 
                    */
                    $qr_code = $obj_curd->base_url."secure_login/".$obj_curd->generateQrCode($data['visitor_id'], 'secure_login/visitor_check_in_out.php');
                    $template = mailTemplate($data, $qr_code, $obj_curd->base_url);
                    $subject="Visitor Id";
                    $obj_curd->send_secure_mail($data['email_id'],$template,$subject);
                    /*
                    * Email Code Ends here 
                    */

                    /*
                    * code to send Visitor Id SMS 
                    */
                    $sms = new Pinnacle();
                    $resp = $sms->sendVisitorId(trim($data['visitor_id']), trim($data['mobile']));
                    /*
                    * SMS Code Ends here
                    */

                    echo json_encode(array('status' => 1, "msg" => "Verification has done successfully")); die();
                }   
            }
        }
        echo json_encode(array('status' => 0, "msg" => "Invalid OTP")); die();
      }else{
        echo json_encode(array('status' => 0, "msg" => "Something went wrong!!")); die();
      }
    }


    if(isset($_POST['resend_otp_code']) && $_POST['resend_otp_code'] == 1 && isset($_POST['user']) && !empty($_POST['user'])) {
        //$id = $obj_curd->decrypt_data(trim($_POST['user']));
        $id = trim($_POST['user']);
        if(!preg_match('/^[0-9]+$/', $id)) {
              echo json_encode(array('status' => 0, "msg" => "Something went wrong!!")); die();
        }  
        $where = array('id' => $id, 'verified' => 0);
        $data = $obj_curd->display_all_record("visitor_info",$where);
        if(count($data) > 0) {
            if($data[0]['otp_count'] >= 3) {
                echo json_encode(array('status' => 2, "msg" => "Resend OTP attempts exceed")); die();
            }else{
                $otp_code = $obj_curd->generateNumericOTP(6);
                $otp_count = $data[0]['otp_count']+1;
                $fields=array('otp_code' => $otp_code, 'otp_count' => $otp_count); 
                if($obj_curd->update_records("visitor_info",$where,$fields))
                {
                    
                    /*
                    * code to send OTP SMS 
                    */
                    $sms = new Pinnacle();
                    $resp = json_decode($sms->sendOtp(trim($otp_code), trim($data[0]['mobile'])), true);
                    //$resp = json_decode($epinet_sms->sendMsg(trim($data[0]['mobile']), $text, $tempid),true);
                    
                    if($resp['status'] == 'success') {
                        echo json_encode(array('status' => 1, "msg" => "OTP has been sent on your mobile")); die();
                    }else{
                        echo json_encode(array('status' => 0, "msg" => "Something went wrong, OTP has not been sent!!")); die();
                    }
                    /*
                    * SMS Code Ends here
                    */
                }
            }
        }
        echo json_encode(array('status' => 0, "msg" => "Something went wrong")); die();
    }

    function mailTemplate($data, $qr_code, $basepath) {
      return $message="
                        <html>
                          <head>
                            <title>Visitor Id Details</title>
                          </head>
                          <body>
                            <p>Dear Sir/Madam,<br> Greetings from Orize !!! <br>Thanks for visit.<br> please use following visitor id to verify as security desk.</p>
                            <table style='margin: 0 auto 30px;
                                display: block;
                                max-width: 420px;
                                border: 1px solid #000;
                                border-radius: 10px;
                            '>
                              <tr>
                                <td   style='background: #000; color: #fff; font-weight: 700; font-size: 20px; line-height: 23px;padding: 10px 7px;  ' >V<br>I<br>S<br>I<br>T<br>O<br>R</td>
                                <td style='padding: 15px'>
                                  <table>
                                   <!-- <tr >
                                      <td colspan='2' class='text-left'><h3 style='margin-bottom: 5px; font-weight: 700; font-size:24px'>ID: ".$data['visitor_id']."</h3></td>
                                    
                                    </tr>
                                    <tr>-->
                                    <tr >
                                      <td colspan='2' class='text-left'><h3 style='margin-bottom: 5px; font-weight: 700; font-size:24px' class='text-success'>Verified</h3></td>
                                    
                                    </tr>
                                      <td>
                                        <img src='".$basepath."secure_login/".$data['photo']."'  style='width: 120px;'>
                                      </td>
                                      <td class='text-left'>
                                          <ul style='font-size: 15px; margin-left: 15px; list-style:none'>
                                            <li>Id: <strong>".$data['visitor_id']."</strong></li> 
                                            <li>Name: <strong>".$data['visitor_name']."</strong></li> 
                                            <li>Phone: <strong>".$data['mobile']."</strong></li>
                                            <li>From: <strong>".$data['visitor_org'].", ".$data['coming_from']."</strong></li> 
                                            <li>Meets: <strong>".$data['user_name']."</strong></li> 
                                            <li>Host company: <strong>".$data['tenant_name']." </strong></li> 
                                            <li>Purpose: <strong>".$data['meeting_purpose']."</strong></li> 
                                            <li>".$data['in_time']."</li> 
                                          </ul>
                                      </td> 
                                      <td>
                                        <img src=".$qr_code." style='width:100px;'/>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </body>
                          </html>";
    }
    
    if(!$status) {
      echo "<h1 class='text-center'>Url does not exist</h1>"; die();
    } 
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Visitor registration</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="webcam/bootstrap.min.css">
    <link rel="stylesheet" href="webcam/layout.css">
    <link rel="stylesheet" href="webcam/font-awesome.min.css">  
    <link rel="stylesheet" href="webcam/cropper.css">
    <style type="text/css">
      .resend_otp {
        font-size: 14px;
        cursor: pointer;
      }
    .loader {
  
        margin:auto;
        position:fixed;
        z-index:  999;
        text-align: center;
        width: 100%;
        height: 100%;
        background: grey;
        opacity: 0.5;
        display: none;
      } 
      #my_camera{
            /*width: 311px;*/
            height: 200px;
            border: 1px solid #ebebeb;
        }
        .camera_block{
            display: none;
        }
    </style>
  </head>
  <body>
    <div class="loader" >
        <img src="images/loader.gif" style="margin-top: 300px;" width="100" height="100">
    </div>
    <section class=" container-fluid  ">
      <header class="row  innerpage_header ">
        <h2 class="">Visitor Verification </h2>
      </header>
      <div class="container">
        <diV class="row  align-items-center justify-content-center">
          <h1 class="title_heading main-heading">Please verify yourself.</h1>
          <div class="thanks-heading" style="display: none;">
            <h1 class="title_heading" >Thank you for Verification.</h1>
            <p>Visitor ID has been sent on your registered email and mobile number.</p>
          </div>
        </diV>
        <diV class="row  align-items-center justify-content-center">
          <form class="row" id="sch_visitor_verification_form" method="post" action="">
            <div id="result"></div>
            <div class="form-group col-sm-12 col-md-12">
              <label for="usr">Enter OTP: </label>
              <input type="text" class="form-control input_style validate[required]" id="otp_code" name="otp_code" data-errormessage-value-missing="Enter OTP." >
              <input type="hidden" name="user" id="user" value="<?php echo $q;?>">
              <a class="pull-right resend_otp">Resend OTP</a>
            </div>

            <div class="form-group col-sm-12  col-md-12">
              <div class="form-group col-sm-6  col-6 camera_block" style="float:left; display: inline;">
                <input type="hidden" id="webcamcode">
                <div id="my_camera"></div>
                <span class="err"></span>
              </div>
              <div class="form-group col-sm-6  col-6" style="float:left;">
                <div id="results"></div>
              </div>
            </div>
            <div class="form-group col-sm-12  col-md-12">
                <input type=button class="btn btn-info" value="Open Camera" onClick="configure()" style="margin-left:20px;">
                <input type=button class="btn btn-success camera_block" value="Take Snapshot" onClick="take_snapshot()">
            </div>    
            
            <div class="form-group col-sm-12  col-md-12" id="finalsubmit" >
              <button  type="submit" class="btn btn-primary sub_testi">Verify</button>
            </div>
          </form>
        </div>
      </diV>
    </section>
    <!--Script -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script src="js/jquery.validationEngine.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
     
    <script>
      $(document).ready(function(){
        $("#sch_visitor_verification_form").on('submit', function(e){
          e.preventDefault();
          if(!$("#sch_visitor_verification_form").validationEngine('validate', {promptPosition : "inline", scroll: false})){
            return false;
          }
          if($("#webcamcode").val() == "") {
            $(".err").text('Image is required').css("color","red");
            return false;
          }
          var data = new FormData(this);
          data.append('img', $("#webcamcode").val());
          $.ajax({
            type: 'POST',
            url: 'schedule_visitor_verification.php',
            data: data,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
              $(".loader").show();
                $('.sub_testi').attr("disabled","disabled");
            },
            success: function(response){
              $(".loader").hide()
                console.log(response);
                if(response.status) {
                  $("#result").html(response.msg).css("color","green");
                  $(".main-heading, #sch_visitor_verification_form").hide();
                  $(".thanks-heading").show();
                  $("#otp_code").val("");
                }else{
                  $("#result").html(response.msg).css("color","red");
                }
                $(".sub_testi").removeAttr("disabled");
            }
          });
          return false;
        });

        $(".resend_otp").click(function(e){
          e.preventDefault();
          var user = $("#user").val();
          $.ajax({
            type: 'POST',
            url: 'schedule_visitor_verification.php',
            data: {resend_otp_code: 1, user: user},
            dataType: 'json',
            beforeSend: function(){
              $(".loader").show();
                //$('.sub_testi').attr("disabled","disabled");
            },
            success: function(response){
              $(".loader").hide()
                console.log(response);
                if(response.status === 1) {
                  $("#result").html(response.msg).css("color","green");
                }else if(response.status === 2){
                  $(".resend_otp").hide();
                  $("#result").html(response.msg).css("color","red");
                }else{
                  $("#result").html(response.msg).css("color","red");
                }
                $("#otp_code").val("");
                //$(".sub_testi").removeAttr("disabled");
            }
          });
          return false;
        });
      });

      var shutter = new Audio();
        shutter.autoplay = false;
        shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';
      // Configure a few settings and attach camera
      function configure(){

            Webcam.set({
              height: 210,
              image_format: 'jpeg',
              jpeg_quality: 90
            });
            $('.camera_block').show();
            Webcam.attach( '#my_camera' );
            $('.open_camera').hide();
      }

        function take_snapshot() {
          // play sound effect
          shutter.play();
          // take snapshot and get image data
          Webcam.snap( function(data_uri) {

          // display results in page
            document.getElementById('results').innerHTML = '<img id="imageprev" src="'+data_uri+'"/>';
          } );
          saveSnap();
          Webcam.reset();
        }

        function saveSnap(){
          // Get base64 value from <img id='imageprev'> source
          var base64image = document.getElementById("imageprev").src;

          Webcam.upload( base64image, 'uploadnew.php', function(code, text) {
                console.log('Save successfully');
              document.getElementById("webcamcode").value=text;
          });

        }
    </script>
  </body>
</html>