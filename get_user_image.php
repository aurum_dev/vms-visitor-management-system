<?php
 
  
include ('secure_login/class/Curd.php');
$get_id=$_GET['id'];
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Visitor registration</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="css/jquery.signaturepad.css" rel="stylesheet">
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body  >
    
    <section class=" container-fluid  ">
      <header class="row  innerpage_header ">
        
        <h2 class="">Upload Photo</h2>
        
        
      </header>
      <div class="container">
        <diV class="row  align-items-center justify-content-center">
          
          <h1 class="title_heading">Please upload your photo</h1>
          
          <form class="row align-items-center justify-content-center" id="slider_form" method="post" action="" enctype="multipart/form-data">
            <div id="result"></div>
             
            <div class="form-group col-sm-10  col-10">
              <label for="usr">Upload your photo:</label>
              <input type="file" accept="image/*" class="form-control input_style validate[required]" id="multiFiles">
			  <input type="hidden" name="user_id" id="user_id" value="<?php echo $get_id;?>">
            </div>
           
            
            
            <div class="form-group col-sm-10  col-10">
              <button  type="submit" class="btn btn-primary sub_testi">Submit</button>
            </div>
          </form>
        </div>
      </section>
      <!--Script -->
      <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
      <script src="js/jquery.signaturepad.js"></script>
      <script src="js/jquery.validationEngine.min.js" type="text/javascript" charset="utf-8"></script>
      <script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      
      
      <script>
      
      jQuery(document).ready(function () {
      $('#slider_form').signaturePad({drawOnly:true});
      
      });
      
      </script>
      
      
      <script type="text/javascript">
      $('.sub_testi').on('click', function (e) {
      
      e.preventDefault();
      
      if(!$("#slider_form").validationEngine('validate', {promptPosition : "inline", scroll: false})){
      return false;
      }
      
      $("#upload").css("display","none");
      $("#loader").css("display","block");
      var form_data = new FormData();
      var ins = document.getElementById('multiFiles').files.length;
      for (var x = 0; x < ins; x++) {
      form_data.append("files[]", document.getElementById('multiFiles').files[x]);
      
      }
      form_data.append("user_id", document.getElementById('user_id').value); 
      
      
      $.ajax({
      url: 'get_new_img_conf.php',
      dataType: 'text',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      success: function (response) {
    // $("#result").html(response);
      
    if(response='1')
      {
      document.body.scrollTop = 0; // For Safari
      document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      $("#result").addClass("alert alert-success fade show");
      $("#result").html("Visitor information added successfuly!");
      window.location.replace("thanks.php");
      }
      else
      {
      $("#result").addClass("alert alert-danger fade show");
      $("#result").html("Please update proper data.");
      }
      $('#description').val('');
      $(".alert").delay(2000).slideUp(250, function() {
      $(this).alert('close'); 
      });
      
      location.reload();
      },
      error: function (response) {
      //alert(response);
      
       location.reload();
      }
      });
      
      });

      function getEmployee(val) {

  $.ajax({
    type: "POST",
    url: "get-employee.php",
    data:'company_id='+val,
    beforeSend: function() {
      $("#shost_person").addClass("loader");
    },
    success: function(data){
      $("#host_person").html(data); 
      $("#host_person").removeClass("loader");
    }
  });
}
      </script>
      <!--Script -->
    </body>
  </html>