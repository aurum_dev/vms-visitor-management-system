<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'twilio/sdk' => array(
            'pretty_version' => '5.0.0',
            'version' => '5.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../twilio/sdk',
            'aliases' => array(),
            'reference' => 'f907461ea5fd0a1d6850415b0a07117a980aae61',
            'dev_requirement' => false,
        ),
    ),
);
