<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitbf663962b98be2126d2ed6011e3e7f5b
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'Twilio\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Twilio\\' => 
        array (
            0 => __DIR__ . '/..' . '/twilio/sdk/Twilio',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitbf663962b98be2126d2ed6011e3e7f5b::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitbf663962b98be2126d2ed6011e3e7f5b::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitbf663962b98be2126d2ed6011e3e7f5b::$classMap;

        }, null, ClassLoader::class);
    }
}
