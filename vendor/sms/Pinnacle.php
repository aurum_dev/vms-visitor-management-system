<?php
	/**
	 * below class uses pinnacle service
	 */
	class Pinnacle {
		private	$apikey="0f6f00-ae6033-755635-e89f0f-2057ba";
		private	$dltentityid="1201162038886758051";
		private $sender="ARMORZ";

		public function sendMsg($mobile, $text, $tempid) {
			$smsurl="http://api.pinnacle.in/index.php/sms/send/".$this->sender."/".$mobile."/".urlencode($text)."/TXT?apikey=".$this->apikey."&dltentityid=".$this->dltentityid."&dlttempid=".$tempid;

			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => $smsurl,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_CUSTOMREQUEST => 'GET',
				CURLOPT_HTTPHEADER => array(),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			return $response;
		}

        public function sendVisitorId($data, $mobile) {
            $text = "Hello, your visitor id is ".$data." - Aurum Orize";
            $tempid = "1207164621986325074";
            return $this->sendMsg($mobile, $text, $tempid);
        }

        public function sendOtp($data, $mobile) {
            $text = "Your Verification Code is ".$data." -Aurum Orize";
            $tempid = "1207164197223853970";
            return $this->sendMsg($mobile, $text, $tempid);
        }

        public function sendPassword($data, $mobile) {
            $text = "Hello, your password is ".$data." - Aurum Orize";
            $tempid = "1207164622012553242";
            return $this->sendMsg($mobile, $text, $tempid);
        }

        public function sendVerificationSms($otp, $link, $mobile) {
            $text = "Dear User, Your OTP for verification to VMS Portal QPARC is ".$otp.". Valid for 30 minutes. Please do not share this OTP. Please click here ".$link." to verify your OTP. -Aurum Orize";
            $tempid = "1207164793456584768";
            return $this->sendMsg($mobile, urlencode($text), $tempid);
        }
	}

    //$a = new Pinnacle();
    // echo $a->sendVisitorId('1234', '7021098759');
    //print_r(json_decode($a->sendPassword('0001', '7021098759'),true));
    //print_r(json_decode($a->sendVerificationSms('0001', 'https://qparc.in/visitor/asdfghj', '7021098759'),true));
?>

